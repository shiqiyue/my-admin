package com.github.shiqiyue.myadmin;

import com.github.shiqiyue.myadmin.entity.blog.Article;
import com.github.shiqiyue.myadmin.service.blog.article.ArticleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleServiceTest {
	
	@Autowired
	private ArticleService articleService;
	
	@Test
	public void addBatch() {
        insertData(1000000);
        insertData(1000000);
        insertData(1000000);
        insertData(1000000);
        insertData(1000000);
        insertData(1000000);
    }

    private void insertData(int num) {
        long startTime = System.currentTimeMillis();
        List<Article> list = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
			Article article = new Article();
			article.setHot(Article.HOT_NO);
			article.setAuthorId("AuthorId" + i);
			article.setAuthorName("AuthorName" + i);
			article.setBrief("Brief" + i);
			article.setComments(1);
			article.setContent("Content" + i);
			article.setImg("Img" + i);
			article.setTags("Tags" + i);
			article.setTitle("Title" + i);
			article.setViews(1);
			list.add(article);
            if (i % 1000000 == 0) {
                System.out.println(i);
            }
		}
		articleService.insertBatch(list);
        System.out.println("用时:" + (System.currentTimeMillis() - startTime));
	}
}
