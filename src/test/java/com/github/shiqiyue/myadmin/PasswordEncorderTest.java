package com.github.shiqiyue.myadmin;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PasswordEncorderTest {

    @Autowired
    @Qualifier("adminPasswordEncoder")
    private PasswordEncoder passwordEncoder;

    @Test
    public void testEncorder() {
        System.out.println(passwordEncoder.encode("123456"));
    }
}
