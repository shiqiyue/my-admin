package com.github.shiqiyue.myadmin;

import java.util.ArrayList;
import java.util.List;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/12/012 17:08 
 **/
public class JvmTest1 {

    public int stackLevel = 1;

    public void stack() {
        stackLevel++;
        List<String> str = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            str.add(new String());
        }
        stack();
    }

    public static void main(String[] args) {
        JvmTest1 test1 = null;
        try {
            test1 = new JvmTest1();
            test1.stack();
        } catch (Exception e) {
            System.out.println("level:" + test1.stackLevel);
            throw e;
        }

    }
}
