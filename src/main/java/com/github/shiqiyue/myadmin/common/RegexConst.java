package com.github.shiqiyue.myadmin.common;

/***
 * 正则常用表达式
 * 
 * @author wwy
 *
 */
public class RegexConst {
	
	/** WORD */
	public static final String WORD = "\\w+";
}
