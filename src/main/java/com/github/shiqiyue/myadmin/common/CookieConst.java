package com.github.shiqiyue.myadmin.common;

/***
 * cookie名称
 * 
 * @author wwy
 *
 */
public class CookieConst {
	
	/** cookie-管理员记住密码 */
	public static final String COOKIE_ADMIN_REMEMBER_ME = "COOKIE_ADMIN_REMEMBER_ME";
	
}
