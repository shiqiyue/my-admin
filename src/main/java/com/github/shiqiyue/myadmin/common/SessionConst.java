package com.github.shiqiyue.myadmin.common;

/***
 * session名称
 * 
 * @author wwy
 *
 */
public class SessionConst {
	/** 后台 */
	/** session用的用户 */
	public static String SESSION_USER = "SESSION_USER";
	
	/** api */
	public static final String SESSION_API_MEMBER_INFO = "SESSION_API_MEMBER_INFO";
	
	public static final String SESSION_SMS_CODE_FORGET_PASSWORD = "SESSION_SMS_CODE_FORGET_PASSWORD";
	
	public static final String SESSION_SMS_CODE_REGISTE = "SESSION_SMS_CODE_REGISTE";
	
	public static final String SESSION_SMS_FIND_PAY_PASSWORD = "SESSION_SMS_FIND_PAY_PASSWORD";
	
	public static final String SESSION_CAPTCHA_BASE = "SESSION_CAPTCHA_BASE_";
}
