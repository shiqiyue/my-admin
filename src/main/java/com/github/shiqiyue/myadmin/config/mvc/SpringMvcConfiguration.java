package com.github.shiqiyue.myadmin.config.mvc;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.github.shiqiyue.myadmin.config.ApplicationConfig;
import com.github.shiqiyue.myadmin.config.mvc.interceptor.LoggerInterceptor;
import com.github.shiqiyue.myadmin.config.security.interceptor.AdminAuthenticationInterceptor;
import com.github.shiqiyue.myadmin.config.security.interceptor.AdminUserInfoInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/***
 * spring mvc 配置
 *
 * @author wwy
 *
 */
@Configuration
public class SpringMvcConfiguration implements WebMvcConfigurer {
	
	@Autowired
	private LoggerInterceptor loggerInterceptor;
	
	@Autowired
	private ApplicationConfig applicationConfig;
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {


    }
	
	@Override
	public void addFormatters(FormatterRegistry registry) {


    }
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		// stringhttp converter
		converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
		// json converter
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.IgnoreErrorGetter);
		FastJsonHttpMessageConverter jsonConverter = new FastJsonHttpMessageConverter();
		jsonConverter.setDefaultCharset(Charset.forName("UTF-8"));
		jsonConverter.setSupportedMediaTypes(
				Arrays.asList(MediaType.APPLICATION_JSON, new MediaType("application", "*+json")));
		jsonConverter.setFastJsonConfig(fastJsonConfig);
		converters.add(jsonConverter);
		
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new AdminAuthenticationInterceptor()).addPathPatterns("/admin/**")
				.excludePathPatterns("/admin/login/**", "/admin/logout/**");
		registry.addInterceptor(new AdminUserInfoInterceptor()).addPathPatterns("/admin/**")
				.excludePathPatterns("/admin/login/**", "/admin/logout/**");
		registry.addInterceptor(loggerInterceptor).addPathPatterns("/**");

    }
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/",
		// "/static/");
		// registry.addResourceHandler("/plugins/**").addResourceLocations("classpath:/plugins/",
		// "/plugins/");
		registry.addResourceHandler("/uploads/**")
				.addResourceLocations("file:" + applicationConfig.getUploadFileLocation());
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// 设置跨域支持
		// registry.addMapping("/api/**").allowedOrigins(consts.getCrosUrl()).allowedMethods("GET",
		// "POST")
		// .allowCredentials(true).maxAge(3600);
	}
	
	/****
	 * 跨域支持
	 *
	 * @return
	 */
	@Bean
	public FilterRegistrationBean corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/api/**", applicationConfig.getMvc().getCros());
		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}
	
	/**
	 * 文件上传配置
	 *
	 * @return
	 */
	// @Bean
	// public MultipartConfigElement multipartConfigElement() {
	// MultipartConfigFactory factory = new MultipartConfigFactory();
	// // 单个文件最大KB,MB
	// factory.setMaxFileSize("30MB");
	// /// 设置总上传数据总大小
	// factory.setMaxRequestSize("50MB");
	// return factory.createMultipartConfig();
	// }
	//
}
