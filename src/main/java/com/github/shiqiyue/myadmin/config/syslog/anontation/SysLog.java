package com.github.shiqiyue.myadmin.config.syslog.anontation;

import java.lang.annotation.*;

/****
 * 系统日志注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    /***
     * 功能名称
     * @return
     */
    String value() default "";

    /****
     * 是否忽略请求参数(不打印请求参数)
     * @return
     */
    boolean ignoreResquestParams() default false;

    /***
     * 是否忽略返回参数(不打印返回参数)
     * @return
     */
    boolean ignoreResponseParams() default false;
}