package com.github.shiqiyue.myadmin.config.kaptcha;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/***
 * 图形验证码配置
 *
 * @author wwy
 *
 */
@Configuration
public class KaptchaConfiguration {
	
	@Bean
	public Producer getProducer() {
		DefaultKaptcha kaptcha = new DefaultKaptcha();
		Properties properties = new Properties();
		properties.setProperty(Constants.KAPTCHA_BORDER, "yes");
		properties.setProperty(Constants.KAPTCHA_BORDER_THICKNESS, "1");
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR, "black");
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "10");
		properties.setProperty(Constants.KAPTCHA_IMAGE_WIDTH, "300");
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_SIZE, "90");
		properties.setProperty(Constants.KAPTCHA_IMAGE_HEIGHT, "130");
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "4");
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier");
        properties.setProperty(Constants.KAPTCHA_WORDRENDERER_IMPL, CustomWordRenderer.class.getName());
        properties.setProperty(Constants.KAPTCHA_NOISE_IMPL, SimpleNoiseProducer.class.getName());
		properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING,
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890");
		Config config = new Config(properties);
		kaptcha.setConfig(config);
		return kaptcha;
	}
	
}