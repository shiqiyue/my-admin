package com.github.shiqiyue.myadmin.config;

import org.springframework.web.cors.CorsConfiguration;

import lombok.Data;

/***
 * spring mvc配置信息
 * 
 * @author wwy shiqiyue.github.com
 *
 */
@Data
public class MvcConfig {
	
	/***
	 * cros配置信息
	 */
	private CorsConfiguration cros = new CorsConfiguration();
	
}
