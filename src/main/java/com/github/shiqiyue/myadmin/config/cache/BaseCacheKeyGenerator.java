package com.github.shiqiyue.myadmin.config.cache;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;

/***
 * 自定义 缓存 key 生成器
 * 
 * @author wwy
 *
 */
public class BaseCacheKeyGenerator implements KeyGenerator {
	
	@Override
	public Object generate(Object target, Method method, Object... params) {
		Object key = new BaseCacheKey(target, method, params);
		return key.toString();
	}
	
}