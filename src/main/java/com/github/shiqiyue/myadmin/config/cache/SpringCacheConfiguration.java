package com.github.shiqiyue.myadmin.config.cache;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Configuration;

/***
 * spring cache 配置类
 * 
 * @author wwy
 *
 */
@EnableCaching
@Configuration
public class SpringCacheConfiguration extends CachingConfigurerSupport {
	
	@Override
	public KeyGenerator keyGenerator() {
		return new BaseCacheKeyGenerator();
	}
	
}
