package com.github.shiqiyue.myadmin.config.druid;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.github.shiqiyue.myadmin.config.ApplicationConfig;
import com.github.shiqiyue.myadmin.config.DruidConfig;

/***
 * druid 配置类
 * 
 * @author wwy
 *
 */
@Configuration
public class DruidConfiguration {
	
	@Autowired
	private ApplicationConfig applicationConfig;
	
	private static final Logger LOG = LoggerFactory.getLogger(DruidConfiguration.class);
	
	@Bean
	public ServletRegistrationBean druidServlet() {
		ServletRegistrationBean reg = new ServletRegistrationBean();
		reg.setServlet(new StatViewServlet());
		reg.addUrlMappings("/druid/*");
		reg.addInitParameter("loginUsername", "druid");
		reg.addInitParameter("loginPassword", "sztlaiju");
		return reg;
	}
	
	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions",
				"*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*,*.jsp,*.woff");
		filterRegistrationBean.addInitParameter("profileEnable", "true");
		filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
		filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
		return filterRegistrationBean;
	}
	
	@Bean
	@Primary
	public DataSource druidDataSource() {
		DruidConfig druidConfig = applicationConfig.getDruid();
		DruidDataSource datasource = new DruidDataSource();
		
		datasource.setUrl(druidConfig.getUrl());
		datasource.setUsername(druidConfig.getUsername());
		datasource.setPassword(druidConfig.getPassword());
		datasource.setDriverClassName(druidConfig.getDriverClassName());
		datasource.setInitialSize(druidConfig.getInitialSize());
		datasource.setMinIdle(druidConfig.getMinIdle());
		datasource.setMaxActive(druidConfig.getMaxActive());
		datasource.setMaxWait(druidConfig.getMaxWait());
		datasource.setTimeBetweenEvictionRunsMillis(druidConfig.getTimeBetweenEvictionRunsMillis());
		datasource.setMinEvictableIdleTimeMillis(druidConfig.getMinEvictableIdleTimeMillis());
		datasource.setValidationQuery(druidConfig.getValidationQuery());
		datasource.setTestWhileIdle(druidConfig.isTestWhileIdle());
		datasource.setTestOnBorrow(druidConfig.isTestOnBorrow());
		datasource.setTestOnReturn(druidConfig.isTestOnReturn());
		datasource.setMaxOpenPreparedStatements(druidConfig.getMaxOpenPreparedStatements());
		datasource.setRemoveAbandoned(druidConfig.isRemoveAbandoned());
		datasource.setRemoveAbandonedTimeout(druidConfig.getRemoveAbandonedTimeout());
		datasource.setLogAbandoned(druidConfig.isLogAbandoned());
		datasource.setTimeBetweenLogStatsMillis(druidConfig.getTimeBetweenLogStatsMillis());
		try {
			datasource.setFilters(druidConfig.getFilters());
		} catch (SQLException e) {
			LOG.error("druid configuration initialization filter", e);
		}
		return datasource;
	}
	
}
