package com.github.shiqiyue.myadmin.config.security.anontation;

import java.lang.annotation.*;

/****
 * 类全局 权限配置
 *
 * @author wwy shiqiyue.github.com
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthorityConfig {

    /***
     * 权限前缀
     *
     * @return 返回权限前缀
     */
    String prefix();

}
