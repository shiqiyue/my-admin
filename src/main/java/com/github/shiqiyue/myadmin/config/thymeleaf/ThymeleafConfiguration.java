package com.github.shiqiyue.myadmin.config.thymeleaf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.dialect.IDialect;

import com.github.shiqiyue.myadmin.config.thymeleaf.dialect.MySecurityDialect;

@Configuration
public class ThymeleafConfiguration {
	
	private static final Logger log = LoggerFactory.getLogger(ThymeleafConfiguration.class);
	
	@Bean
	public IDialect mySecurityDialect() {
		return new MySecurityDialect();
	}
}
