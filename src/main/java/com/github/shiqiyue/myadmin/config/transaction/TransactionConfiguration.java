package com.github.shiqiyue.myadmin.config.transaction;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/***
 * 数据库事务配置类
 * @Author: shiqiyue
 * @Date: 创建于2018/4/16/016 9:28 
 **/
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class TransactionConfiguration {

}
