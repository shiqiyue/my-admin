package com.github.shiqiyue.myadmin.config;

/***
 * 安全配置信息
 * 
 * @author wwy shiqiyue.github.com
 *
 */
public class SecurityConfig {
	
	/***
	 * 权限前缀放到httpserveltrequest中的属性名称
	 */
	public static final String AUTHORITY_PREFIX_ATTRIBUTE_NAME = "authorityPrefix";
}
