package com.github.shiqiyue.myadmin.config.aspect;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/***
 * aspect 配置类
 * @Author: shiqiyue
 * @Date: 创建于2018/4/16/016 9:27 
 **/
@Configuration
@EnableAspectJAutoProxy
public class AspectConfiguration {
}
