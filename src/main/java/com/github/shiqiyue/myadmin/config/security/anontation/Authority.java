package com.github.shiqiyue.myadmin.config.security.anontation;

import java.lang.annotation.*;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Authority {

    /***
     * 权限名称
     */
    String name();
	
}
