package com.github.shiqiyue.myadmin.config.security.interceptor;

import com.alibaba.fastjson.JSON;
import com.github.shiqiyue.myadmin.config.SecurityConfig;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.util.security.SecurityUtils;
import com.github.shiqiyue.myadmin.util.web.AjaxUtils;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.github.shiqiyue.myadmin.vo.rep.base.ResultCode;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/****
 * 后台-权限认证
 * 
 * @author wwy
 *
 */

public class AdminAuthenticationInterceptor extends HandlerInterceptorAdapter {


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		if (!(handler instanceof HandlerMethod)) {
			return true;
		}
		HandlerMethod method = (HandlerMethod) handler;
		AuthorityConfig authorityConfig = AnnotationUtils.findAnnotation(method.getBean().getClass(),
				AuthorityConfig.class);
		Authority authority = AnnotationUtils.findAnnotation(method.getMethod(), Authority.class);
		if (authorityConfig == null) {
			return true;
		}
		// 先验证菜单权限
		boolean canAccess = SecurityUtils.hasAuthority(authorityConfig.prefix());
		if (!canAccess) {
			authorityRrror(request, response);
			return false;
		}
		if (authority == null) {
			return true;
		}
		// 验证按钮权限
		canAccess = SecurityUtils.hasAuthority(authorityConfig.prefix(), authority.name());
		if (!canAccess) {
			authorityRrror(request, response);
			return false;
		}

		// 保存权限前缀
		request.setAttribute(SecurityConfig.AUTHORITY_PREFIX_ATTRIBUTE_NAME, authorityConfig.prefix());
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}
	
	private void authorityRrror(HttpServletRequest request, HttpServletResponse response) {
		if (AjaxUtils.isAjaxRequest(request) || AjaxUtils.isAjaxUploadRequest(request)) {
			response.setContentType("application/json;charset=UTF-8");
            RepBase<String> rep = new RepBase<>();
            rep.setCode(ResultCode.AUTHORITY_ERROR).setMes(ResultCode.AUTHORITY_ERROR.getMes());
			try {
                response.getWriter().print(JSON.toJSONString(rep));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				response.sendError(403);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
