package com.github.shiqiyue.myadmin.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/18/018 9:47 
 **/
@Configuration
public class PasswordEncoderConfiguration {

    @Bean("adminPasswordEncoder")
    public PasswordEncoder passwordEncorder() {
        return new BCryptPasswordEncoder();
    }
}
