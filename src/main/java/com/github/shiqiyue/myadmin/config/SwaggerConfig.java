package com.github.shiqiyue.myadmin.config;

/***
 * swagger配置信息
 * 
 * @author wwy
 *
 */
public class SwaggerConfig {
	/****
	 * 是否启用swagger
	 */
	private Boolean enable = false;
	/***
	 * title
	 */
	private String title = "";
	/***
	 * describe
	 */
	private String desc = "";
	/***
	 * version
	 */
	private String version = "";
	
	public Boolean isEnable() {
		return enable;
	}
	
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
}
