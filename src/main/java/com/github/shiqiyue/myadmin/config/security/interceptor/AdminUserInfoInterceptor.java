package com.github.shiqiyue.myadmin.config.security.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.github.shiqiyue.myadmin.util.security.SecurityUtils;

public class AdminUserInfoInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (!(handler instanceof HandlerMethod)) {
			return;
		}
		if (modelAndView == null) {
			return;
		}
		modelAndView.addObject("sessionUser", SecurityUtils.getSessionUser());
	}
	
}
