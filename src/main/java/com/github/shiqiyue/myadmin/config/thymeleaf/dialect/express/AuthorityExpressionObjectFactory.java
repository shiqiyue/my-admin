package com.github.shiqiyue.myadmin.config.thymeleaf.dialect.express;

import java.util.LinkedHashSet;
import java.util.Set;

import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import com.github.shiqiyue.myadmin.util.security.SecurityUtils;

public class AuthorityExpressionObjectFactory implements IExpressionObjectFactory {
	
	private static final String OBJECT_NAME = "auths";
	
	private static final SecurityUtils UTILS = new SecurityUtils();
	
	@Override
	public Set<String> getAllExpressionObjectNames() {
		Set<String> objectNames = new LinkedHashSet<>(1);
		objectNames.add(OBJECT_NAME);
		return objectNames;
	}
	
	@Override
	public Object buildObject(IExpressionContext context, String expressionObjectName) {
		if (OBJECT_NAME.equals(expressionObjectName)) {
			return UTILS;
		}
		return null;
	}
	
	@Override
	public boolean isCacheable(String expressionObjectName) {
		return true;
	}
	
}
