package com.github.shiqiyue.myadmin.config.thymeleaf.dialect;

import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.thymeleaf.dialect.express.AuthorityExpressionObjectFactory;
import com.github.shiqiyue.myadmin.config.thymeleaf.dialect.processor.AuthorityListProcessor;
import com.github.shiqiyue.myadmin.config.thymeleaf.dialect.processor.AuthorityProcessor;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.dialect.IProcessorDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import java.util.LinkedHashSet;
import java.util.Set;

/***
 * 安全dialect
 */
public class MySecurityDialect extends AbstractDialect implements IProcessorDialect, IExpressionObjectDialect {
	
	private static final String NAME = "CustomSecurity";
	private static final String DEFAULT_PREFIX = "securitys";
	private static final int PROCESSOR_PRECEDENCE = 900;
	
	public MySecurityDialect() {
		super(NAME);
	}
	
	@Override
	public String getPrefix() {
		return DEFAULT_PREFIX;
	}
	
	@Override
	public int getDialectProcessorPrecedence() {
		return PROCESSOR_PRECEDENCE;
	}
	
	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processors = new LinkedHashSet<IProcessor>(6);
		processors.add(
				new AuthorityProcessor(TemplateMode.HTML, DEFAULT_PREFIX, AuthorityCommon.ADD, AuthorityCommon.ADD));
		processors.add(new AuthorityProcessor(TemplateMode.HTML, DEFAULT_PREFIX, AuthorityCommon.DELETE,
				AuthorityCommon.DELETE));
		processors.add(
				new AuthorityProcessor(TemplateMode.HTML, DEFAULT_PREFIX, AuthorityCommon.EDIT, AuthorityCommon.EDIT));
		processors.add(
				new AuthorityProcessor(TemplateMode.HTML, DEFAULT_PREFIX, AuthorityCommon.LIST, AuthorityCommon.LIST));
		processors.add(new AuthorityProcessor(TemplateMode.HTML, DEFAULT_PREFIX, AuthorityCommon.EXPORT_EXCEL,
				AuthorityCommon.EXPORT_EXCEL));
		processors.add(new AuthorityListProcessor(TemplateMode.HTML, DEFAULT_PREFIX, "printAll"));
		return processors;
	}
	
	@Override
	public IExpressionObjectFactory getExpressionObjectFactory() {
		return new AuthorityExpressionObjectFactory();
	}
	
}
