package com.github.shiqiyue.myadmin.config;

/***
 * 系统日志（后台请求日志）配置信息
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 10:44 
 **/
public class SyslogConfig {

    /***
     * 是否开启
     */
    private Boolean enable = true;

    /***
     * 请求参数最大长度，超过则不处理
     */
    private Integer maxRequsetParamLength = 1000;


    /***
     * 返回参数最大长度，超过则不处理
     */
    private Integer maxResponseParamLength = 1000;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Integer getMaxResponseParamLength() {
        return maxResponseParamLength;
    }

    public void setMaxResponseParamLength(Integer maxResponseParamLength) {
        this.maxResponseParamLength = maxResponseParamLength;
    }

    public Integer getMaxRequsetParamLength() {
        return maxRequsetParamLength;
    }

    public void setMaxRequsetParamLength(Integer maxRequsetParamLength) {
        this.maxRequsetParamLength = maxRequsetParamLength;
    }
}
