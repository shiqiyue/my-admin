package com.github.shiqiyue.myadmin.config.thymeleaf.dialect.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.standard.processor.AbstractStandardConditionalVisibilityTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

import com.github.shiqiyue.myadmin.util.security.SecurityUtils;

public class AuthorityProcessor extends AbstractStandardConditionalVisibilityTagProcessor {
	
	private static final Integer PRECEDENCE = 400;
	
	private String authority;
	
	public AuthorityProcessor(TemplateMode templateMode, String dialectPrefix, String attrName, String authority) {
		super(templateMode, dialectPrefix, attrName, PRECEDENCE);
		this.authority = authority;
	}
	
	@Override
	protected boolean isVisible(ITemplateContext context, IProcessableElementTag tag, AttributeName attributeName,
			String attributeValue) {
		return SecurityUtils.auth(authority);
	}
	
}
