package com.github.shiqiyue.myadmin.config.syslog;

import com.github.shiqiyue.myadmin.config.ApplicationConfig;
import com.github.shiqiyue.myadmin.config.syslog.aspect.SysLogAspect;
import com.github.shiqiyue.myadmin.service.system.syslog.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 11:01 
 **/
@ConditionalOnProperty(prefix = "application.syslog", value = "enable", havingValue = "true", matchIfMissing = false)
@Configuration
public class SyslogConfiguration {

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private SysLogService sysLogService;

    @Bean
    public SysLogAspect sysLogAspect() {
        SysLogAspect sysLogAspect = new SysLogAspect();
        sysLogAspect.setSysLogService(sysLogService);
        sysLogAspect.setMaxRequsetParamLength(applicationConfig.getSyslog().getMaxRequsetParamLength());
        sysLogAspect.setMaxResponseParamLength(applicationConfig.getSyslog().getMaxResponseParamLength());
        return sysLogAspect;
    }


}
