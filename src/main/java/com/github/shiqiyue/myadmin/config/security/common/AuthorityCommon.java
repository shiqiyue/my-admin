package com.github.shiqiyue.myadmin.config.security.common;

/**
 * 通用权限
 * 
 * @author wwy shiqiyue.github.com
 *
 */
public class AuthorityCommon {
	/*********** 通用权限 start *********/
	/***
	 * 权限标识-添加
	 */
	public static final String ADD = "add";
	
	/***
	 * 权限标识-修改
	 */
	public static final String EDIT = "edit";
	
	/***
	 * 权限标识-列表
	 */
	public static final String LIST = "list";
	
	/***
	 * 权限标识-删除
	 */
	public static final String DELETE = "delete";
	
	/***
	 * 权限标识-导出excel
	 */
	public static final String EXPORT_EXCEL = "exportExcel";
	/*********** 通用权限 end *********/
	
	/***
	 * 权限分隔符
	 */
	public static final String DELIMITER = ":";
	
}
