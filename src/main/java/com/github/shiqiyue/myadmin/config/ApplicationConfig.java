package com.github.shiqiyue.myadmin.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

/***
 * 应用配置信息
 * 
 * @author wwy shiqiyue.github.com
 *
 */
@Data
@Component
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {
	
	/**
	 * 应用名称
	 */
	private String name = "sys-template";
	
	/***
	 * 包的基本路径
	 */
	private String basePackage = "com.szt.sy";
	
	/***
	 * 生成器是否直接生成文件到生成路径
	 */
	private Boolean generateFile = true;
	
	/***
	 * 上传文件目录
	 */
	private String uploadFileLocation = "";
	
	/***
	 * 文件服务器链接前缀
	 */
	private String fileServerPrefix = "";
	
	/***
	 * 数据源配置信息
	 */
	@NestedConfigurationProperty
	private DruidConfig druid = new DruidConfig();
	
	/***
	 * mvc配置信息
	 */
	@NestedConfigurationProperty
	private MvcConfig mvc = new MvcConfig();
	
	/***
	 * swagger配置信息
	 */
	@NestedConfigurationProperty
	private SwaggerConfig swagger = new SwaggerConfig();

	/***
	 * 系统日志配置信息
	 */
	@NestedConfigurationProperty
	private SyslogConfig syslog = new SyslogConfig();
	
}
