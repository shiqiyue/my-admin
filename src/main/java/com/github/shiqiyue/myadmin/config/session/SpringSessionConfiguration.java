package com.github.shiqiyue.myadmin.config.session;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.config.annotation.web.http.EnableSpringHttpSession;

/***
 * spring session配置
 *
 * @author wwy
 *
 */
@Configuration
@EnableSpringHttpSession
public class SpringSessionConfiguration {
	
}
