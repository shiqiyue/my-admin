package com.github.shiqiyue.myadmin.entity.system;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;

/***
 * 地区
 * 
 * @author wwy
 *
 */
@Data
@Alias("Area")
@TableName(value = "sys_area")
public class Area implements Serializable {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/** 启用 */
	public static final Integer LOCK_ON = 1;
	/** 禁用 */
	public static final Integer LOCK_OFF = 0;
	/** 类型-省 */
	public static final Integer TYPE_PROVINCE = 1;
	/** 类型-市 */
	public static final Integer TYPE_CITY = 2;
	/** 类型-区 */
	public static final Integer TYPE_AREA = 3;
	
	private Long id;
	
	/** 名称 */
	private String name;
	/** 类型:1:省,2-市,3-区 */
	private Integer type;
	/** 上级id */
	private Long pid;
	/** 是否启用 */
	private Integer isLock;
	
	/***
	 * 获得类型（中文）
	 *
	 * @return
	 */
	public String getTypeStr() {
		if (type.intValue() == TYPE_PROVINCE.intValue()) {
			return "省";
		} else if (type.intValue() == TYPE_CITY.intValue()) {
			return "市";
		} else if (type.intValue() == TYPE_AREA.intValue()) {
			return "区";
		}
		return "";
	}
	
}