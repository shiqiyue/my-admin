package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 15:14 
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role_commonly_authority")
public class RoleCommonlyAuthority extends BaseUuidEntity {

    private String roleId;

    private String authorityId;

    @TableField(exist = false)
    private String authorityName;
}
