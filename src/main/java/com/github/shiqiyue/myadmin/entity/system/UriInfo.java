package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/***
 * uri信息
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_uri_info")
public class UriInfo extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** uri */
	private String uri;
	/** 备注 */
	private String remark;
	/** 分组id */
	private String groupId;
	
	/** 访问次数 */
	private Long requestCount;
	/** 总的请求时间 */
	private Long requestTimeMillis;
	
	/** 最大并发量 */
	private Long concurrentMax;
	/** 上次访问时间 */
	private Date lastAccessTime;
	/** 分组名称 */
	@TableField(exist = false)
	private String groupName;
	
}