package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/***
 * 角色
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Alias("Role")
@TableName(value = "sys_role")
public class Role extends BaseUuidEntity {
	
	/** 根节点id(不存在的，当做顶层角色的父节点) */
	public static final String ROLE_ROOT_ID = "0";


    /**
     * 第一层的layer
     */
    public static final Short FIRST_LAYER = 1;
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**角色名称*/
	private String roleName;
    /**上级角色id*/
	private String parentId;


    /** 首页地址 */
	private String indexUrl;
	/** 是否可以删除 */
	private Short delAble;
	
	public static class DelAble {
		public static Short NO = -1;
		public static Short YES = 1;
	}
}
