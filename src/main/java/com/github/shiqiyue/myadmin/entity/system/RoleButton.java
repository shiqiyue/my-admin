package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wwy
 *
 *         <p>
 *         说明：角色-按钮权限<br/>
 *         创建时间：2018-03-14
 *         </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role_button")
public class RoleButton extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 角色Id */
	private String roleId;
	/** 菜单Id */
	private Long menuId;
	/** 按钮Id */
	private String buttonId;
	
	/***
	 * 菜单链接
	 */
	@TableField(exist = false)
	private String menuUrl;
	
	/***
	 * 按钮权限名称
	 */
	@TableField(exist = false)
	private String buttonAuthority;
	
}