package com.github.shiqiyue.myadmin.entity.base;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;

import java.io.Serializable;
import java.util.Date;

/***
 * 自定义id
 *
 * @author wwy
 *
 */
public abstract class BaseEntity implements Serializable {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

    @TableField(fill = FieldFill.INSERT)
	private Long version;
	
	@TableField(fill = FieldFill.INSERT)
	private Date addDate;
	
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date editDate;
	
	public Long getVersion() {
		return version;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}
	
	public Date getAddDate() {
		return addDate;
	}
	
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	
	public Date getEditDate() {
		return editDate;
	}
	
	public void setEditDate(Date editDate) {
		this.editDate = editDate;
	}
	
}
