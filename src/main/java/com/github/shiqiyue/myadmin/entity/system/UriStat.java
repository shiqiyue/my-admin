package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/***
 * uri请求状况
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_uri_stat")
public class UriStat extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 请求uri */
	private String uri;
	/** 最大并发数量 */
	private Long concurrentMax;
	/** 请求次数 */
	private Long requestCount;
	/** 总的请求时间 */
	private Long requestTimeMillis;
	/** 错误次数 */
	private Long errorCount;
	/** 上次访问时间 */
	private Date lastAccessTime;
	/** jdbc提交次数 */
	private Long jdbcCommitCount;
	/** idbc回滚次数 */
	private Long jdbcRollbackCount;
	/** jdbc执行次数 */
	private Long jdbcExecuteCount;
	/** jdbc执行错误次数 */
	private Long jdbcExecuteErrorCount;
	/** idbc执行峰值 */
	private Long jdbcExecutePeak;
	/** jdbc执行时间 */
	private Long jdbcExecuteTimeMillis;
	/** jdbc获取的数据行数 */
	private Long jdbcFetchRowCount;
	/** jdbc获取数据行数峰值 */
	private Long jdbcFetchRowPeak;
	/** jdbc更新次数 */
	private Long jdbcUpdateCount;
	/** jdbc更新峰值 */
	private Long jdbcUpdatePeak;
	/** jdbc连接池打开次数 */
	private Long jdbcPoolConnectionOpenCount;
	/** jdbc连接池关闭次数 */
	private Long jdbcPoolConnectionCloseCount;
	/** jdbc结果集打开次数 */
	private Long jdbcResultSetOpenCount;
	/** jdbc结果集关闭次数 */
	private Long jdbcResultSetCloseCount;
	/** 直方图 */
	private String histogram;
	/** 简介 */
	private String profiles;
	/** 开始统计时间 */
	private Date startDate;
	/** 结束统计时间 */
	private Date endDate;
	
}