package com.github.shiqiyue.myadmin.entity.common;

import java.util.List;

/***
 * 树节点
 * 
 * @author wwy
 *
 */
public class TreeNode {
	
	private Long id;
	
	/** 父级id */
	private Long pid;
	/** 名称 */
	private String name;
	
	private List<TreeNode> nodes;
	
	private boolean checked;
	
	private String url;
	
	private String target;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getPid() {
		return pid;
	}
	
	public void setPid(Long pid) {
		this.pid = pid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<TreeNode> getNodes() {
		return nodes;
	}
	
	public void setNodes(List<TreeNode> nodes) {
		this.nodes = nodes;
	}
	
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public String getTarget() {
		return target;
	}
	
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
}
