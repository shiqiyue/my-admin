package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 系统图片
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_system_image")
public class SystemImage extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 类型 */
	private Long type;
	/** 图片地址 */
	private String photoUrl;
	/** 排序 */
	private Short sort;
	/** 跳转链接 */
	private String url;
	
	private String title;
	
	@TableField(exist = false)
	private ImageType imageType;
	
}