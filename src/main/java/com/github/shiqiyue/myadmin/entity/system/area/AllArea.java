package com.github.shiqiyue.myadmin.entity.system.area;

import java.util.List;

import lombok.Data;

/***
 * 所有区域信息
 * 
 * @author wwy
 *
 */
@Data
public class AllArea {
	
	private List<ProvinceInfo> provinces;
	
}
