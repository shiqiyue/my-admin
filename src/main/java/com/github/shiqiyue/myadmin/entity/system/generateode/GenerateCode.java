package com.github.shiqiyue.myadmin.entity.system.generateode;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author wwy
 * <p>
 * <p>说明：代码生成器<br/>
 * 创建时间：2018-03-20</p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_generate_code")
public class GenerateCode extends BaseUuidEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
    private String tableName;

    /***
     * 模块名称
     */
    private String moduleName;

    /***
     * 实体名称
     */
    private String entityName;

    /***
     * 是否生成控制器
     */
    private Boolean generateController;

    /***
     * 是否生成服务类接口
     */
    private Boolean generateSerivceInteface;

    /****
     * 是否生成html
     */
    private Boolean generateHtml;

    /***
     * 是否生成添加功能的代码
     */
    private Boolean generateAddFunctionCode;

    /***
     * 是否生成修改功能的代码
     */
    private Boolean generateEditFunctionCode;

    /***
     * 是否生成删除功能的代码
     */
    private Boolean generateDeleteFunctionCode;

    /***
     * 是否生成列表查询功能的代码
     */
    private Boolean generateListFunctionCode;

    /***
     * 是否生成导出excel功能的代码
     */
    private Boolean generateExportExcelFunctionCode;


    /***
     * 是否直接生成文件到工程中
     */
    private Boolean generateFile;

    /***
     * 直接生成文件到工程的过程中，是否覆盖已存在的文件
     */
    private Boolean overwriteFile;


}