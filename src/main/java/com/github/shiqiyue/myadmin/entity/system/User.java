package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/***
 * 用户
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Alias("User")
@TableName(value = "sys_user")
public class User extends BaseUuidEntity {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/** 管理员id */
	public static final String USER_ID = "1";
	
	/** 用户名 */
    private String username;
	/** 密码 */
	private String password;

    /** 姓名 */
	private String nickname;
	/** 角色id */
	private String roleId;
	/** 最后登录时间 */
	private Date lastLogin;
	/** 用户登录ip地址 */
	private String ip;
	/** 备注 */
	private String remarks;
	/** email */
	private String email;
	/** 电话 */
	private String phone;
	/**照片地址*/
	private String photoUrl;
	/** 是否删除 */
	private Boolean del;
	/** 是否启用 */
	private Boolean enable;
	/** 是否可编辑 */
	private Boolean editable;
	/** 是否拥有所有权限 */
	private Boolean hasAllAuthority;
	
	/** 角色对象 */
	@TableField(exist = false)
	private Role role;
	
}
