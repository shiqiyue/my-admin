package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 系统文档
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_document")
public class Document extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 备注 */
	private String remark;
	/** 内容 */
	private String content;
	/** 标题 */
	private String title;
	
}