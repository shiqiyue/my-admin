package com.github.shiqiyue.myadmin.entity.common;

/***
 * 编辑状态
 * 
 * @author wwy
 *
 */
public class Editable {
	/** 可编辑 */
	public static final Short YES = 1;
	
	/** 不可编辑 */
	public static final Short NO = -1;
}
