package com.github.shiqiyue.myadmin.entity.system.generateode;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author wwy
 * <p>
 * <p>说明：代码生成器-列<br/>
 * 创建时间：2018-03-20</p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_generate_code_column")
public class Column extends BaseUuidEntity {
    private static final long serialVersionUID = 1L;

    /***
     * 列名
     */
    private String columnName;

    /***
     * 属性名称
     */
    private String attributeName;

    /***
     * 数据库类型
     */
    private String dbType;

    /***
     * 属性类型
     */
    private String attributeType;

    /***
     * 是否显示在列表中
     */
    private Boolean listShow;

    /***
     * 列表显示是否的formatter
     */
    private String listFormatter;

    /***
     * 添加或者修改是否是否显示
     */
    private Boolean editShow;

    /***
     * 修改页面使用的插件
     */
    private String editPlugin;

}