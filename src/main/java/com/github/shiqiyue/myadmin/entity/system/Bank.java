package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 银行
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_bank")
public class Bank extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 名称 */
	private String name;
	/** 图片地址 */
	private String photoUrl;
	
}