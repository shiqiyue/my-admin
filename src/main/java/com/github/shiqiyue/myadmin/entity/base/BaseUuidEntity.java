package com.github.shiqiyue.myadmin.entity.base;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/***
 * 实体基类，id生成方式为uuid方式
 *
 * @author wwy
 *
 */
public abstract class BaseUuidEntity extends BaseEntity {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	/***
	 * id
	 */
	@TableId(value = "ID", type = IdType.UUID)
	private String id;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
}
