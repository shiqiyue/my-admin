package com.github.shiqiyue.myadmin.entity.base;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/***
 * idWork id
 *
 * @author wwy
 *
 */
public abstract class BaseIdWorkEntity extends BaseEntity {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@TableId(value = "ID", type = IdType.ID_WORKER)
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
}
