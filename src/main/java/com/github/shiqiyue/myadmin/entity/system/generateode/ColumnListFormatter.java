package com.github.shiqiyue.myadmin.entity.system.generateode;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author wwy
 * <p>
 * <p>说明：代码生成器-列表formatter<br/>
 * 创建时间：2018-03-20</p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_generate_code_column_listformatter")
public class ColumnListFormatter extends BaseUuidEntity {
    private static final long serialVersionUID = 1L;

    /***
     * 名称
     */
    private String name;
    /***
     * 备注
     */
    private String remarks;

    /***
     * 额外信息
     */
    private String extra;


}