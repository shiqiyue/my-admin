package com.github.shiqiyue.myadmin.entity.system.area;

import java.util.List;

import lombok.Data;

/***
 * 市信息
 * 
 * @author wwy
 *
 */
@Data
public class CityInfo {
	
	private String name;
	
	private Long id;
	
	private List<AreaInfo> areas;
	
}
