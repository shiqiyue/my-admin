package com.github.shiqiyue.myadmin.entity.blog;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
*  @author wwy
*
*  <p>说明：测试<br/>
*  创建时间：2018-02-01</p>
*/
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "test_test")
public class Test extends BaseUuidEntity{
	private static final long serialVersionUID = 1L;
	
	/** dd */
	private String dfg;

	
	
	
}