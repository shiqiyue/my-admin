package com.github.shiqiyue.myadmin.entity.common;

/****
 * 启用状态
 * 
 * @author wwy
 *
 */
public class Enable {
	/** 启用 */
	public static final Short YES = 1;
	
	/** 不启用 */
	public static final Short NO = -1;
}
