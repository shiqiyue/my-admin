package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 系统日志
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_sys_log")
public class SysLogRecord extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 操作 */
	private String operation;
	/** 方法 */
	private String method;
	/** 请求的参数 */
	private String params;
	/** 返回的信息 */
	private String response;
	
	/** ip地址 */
	private String ip;
	/** 用户名 */
	private String username;
	/** 会员id */
	private String userId;
	/** 执行时间 */
	private Long time;
	
}