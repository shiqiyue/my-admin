package com.github.shiqiyue.myadmin.entity.blog;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wwy
 * <p>
 * <p>
 * 说明：文章 <br/>
 * 创建时间：2017-12-14
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "blog_article")
public class Article extends BaseUuidEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 热门-否
     */
    public static final Short HOT_NO = -1;
    /**
     * 热门-是
     */
    public static final Short HOT_YES = 1;

    /**
     * 标题
     */
    private String title;
    /**
     * 简介
     */
    private String brief;
    /**
     * 内容
     */
    private String content;
    /**
     * 作者编号
     */
    private String authorId;
    /**
     * 作者名称
     */
    private String authorName;
    /**
     * 标签
     */
    private String tags;
    /**
     * 查看次数
     */
    private Integer views;
    /**
     * 评价次数
     */
    private Integer comments;

    private Short hot;

    private String img;

}