package com.github.shiqiyue.myadmin.entity.base;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;

/***
 * 自增id
 *
 * @author wwy
 *
 */
public abstract class BaseAutoIncEntity extends BaseEntity {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@TableId(value = "ID", type = IdType.AUTO)
	private Long id;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
}
