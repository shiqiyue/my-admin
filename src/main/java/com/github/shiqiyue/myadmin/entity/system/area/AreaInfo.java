package com.github.shiqiyue.myadmin.entity.system.area;

import lombok.Data;

/***
 * 区信息
 * 
 * @author wwy
 *
 */
@Data
public class AreaInfo {
	
	private String name;
	
	private Long id;
	
}
