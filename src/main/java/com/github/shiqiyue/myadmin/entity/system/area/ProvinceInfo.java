package com.github.shiqiyue.myadmin.entity.system.area;

import java.util.List;

import lombok.Data;

/***
 * 省信息
 * 
 * @author wwy
 *
 */
@Data
public class ProvinceInfo {
	
	private String name;
	
	private Long id;
	
	private List<CityInfo> citys;
	
}
