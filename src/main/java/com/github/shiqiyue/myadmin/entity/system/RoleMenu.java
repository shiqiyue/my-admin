package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wwy
 *
 *         <p>
 *         说明：角色-菜单权限<br/>
 *         创建时间：2018-03-14
 *         </p>
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_role_menu")
public class RoleMenu extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 角色 */
	private String roleId;
	/** 菜单id */
	private Long menuId;
	
	/***
	 * 菜单链接
	 */
	@TableField(exist = false)
	private String menuUrl;
	
}