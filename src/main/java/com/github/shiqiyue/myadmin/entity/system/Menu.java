package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.util.List;

/***
 * 菜单
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Alias("Menu")
@TableName(value = "sys_menu")
public class Menu extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 根节点id */
	public static final String ROOT_ID = "0";
	/** 空链接 */
	public static final String EMPTY_URL = "#";
	/** 默认图标 */
	public static final String DEFAULT_ICON = "fa fa-leaf ";

	
	/** 菜单名称 */
	private String name;
	/** 链接 */
	private String url;
	/** 上级菜单ID */
	private String parentId;
	/** 排序 */
	private Integer sort;
	/** 图标 */
	private String icon;


	/** 是否拥有这个菜单的权限 */
	@TableField(exist = false)
    private Boolean hasAuthority = false;

	/***
	 * 子菜单
	 */
	@TableField(exist = false)
	private List<Menu> sons;


}
