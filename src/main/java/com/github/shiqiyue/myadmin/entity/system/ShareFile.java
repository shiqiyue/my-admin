package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 共享文件
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_share_file")
public class ShareFile extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	/** 文件名 */
	private String filename;
	/** 文件地址 */
	private String fileurl;
	/** 备注 */
	private String remark;
	
}