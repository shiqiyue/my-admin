package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 图片类型(用于系统的图片上传，如轮播等)
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_image_type")
public class ImageType extends BaseUuidEntity {
	private static final long serialVersionUID = 1L;
	
	public static Long TYPE_INDEX_BANNER = 1L;
	
	/** 类型 */
	private Long type;
	/** 文件上传上限,不限制则设置为-1 */
	private Integer max;
	/** 备注 */
	private String remark;
	
	/** 长除以宽的比率 */
	private Double rate;
	
}