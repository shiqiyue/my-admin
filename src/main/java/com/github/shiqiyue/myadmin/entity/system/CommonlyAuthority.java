package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 一般权限
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 14:25 
 **/

@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_commonly_authority")
public class CommonlyAuthority extends BaseUuidEntity {

    /***
     * 名称
     */
    private String name;

    /***
     * 权限名称
     */
    private String authorityName;

    /***
     * 备注
     */
    private String remarks;


    /***
     * 是否拥有权限
     */
    @TableField(exist = false)
    private Boolean own;

}
