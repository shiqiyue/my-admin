package com.github.shiqiyue.myadmin.entity.system.generateode;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 14:16 
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_generate_code_column_plugin")
public class ColumnPlugin extends BaseUuidEntity {

    /***
     * 姓名
     */
    private String name;

    /***
     * 备注
     */
    private String remarks;

    /***
     * 元素模板
     */
    private String elTemplate;

}
