package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/***
 * 按钮
 *
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_button")
public class Button extends BaseUuidEntity {



    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 名称
     */
    private String name;
    /**
     * 权限标识
     */
    private String authorityName;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 是否可以修改
     */
    private Boolean editable;


}
