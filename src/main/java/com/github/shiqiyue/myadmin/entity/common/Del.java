package com.github.shiqiyue.myadmin.entity.common;

/***
 * 删除状态
 *
 * @author wwy
 *
 */
public class Del {
	
	/** 已删除 */
	public static final Short YES = 1;
	
	/** 未删除 */
	public static final Short NO = -1;
}
