package com.github.shiqiyue.myadmin.entity;

import com.github.shiqiyue.myadmin.entity.system.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/***
 * 存放在session中的用户信息，后台管理系统使用
 * 
 * @author wwy
 *
 */
@Data
public class SessionUser implements Serializable, UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 293933992940142727L;
	
	/***
	 * 用户信息
	 */
	private User user;
	
	/** 登录信息hash */
	private Integer loginInfoHash;

	/**
	 * 菜单json
	 */
	private String menuListJson;
	
	/***
	 * 权限
	 */
	private List<SimpleGrantedAuthority> authorities;
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return user.getPassword();
	}
	
	@Override
	public String getUsername() {
		return user.getUsername();
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return !user.getDel();
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return user.getEnable();
	}
	
}
