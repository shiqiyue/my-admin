package com.github.shiqiyue.myadmin.entity.system;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.github.shiqiyue.myadmin.entity.base.BaseUuidEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/***
 * 代码生成
 * 
 * @author wwy
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Alias("Createcode")
@TableName(value = "sys_createcode")
public class Createcode extends BaseUuidEntity {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/** 包名 */
	private String packageName;
	/** 类名 */
	private String objectName;
	/** 表名 */
	private String tableName;
	/** 属性集 */
	private String fieldlist;
	/** 描述 */
	private String describe;
	
	/** id类型 */
	private Short idType;
	/** 作者 */
	private String author;
	
	/** 表前缀 */
	@TableField(exist = false)
	private String tabletop;
	/** 属性总数 **/
	@TableField(exist = false)
	private Integer zindex;
	
}
