package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.UriGroup;

/***
 * uri分组mapper
 * 
 * @author wwy
 *
 */
public interface UriGroupMapper extends BaseMapper<UriGroup> {
	
	/***
	 * uri组-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<UriGroup> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * uri组-列表
	 * 
	 * @param map
	 * @return
	 */
	public List<UriGroup> listAll(@Param("map") Map<String, Object> map);
	
}
