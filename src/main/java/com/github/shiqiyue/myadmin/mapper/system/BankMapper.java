package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Bank;

/***
 * 银行mapper
 * 
 * @author wwy
 *
 */
public interface BankMapper extends BaseMapper<Bank> {
	
	/***
	 * 银行分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Bank> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 银行列表
	 * 
	 * @param map
	 * @return
	 */
	public List<Bank> listAll(@Param("map") Map<String, Object> map);
	
}
