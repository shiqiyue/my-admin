package com.github.shiqiyue.myadmin.mapper.blog;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.blog.Test;

/**
*  @author wwy
*
*  <p>说明：测试 mapper<br/>
*  创建时间：2018-02-01</p>
*/
public interface TestMapper extends BaseMapper<Test>{

	/***
	 * 分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Test> dataList(Pagination page, @Param("map") Map<String, Object> map);

	/***
	 * 列表
	 * 
	 * @param map
	 * @return
	 */
	public List<Test> dataList(@Param("map") Map<String, Object> map);

}
