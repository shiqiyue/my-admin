package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.SysLogRecord;

/***
 * 系统日志mapper
 * 
 * @author wwy
 *
 */
public interface SysLogMapper extends BaseMapper<SysLogRecord> {
	
	/***
	 * 系统日志-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<SysLogRecord> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 系统日志-列表
	 * 
	 * @param map
	 * @return
	 */
	public List<SysLogRecord> listAll(@Param("map") Map<String, Object> map);
	
}
