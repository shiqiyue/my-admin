package com.github.shiqiyue.myadmin.mapper.system.generatecode;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnPlugin;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 14:42 
 **/
public interface ColumnPluginMapper extends BaseMapper<ColumnPlugin> {
    /***
     * 分页
     * @param page
     * @param map
     * @return
     */
    List<ColumnPlugin> dataList(Pagination page, @Param("map") Map<String, Object> map);

    /***
     * 列表
     *
     * @param map
     * @return
     */
    public List<ColumnPlugin> dataList(@Param("map") Map<String, Object> map);

}
