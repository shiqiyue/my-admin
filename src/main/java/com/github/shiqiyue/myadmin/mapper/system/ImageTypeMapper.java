package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.ImageType;

/***
 * 图片类型mapper
 * 
 * @author wwy
 *
 */
public interface ImageTypeMapper extends BaseMapper<ImageType> {
	
	/***
	 * 图片类型-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<ImageType> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 图片类型-列表
	 * 
	 * @param map
	 * @return
	 */
	public List<ImageType> listAll(@Param("map") Map<String, Object> map);
	
}
