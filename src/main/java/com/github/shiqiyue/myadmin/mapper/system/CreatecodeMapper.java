package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Createcode;

/***
 * 代码生成mapper
 * 
 * @author wwy
 *
 */
public interface CreatecodeMapper extends BaseMapper<Createcode> {
	
	/***
	 * 代码生成分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Createcode> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
}
