package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Menu;
import com.github.shiqiyue.myadmin.entity.system.RoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wwy
 *
 *         <p>
 *         说明：角色-菜单权限 mapper<br/>
 *         创建时间：2018-03-14
 *         </p>
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
	
	/***
	 * 分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<RoleMenu> dataList(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 列表
	 * 
	 * @param map
	 * @return
	 */
	public List<RoleMenu> dataList(@Param("map") Map<String, Object> map);
	
	/***
	 * 列表（包含菜单链接）
	 * 
	 * @param queryParam
	 * @return 菜单
	 */
	public List<RoleMenu> selectMoreInfo(@Param("map") Map<String, Object> queryParam);

	/***
	 * 获取角色拥有的菜单
	 * @param roleId
	 * @return
	 */
	public List<Menu> selectAuthMenuByRoleId(@Param("roleId") String roleId);

}
