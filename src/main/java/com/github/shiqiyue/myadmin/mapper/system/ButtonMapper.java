package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Button;

/***
 * 按钮mapper
 * 
 * @author wwy
 *
 */
public interface ButtonMapper extends BaseMapper<Button> {
	
	/***
	 * 按钮分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Button> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 按钮列表
	 * 
	 * @param map
	 * @return
	 */
	public List<Button> listAll(@Param("map") Map<String, Object> map);
}
