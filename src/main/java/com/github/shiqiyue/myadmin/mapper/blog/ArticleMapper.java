package com.github.shiqiyue.myadmin.mapper.blog;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.blog.Article;

/***
 * 文章 mapper
 * 
 * @author wwy
 *
 */
public interface ArticleMapper extends BaseMapper<Article> {
	
	/****
	 * 文章列表-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Article> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 文章列表
	 * 
	 * @param map
	 * @return
	 */
	public List<Article> listAll(@Param("map") Map<String, Object> map);
	
	/***
	 * 递增浏览数
	 * 
	 * @param id
	 */
	public void increaseViews(@Param("id") Long id);
	
	/***
	 * 递增评论数
	 * 
	 * @param id
	 */
	public void increaseComments(@Param("id") Long id);
	
}
