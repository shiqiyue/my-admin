package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.UriStat;

/***
 * UriStat mapper
 * 
 * @author wwy
 *
 */
public interface UriStatMapper extends BaseMapper<UriStat> {
	
	/***
	 * UriStat page
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<UriStat> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * UriStat list
	 * 
	 * @param map
	 * @return
	 */
	public List<UriStat> listAll(@Param("map") Map<String, Object> map);
	
}
