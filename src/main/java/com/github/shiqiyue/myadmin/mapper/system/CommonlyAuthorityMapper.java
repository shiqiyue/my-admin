package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.CommonlyAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 14:27 
 **/
public interface CommonlyAuthorityMapper extends BaseMapper<CommonlyAuthority> {


    /***
     * 分页
     * @param page
     * @param map
     * @return
     */
    List<CommonlyAuthority> dataList(Pagination page, @Param("map") Map<String, Object> map);

    /***
     * 列表
     *
     * @param map
     * @return
     */
    public List<CommonlyAuthority> dataList(@Param("map") Map<String, Object> map);
}
