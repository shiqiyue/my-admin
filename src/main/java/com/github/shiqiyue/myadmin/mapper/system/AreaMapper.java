package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Area;
import com.github.shiqiyue.myadmin.entity.system.area.ProvinceInfo;

/***
 * 地区mapper
 * 
 * @author wwy
 *
 */
public interface AreaMapper extends BaseMapper<Area> {
	
	/***
	 * 地区分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Area> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 所有地区
	 * 
	 * @return
	 */
	public List<ProvinceInfo> listAll();
	
}
