package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Document;

/****
 * 文档mapper
 * 
 * @author wwy
 *
 */
public interface DocumentMapper extends BaseMapper<Document> {
	
	/***
	 * 文档分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<Document> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 文档列表
	 * 
	 * @param map
	 * @return
	 */
	public List<Document> listAll(@Param("map") Map<String, Object> map);
	
}
