package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/***
 * 用户mapper
 * 
 * @author wwy
 *
 */
public interface UserMapper extends BaseMapper<User> {
	
	/***
	 * 用户-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<User> datalistPage(Pagination page, @Param("map") Map<String, Object> map);


}
