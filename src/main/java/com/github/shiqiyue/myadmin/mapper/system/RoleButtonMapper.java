package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.Menu;
import com.github.shiqiyue.myadmin.entity.system.RoleButton;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wwy
 * <p>
 * <p>
 * 说明：角色-按钮权限 mapper<br/>
 * 创建时间：2018-03-14
 * </p>
 */
public interface RoleButtonMapper extends BaseMapper<RoleButton> {

    /***
     * 分页
     *
     * @param page
     * @param map
     * @return
     */
    public List<RoleButton> dataList(Pagination page, @Param("map") Map<String, Object> map);

    /***
     * 列表
     *
     * @param map
     * @return
     */
    public List<RoleButton> dataList(@Param("map") Map<String, Object> map);

    /***
     * 按钮权限列表，返回内容包含按钮权限标志和菜单链接
     *
     * @param map
     * @return
     */
    public List<RoleButton> selectMoreInfo(@Param("map") Map<String, Object> map);


    /***
     * 通过角色id和按钮id获取权限信息
     * @param roleId
     * @param buttonId
     * @return
     */
    public List<Menu> selectAuthsByRoleIdAndButtonId(@Param("roleId") String roleId, @Param("buttonId") String buttonId);
}
