package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.UriInfo;

/***
 * uriInfo mapper
 * 
 * @author wwy
 *
 */
public interface UriInfoMapper extends BaseMapper<UriInfo> {
	
	/***
	 * uriInfo 分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<UriInfo> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/****
	 * uriInfo 列表
	 * 
	 * @param map
	 * @return
	 */
	public List<UriInfo> listAll(@Param("map") Map<String, Object> map);
	
}
