package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.SystemImage;

/***
 * 系统图片mapper
 * 
 * @author wwy
 *
 */
public interface SystemImageMapper extends BaseMapper<SystemImage> {
	
	/***
	 * 系统图片分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<SystemImage> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 系统图片列表
	 * 
	 * @param map
	 * @return
	 */
	public List<SystemImage> listAll(@Param("map") Map<String, Object> map);
	
}
