package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.shiqiyue.myadmin.entity.system.CommonlyAuthority;
import com.github.shiqiyue.myadmin.entity.system.RoleCommonlyAuthority;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 16:37 
 **/
public interface RoleCommonlyAuthorityMapper extends BaseMapper<RoleCommonlyAuthority> {

    public List<CommonlyAuthority> selectAuthsByRoleId(@Param("roleId") String roleId);

    public List<RoleCommonlyAuthority> selectMoreInfoByRoleId(@Param("roleId") String roleId);
}
