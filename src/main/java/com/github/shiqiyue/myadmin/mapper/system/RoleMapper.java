package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.shiqiyue.myadmin.entity.system.Role;

/***
 * 角色mapper
 * 
 * @author wwy
 *
 */
public interface RoleMapper extends BaseMapper<Role> {


}
