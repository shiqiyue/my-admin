package com.github.shiqiyue.myadmin.mapper.system;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.ShareFile;

/***
 * 共享文件mapper
 * 
 * @author wwy
 *
 */
public interface ShareFileMapper extends BaseMapper<ShareFile> {
	
	/***
	 * 共享文件-分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<ShareFile> datalistPage(Pagination page, @Param("map") Map<String, Object> map);
	
	/***
	 * 共享文件-列表
	 * 
	 * @param map
	 * @return
	 */
	public List<ShareFile> listAll(@Param("map") Map<String, Object> map);
	
}
