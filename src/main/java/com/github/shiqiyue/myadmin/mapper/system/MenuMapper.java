package com.github.shiqiyue.myadmin.mapper.system;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.github.shiqiyue.myadmin.entity.system.Menu;

/***
 * 菜单-mapper
 * 
 * @author wwy
 *
 */
public interface MenuMapper extends BaseMapper<Menu> {
	
}
