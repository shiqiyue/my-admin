package com.github.shiqiyue.myadmin.mapper.system.generatecode;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.generateode.GenerateCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
*  @author wwy
*
*  <p>说明：代码 mapper<br/>
*  创建时间：2018-03-20</p>
*/
public interface GenerateCodeMapper extends BaseMapper<GenerateCode>{

	/***
	 * 分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<GenerateCode> dataList(Pagination page, @Param("map") Map<String, Object> map);

	/***
	 * 列表
	 * 
	 * @param map
	 * @return
	 */
	public List<GenerateCode> dataList(@Param("map") Map<String, Object> map);

}
