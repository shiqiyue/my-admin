package com.github.shiqiyue.myadmin.mapper.system.generatecode;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnListFormatter;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wwy
 * <p>
 * <p>说明：代码生成器-列表formatter mapper<br/>
 * 创建时间：2018-03-20</p>
 */
public interface ColumnListFormatterMapper extends BaseMapper<ColumnListFormatter> {

    /***
     * 分页
     * @param page
     * @param map
     * @return
     */
    List<ColumnListFormatter> dataList(Pagination page, @Param("map") Map<String, Object> map);

    /***
     * 列表
     *
     * @param map
     * @return
     */
    public List<ColumnListFormatter> dataList(@Param("map") Map<String, Object> map);
}
