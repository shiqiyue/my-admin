package com.github.shiqiyue.myadmin.controller.system.generatecode;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnListFormatter;
import com.github.shiqiyue.myadmin.service.system.generatecode.ColumnListFormatterService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.github.shiqiyue.myadmin.vo.rep.base.RepPage;
import com.github.shiqiyue.myadmin.vo.req.page.ReqPageVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/***
 * 代码生成器-listformatter 控制器
 */
@Controller
@AuthorityConfig(prefix = "/admin/generatecode/listformatter/list")
@RequestMapping(value = "admin/generatecode/listformatter")
public class ColumnListFormatterController extends AdminBaseController {

    @Autowired
    private ColumnListFormatterService columnListFormatterService;

    /*
     *角色列表-页面
     * @return
     */
    @Authority(name = AuthorityCommon.LIST)
    @GetMapping("list")
    public String listPage() {
        return "pages/generatecode/listformatter/listformatter-list";
    }

    /***
     * 列表数据
     * @param pageVO
     * @param map
     * @return
     */
    @Authority(name = AuthorityCommon.LIST)
    @PostMapping("list")
    @ResponseBody
    public RepBase listData(ReqPageVO pageVO, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
        processParams(map);
        Page<ColumnListFormatter> page = processPage(pageVO);
        page = columnListFormatterService.list(page, map);
        RepPage<ColumnListFormatter> repPage = new RepPage<>(page.getRecords(), page.getTotal());
        return rep.setCommonSuccess().setData(repPage);
    }

    /***
     * 新增页面
     * @param model
     * @return
     */
    @Authority(name = AuthorityCommon.ADD)
    @GetMapping(value = "/add")
    public String toAdd(Model model) {
        model.addAttribute("entity", new ColumnListFormatter());
        return "pages/generatecode/listformatter/listformatter-edit";
    }

    /***
     * 新增操作
     * @param entity
     * @return
     */
    @SysLog("listformatter-添加")
    @Authority(name = AuthorityCommon.ADD)
    @PostMapping("add")
    @ResponseBody
    public RepBase add(ColumnListFormatter entity) {
        columnListFormatterService.insert(entity);
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 修改页面
     * @param id
     * @return
     */
    @Authority(name = AuthorityCommon.EDIT)
    @GetMapping("edit")
    public String toEdit(String id, Model model) {
        model.addAttribute("entity", columnListFormatterService.selectById(id));
        return "pages/generatecode/listformatter/listformatter-edit";
    }

    /***
     * 修改操作
     * @param entity
     * @return
     */
    @SysLog("listformatter-修改")
    @Authority(name = AuthorityCommon.EDIT)
    @PostMapping("edit")
    @ResponseBody
    public RepBase edit(ColumnListFormatter entity) {
        columnListFormatterService.updateById(entity);
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 删除操作
     * @param id
     * @return
     */
    @SysLog("listformatter-删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/del")
    @ResponseBody
    public RepBase deleteRole(@RequestParam String id) {
        RepBase rep = new RepBase<>();
        columnListFormatterService.deleteById(id);
        return rep.setCommonSuccess();
    }

    /**
     * 批量删除
     */
    @SysLog("listformatter-批量删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/batch/del")
    @ResponseBody
    public RepBase deleteAll(@RequestParam("ids[]") List<String> ids) {
        RepBase rep = new RepBase<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            columnListFormatterService.deleteBatchIds(ids);
        }
        return rep.setCommonSuccess();
    }
}
