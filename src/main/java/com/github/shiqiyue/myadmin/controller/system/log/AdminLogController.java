package com.github.shiqiyue.myadmin.controller.system.log;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/***
 * 日志文件-控制器
 * 
 * @author wwy
 *
 */
@Controller
@RequestMapping(value = "admin/log")
public class AdminLogController {
	
	final String menuUrl = "/admin/log/list";
	
	@GetMapping("list")
	public String list() {
		return "system/log/logs";
	}
}
