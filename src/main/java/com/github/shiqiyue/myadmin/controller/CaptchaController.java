package com.github.shiqiyue.myadmin.controller;

import com.github.shiqiyue.myadmin.service.KaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/***
 * 图形验证码控制器
 *
 * @author wwy
 *
 */
@Controller
@RequestMapping("/captcha")
public class CaptchaController {

    @Autowired
    private KaptchaService kaptchaService;

    /***
     * 获取图形验证码
     * @param time
     * @param type
     * @param httpSession
     * @param response
     * @throws Exception
     */
    @RequestMapping("get")
    public void handleRequest(@RequestParam(value = "time", required = false) Long time,
                              @RequestParam(value = "type", required = true) int type, HttpSession httpSession,
                              HttpServletResponse response) throws Exception {
        kaptchaService.get(type, httpSession, response);
    }
}
