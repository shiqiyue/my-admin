package com.github.shiqiyue.myadmin.controller.blog.article;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.blog.Article;
import com.github.shiqiyue.myadmin.entity.system.User;
import com.github.shiqiyue.myadmin.service.blog.article.ArticleService;
import com.github.shiqiyue.myadmin.util.excel.ObjectExcelView;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.github.shiqiyue.myadmin.vo.rep.base.RepPage;
import com.github.shiqiyue.myadmin.vo.req.page.ReqPageVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 文章-controller
 *
 * @author wwy
 *
 */
@AuthorityConfig(prefix = "/admin/article/list")
@Controller
@RequestMapping(value = "admin/article")
public class ArticleController extends AdminBaseController {
	
	@Autowired
	private ArticleService articleService;
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@GetMapping(value = "/list")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("pages/article/article-list");
		mv.addObject("queryParam", map);
		return mv;
	}

    /***
     * 列表数据
     * @param pageVO
     * @param map
     * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@PostMapping(value = "/list")
	@ResponseBody
    public RepBase list(ReqPageVO pageVO, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		Page<Article> page = processPage(pageVO);
		// 列出Article列表
        page = articleService.list(page, map);
        RepPage repPage = new RepPage(page.getRecords(), page.getTotal());
        return rep.setCommonSuccess().setData(repPage);
	}
	
	@Authority(name = AuthorityCommon.ADD)
	@GetMapping("add")
	public String toAddPage(Model model) {
		Article entity = new Article();
		model.addAttribute("entity", entity);
		return "pages/article/article-edit";
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@PostMapping(value = "/add")
	@ResponseBody
    public RepBase save(Article entity) {
        RepBase rep = new RepBase();
		User user = getLoginUser();
		entity.setAuthorId(user.getId());
		entity.setAuthorName(user.getNickname());
		entity.setViews(0);
		entity.setComments(0);
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		articleService.addRecord(entity);
        return rep.setCommonSuccess();
	}

    /***
     *  删除操作
     * @param id
     * @return
	 */
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value = "/del")
	@ResponseBody
    public RepBase delete(String id) {
		articleService.deleteRecord(id);
        return RepBase.newIntance().setCommonSuccess();
	}
	
	/**
	 * 批量删除
	 *
	 * @param @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value = "/batch/del")
	@ResponseBody
    public RepBase deleteAll(@RequestParam("ids[]") List<String> ids) {
        RepBase rep = new RepBase<>();
        if (CollectionUtils.isNotEmpty(ids)) {
			articleService.deleteBatchIds(ids);
		}
        return rep.setCommonSuccess();
	}
	
	@Authority(name = AuthorityCommon.EDIT)
	@GetMapping("edit")
    public String editPage(String id, Model model) {
		Article entitiy = articleService.selectById(id);
		model.addAttribute("entity", entitiy);
		return "pages/article/article-edit";
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value = "/edit")
	@ResponseBody
    public RepBase edit(Article entity) {
        RepBase rep = new RepBase<>();
		articleService.editRecord(entity);
        return rep.setCommonSuccess();
	}
	
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value = "/hot/change")
	@ResponseBody
    public RepBase changeHot(String id) {
		Article article = articleService.selectById(id);
		Article updateInfo = new Article();
		updateInfo.setId(id);
		if (article.getHot().equals(Article.HOT_NO)) {
			updateInfo.setHot(Article.HOT_YES);
		} else {
			updateInfo.setHot(Article.HOT_NO);
		}
		articleService.updateById(updateInfo);
        return RepBase.newIntance().setCommonSuccess();
	}
	
	/**
	 * 导出到excel
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EXPORT_EXCEL)
	@PostMapping(value = "/excel")
	public ModelAndView exportExcel(@RequestParam Map<String, Object> map) {
		ModelAndView mv = new ModelAndView();
		processParams(map);
		List<String> titles = new ArrayList<>();
		titles.add("标题");
		titles.add("简介");
		titles.add("内容");
		titles.add("作者编号");
		titles.add("作者名称");
		List<Article> entityList = articleService.listAll(map);
		List<List<String>> exportDatas = new ArrayList<>(entityList.size());
		int titleNum = titles.size();
		for (Article entity : entityList) {
			List<String> data = new ArrayList<>(titleNum);
			data.add(entity.getTitle());
			data.add(entity.getBrief());
			data.add(entity.getContent());
			data.add(entity.getAuthorId());
			data.add(entity.getAuthorName());
			exportDatas.add(data);
		}
		ObjectExcelView erv = new ObjectExcelView(titles, exportDatas);
		mv = new ModelAndView(erv);
		return mv;
	}


}
