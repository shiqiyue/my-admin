package com.github.shiqiyue.myadmin.controller.system.menu;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.Menu;
import com.github.shiqiyue.myadmin.service.system.menu.MenuService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/***
 * 菜单控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/menu/list")
@RequestMapping(value = "admin/menu")
public class MenuController extends AdminBaseController {
	
	@Autowired
	private MenuService menuService;
	
	/**
	 * 菜单列表页面
	 *
	 * @param model
	 * @return
	 */
	@Authority(name = AuthorityCommon.LIST)
	@GetMapping(value = "/list")
	public String listAllMenu(Model model) throws Exception {
		return "pages/menu/menu-list";
	}
	
	/**
	 * 新增页面
	 *
	 * @return
	 */
	@Authority(name = AuthorityCommon.ADD)
	@GetMapping("/add")
    public ModelAndView toAdd(String pid) throws Exception {
		ModelAndView mv = this.getModelAndView();
		if (pid == null) {
			pid = Menu.ROOT_ID;
		}
		Menu menu = new Menu();
		menu.setParentId(pid);
		mv.addObject("entity", menu);
		mv.setViewName("pages/menu/menu-edit");
		return mv;
	}
	
	/**
	 * 修改菜单页面
	 *
	 * @param
	 * @return
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@GetMapping("/edit")
    public ModelAndView toEdit(String id) throws Exception {
		ModelAndView mv = this.getModelAndView();
		Menu menu = menuService.selectById(id);
		mv.addObject("entity", menu);
		mv.setViewName("pages/menu/menu-edit");
		return mv;
	}
	
	/**
	 * 菜单列表数据
	 *
	 * @return
	 */
	@Authority(name = AuthorityCommon.LIST)
	@PostMapping("list")
	@ResponseBody
    public RepBase listData(String order, String sort) throws Exception {
        RepBase rep = new RepBase();
		EntityWrapper<Menu> entityWrapper = new EntityWrapper<>();
		if (StringUtils.isNotBlank(sort)) {
			if ("desc".equals(order)) {
				entityWrapper.orderBy(sort, false);
			} else {
				entityWrapper.orderBy(sort, true);
			}
		}
        rep.setData(menuService.selectList(entityWrapper));
        return rep.setCommonSuccess();
	}
	
	/**
	 * 新增操作
	 *
	 * @param menu
	 * @return
	 */
	@SysLog("菜单-添加")
	@Authority(name = AuthorityCommon.ADD)
	@PostMapping(value = "/add")
	@ResponseBody
    public RepBase add(Menu menu) throws Exception {
        RepBase rep = new RepBase<>();
		menu.setIcon(Menu.DEFAULT_ICON);
		if (StringUtils.isBlank(menu.getUrl())) {
			menu.setUrl(Menu.EMPTY_URL);
		}

		menuService.insert(menu);
        return rep.setCommonSuccess();
	}
	
	/***
	 * 删除操作
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@SysLog("菜单-删除")
	@Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/del")
	@ResponseBody
    public RepBase delete(String id) throws Exception {
        RepBase rep = new RepBase<>();
		// 判断是否有子菜单,如果有则不允许删除
		if (menuService.countByParentId(id) > 0) {
            return rep.setCommonFail().setMes("含有子菜单，请先删除子菜单");
		}
		menuService.deleteMenuById(id);
        return rep.setCommonSuccess();
	}
	
	/***
	 * 修改操作
	 * 
	 * @param menu
	 * @return
	 * @throws Exception
	 */
	@SysLog("菜单-修改")
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value = "/edit")
	@ResponseBody
    public RepBase edit(Menu menu) throws Exception {
        RepBase rep = new RepBase();
		menuService.edit(menu);
        return rep.setCommonSuccess();
	}
	
	/***
	 * 修改菜单icon页面
	 * 
	 * @param id
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@GetMapping(value = "/icon/edit")
    public String toEditicon(String id, Model model) throws Exception {
        model.addAttribute("entity", menuService.selectById(id));
		model.addAttribute("id", id);
		return "pages/menu/icon-edit";
	}
	
	/***
	 * 修改菜单icon操作
	 * 
	 * @param id
	 * @param icon
	 * @return
	 * @throws Exception
	 */
	@SysLog("菜单-图标修改")
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value = "/icon/edit")
    @ResponseBody
    public RepBase editIcon(String id, String icon) throws Exception {
		menuService.editIcon(id, icon);
        return RepBase.newIntance().setCommonSuccess();
	}
	
}
