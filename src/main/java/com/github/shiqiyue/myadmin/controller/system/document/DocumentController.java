package com.github.shiqiyue.myadmin.controller.system.document;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.Document;
import com.github.shiqiyue.myadmin.service.system.document.DocumentService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 系统文档-控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/document/goPage")
@RequestMapping(value = "admin/document")
public class DocumentController extends AdminBaseController {
	
	@Autowired
	private DocumentService documentService;
	
	/**
	 * 去新增页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/document/document_edit");
		Document entity = new Document();
		mv.addObject("msg", "save");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	@ResponseBody
    public RepBase save(Document entity) {
        RepBase rep = new RepBase<>();
		
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		documentService.insert(entity);
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		// 根据ID读取
		Document entity = documentService.selectById(id);
		mv.setViewName("system/document/document_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	@ResponseBody
    public RepBase edit(Document entity) {
        RepBase rep = new RepBase<>();
		documentService.updateById(entity);
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/document/document_list");
		mv.addObject("queryParam", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<Document> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		// 列出Document列表
		page = documentService.list(new Page<Document>(page.getCurrent(), page.getSize()), map);
		List<Document> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
