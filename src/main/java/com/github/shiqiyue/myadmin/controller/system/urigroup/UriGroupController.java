package com.github.shiqiyue.myadmin.controller.system.urigroup;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.UriGroup;
import com.github.shiqiyue.myadmin.service.system.urigroup.UriGroupService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * uri分组控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/urigroup/goPage")
@RequestMapping(value = "admin/urigroup")
public class UriGroupController extends AdminBaseController {
	
	@Autowired
	private UriGroupService urigroupService;
	
	/**
	 * 去新增页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/urigroup/urigroup_edit");
		UriGroup entity = new UriGroup();
		mv.addObject("msg", "save");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	public ModelAndView save(UriGroup entity) {
		ModelAndView mv = this.getModelAndView();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		urigroupService.insert(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 删除
	 *
	 * @param out
	 * @
	 */
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/delete")
	@ResponseBody
	public String delete(String id) {
		
		urigroupService.deleteById(id);
		return "success";
	}
	
	/**
	 * 批量删除
	 *
	 * @param @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/deleteAll")
	@ResponseBody
    public RepBase deleteAll(String ids) {
        RepBase rep = new RepBase<>();
		if (null != ids && !"".equals(ids)) {
			String[] idArray = ids.split(",");
			urigroupService.deleteBatchIds(Lists.newArrayList(idArray));
			
		}
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		// 根据ID读取
		UriGroup entity = urigroupService.selectById(id);
		mv.setViewName("system/urigroup/urigroup_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(UriGroup entity) {
		ModelAndView mv = this.getModelAndView();
		urigroupService.updateById(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/urigroup/urigroup_list");
		mv.addObject("queryParam", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<UriGroup> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		// 列出UriGroup列表
		page = urigroupService.list(new Page<UriGroup>(page.getCurrent(), page.getSize()), map);
		List<UriGroup> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
