package com.github.shiqiyue.myadmin.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.github.shiqiyue.myadmin.config.ApplicationConfig;
import com.github.shiqiyue.myadmin.entity.system.ImageType;
import com.github.shiqiyue.myadmin.service.system.imagetype.ImageTypeService;
import com.github.shiqiyue.myadmin.service.system.systemimage.SystemImageService;
import com.github.shiqiyue.myadmin.util.img.ImgUtils;
import com.github.shiqiyue.myadmin.util.web.FileUpload;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/***
 * 文件上传-controller
 * 
 * @author wwy
 *
 */
@Controller
@RequestMapping("admin/file")
public class AdminFileController {
	
	@Autowired
	private ApplicationConfig applicationConfig;
	
	@Autowired
	private SystemImageService systemImageService;
	
	@Autowired
	private ImageTypeService imageTypeService;

	/***
	 * 上传文件
	 * @param multipartFile 文件
	 * @param cropperData 裁剪数据，格式{x: "", y: "", height:"",width:""}
	 * @param compressQuailty 压缩质量，默认为1，大于等于1都不压缩
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "image/upload", method = RequestMethod.POST)
	@ResponseBody
    public RepBase uploadFile(@RequestPart(value = "upfile", required = false) MultipartFile multipartFile,
                              @RequestParam(value = "cropperData", required = false) String cropperData,
                              @RequestParam(value = "compressQuailty", defaultValue = "1") double compressQuailty) throws IOException {
        RepBase rep = new RepBase<>();
		if (multipartFile == null) {
            return rep.setCommonFail().setMes("请上传图片");
		}

		// 执行上传
		String fileName = FileUpload.fileUp(multipartFile, applicationConfig.getUploadFileLocation(),
				IdWorker.getId() + "");
		String filePath = applicationConfig.getUploadFileLocation() + fileName;
		// 如果含有裁剪数据则进行裁剪
		if (StringUtils.isNotBlank(cropperData)) {
			JSONObject object = JSON.parseObject(cropperData);
			int x = (int) Float.parseFloat(object.get("x").toString());
			int y = (int) Float.parseFloat(object.get("y").toString());
			int width = (int) Float.parseFloat(object.get("width").toString());
			int height = (int) Float.parseFloat(object.get("height").toString());
			ImgUtils.cut(new File(filePath), filePath, x, y, width, height);
		}
		//如果需要压缩则压缩
		if (compressQuailty < 1) {
			File file = new File(filePath);
			ImgUtils.compressFile1(file, compressQuailty, filePath);
		}
		Map<String, Object> responseData = new HashMap<>(3);
		//原文件名
		responseData.put("original", multipartFile.getOriginalFilename());
		//上传后的文件访问地址
		responseData.put("url", applicationConfig.getFileServerPrefix() + fileName);
		//文件名
		responseData.put("title", multipartFile.getOriginalFilename());
        rep.setCommonSuccess().setData(responseData);
        return rep;
	}

	/****
	 * 百度编辑器上传文件,不裁剪，不压缩
	 * @param multipartFile
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "ueditor/image/upload", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> ueditorUploadFile(@RequestParam(value = "upfile") MultipartFile multipartFile) throws IOException {
		// 执行上传
		String fileName = FileUpload.fileUp(multipartFile, applicationConfig.getUploadFileLocation(),
				IdWorker.getId() + "");
		String filePath = applicationConfig.getUploadFileLocation() + fileName;
		Map<String, Object> responseData = new HashMap<>(4);
		responseData.put("original", multipartFile.getOriginalFilename());
		responseData.put("url", applicationConfig.getFileServerPrefix() + fileName);
		responseData.put("title", multipartFile.getOriginalFilename());
		responseData.put("state", "SUCCESS");
		return responseData;
	}
	
	@RequestMapping(value = "systemImage/upload", method = RequestMethod.POST)
	@ResponseBody
    public RepBase systemImageUpload(@RequestPart(value = "upfile") MultipartFile multipartFile,
                                     @RequestParam(value = "avatar_data", required = false) String avatarData,
                                     @RequestParam(value = "compress", defaultValue = "true") boolean compress,
                                     @RequestParam(value = "type", required = true) Long type, String systemImageId) throws IOException {
        RepBase rep = new RepBase<>();
		// 如为新增图片，判断数量是否超过上限
		if (StringUtils.isBlank(systemImageId)) {
			ImageType imageType = imageTypeService.selectByType(type);
			Integer haveUploadNum = systemImageService.countByType(type);
			if (imageType.getMax() != -1 && haveUploadNum >= imageType.getMax()) {
                return rep.setCommonFail().setMes("上传数量超过上限，最多可上传" + imageType.getMax() + "张");
			}
		}
		
		Map<String, Object> retMes = new HashMap<>(4);
		// 执行上传
		String fileName = FileUpload.fileUp(multipartFile, applicationConfig.getUploadFileLocation(),
				IdWorker.getId() + "");
		String filePath = applicationConfig.getUploadFileLocation() + fileName;
		// 如果含有裁剪数据则进行裁剪
		if (StringUtils.isNotBlank(avatarData)) {
			JSONObject object = JSON.parseObject(avatarData);
			int x = (int) Float.parseFloat(object.get("x").toString());
			int y = (int) Float.parseFloat(object.get("y").toString());
			int width = (int) Float.parseFloat(object.get("width").toString());
			int height = (int) Float.parseFloat(object.get("height").toString());
			ImgUtils.cut(new File(filePath), filePath, x, y, width, height);
		}
		// 判断图片大小，并进行适当的压缩
		File file = new File(filePath);
		Long fileSize = file.length();
		if (compress && fileSize > 600 * 1024) {
			double quailty = 1;
			if (fileSize < 1000 * 1024) {
				quailty = 0.8;
			} else if (fileSize < 2000 * 1024) {
				quailty = 0.7;
			} else {
				quailty = 0.65;
			}
			ImgUtils.compressFile1(file, quailty, filePath);
		}
		retMes.put("original", multipartFile.getOriginalFilename());
		retMes.put("url", applicationConfig.getFileServerPrefix() + "/uploads/" + fileName);
		retMes.put("title", multipartFile.getOriginalFilename());
		// retMes.put("state", "SUCCESS");
        rep.setCommonSuccess().setData(retMes);
        return rep;
	}
	
}
