package com.github.shiqiyue.myadmin.controller.system.imagetype;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.ImageType;
import com.github.shiqiyue.myadmin.service.system.imagetype.ImageTypeService;
import com.github.shiqiyue.myadmin.service.system.systemimage.SystemImageService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 图片类型(用于系统的图片上传，如轮播等)-controller
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/imagetype/goPage")
@RequestMapping(value = "admin/imagetype")
public class ImageTypeController extends AdminBaseController {
	
	@Autowired
	private ImageTypeService imagetypeService;
	
	@Autowired
	private SystemImageService systemImageService;
	
	/**
	 * 去新增页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/imagetype/imagetype_edit");
		ImageType entity = new ImageType();
		mv.addObject("msg", "save");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@SysLog("图片类型-添加")
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	public ModelAndView save(ImageType entity) {
		ModelAndView mv = this.getModelAndView();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		imagetypeService.insert(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 删除
	 *
	 * @param out
	 * @
	 */
	@SysLog("图片类型-删除")
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/delete")
	@ResponseBody
	public String delete(String id) {
		
		imagetypeService.deleteById(id);
		return "success";
	}
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		ImageType entity = imagetypeService.selectById(id);
		mv.setViewName("system/imagetype/imagetype_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@SysLog("图片类型-修改")
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(ImageType entity) {
		ModelAndView mv = this.getModelAndView();
		imagetypeService.updateById(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/imagetype/imagetype_list");
		mv.addObject("pd", map);
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<ImageType> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		page = imagetypeService.list(new Page<ImageType>(page.getCurrent(), page.getSize()), map);
		List<ImageType> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
