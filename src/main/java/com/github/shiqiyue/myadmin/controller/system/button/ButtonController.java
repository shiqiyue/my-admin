package com.github.shiqiyue.myadmin.controller.system.button;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.Button;
import com.github.shiqiyue.myadmin.service.system.button.ButtonService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/***
 * 按钮管理-控制器
 *
 * @author wwy
 *
 */
@Controller
@RequestMapping("/admin/button")
@AuthorityConfig(prefix = "/admin/button/list")
public class ButtonController extends AdminBaseController {

    @Autowired
    private ButtonService buttonService;

    /***
     * 去列表页面
     * @return
     * @
     */
    @Authority(name = AuthorityCommon.LIST)
    @GetMapping("list")
    public String listPage() {
        return "pages/button/button-list";
    }

    /**
     * 去新增页面
     *
     * @param
     * @
     */
    @Authority(name = AuthorityCommon.ADD)
    @GetMapping("add")
    public String goAdd(Model model) {

        model.addAttribute("entity", new Button());
        return "pages/button/button-edit";
    }


    /**
     * 去修改页面
     *
     * @param
     * @
     */
    @Authority(name = AuthorityCommon.EDIT)
    @GetMapping("edit")
    public String goEdit(String id, Model model) {
        Button entity = buttonService.selectById(id);
        model.addAttribute("entity", entity);
        return "pages/button/button-edit";
    }

    /***
     * 列表数据
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Authority(name = AuthorityCommon.LIST)
    @PostMapping(value = "/list")
    @ResponseBody
    public RepBase list(String order, String sort) {
        RepBase rep = new RepBase<>();
        EntityWrapper<Button> entityWrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(sort)) {
            if ("desc".equals(order)) {
                entityWrapper.orderBy(sort, false);
            } else {
                entityWrapper.orderBy(sort, true);
            }
        }
        List<Button> list = buttonService.selectList(entityWrapper);
        return rep.setCommonSuccess().setData(list);
    }


    /***
     * 添加操作
     * @param button
     * @return 是否成功
     */
    @SysLog("按钮-添加")
    @Authority(name = AuthorityCommon.ADD)
    @PostMapping(value = "add")
    @ResponseBody
    public RepBase save(Button button) {
        button.setEditable(true);
        buttonService.insert(button);
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 删除操作
     * @param id
     * @return 是否成功
     */
    @SysLog("按钮-删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "del")
    @ResponseBody
    public RepBase delete(String id) {
        buttonService.delete(new EntityWrapper<Button>().eq("id", id).eq("editable", true));
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 修改操作
     * @param button
     * @return 是否成功
     */
    @SysLog("按钮-修改")
    @Authority(name = AuthorityCommon.EDIT)
    @PostMapping(value = "/edit")
    @ResponseBody
    public RepBase edit(Button button) {
        Button preButton = buttonService.selectById(button.getId());
        if (preButton.getEditable()) {
            buttonService.updateById(button);
        }
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 批量删除操作
     * @param ids
     * @return
     */
    @SysLog("按钮-批量删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/batch/del")
    @ResponseBody
    public RepBase deleteAll(@RequestParam(value = "ids[]") List<String> ids) {
        RepBase rep = new RepBase<>();
        buttonService.delete(new EntityWrapper<Button>().in("id", ids).eq("editable", true));
        return rep.setCommonSuccess();
    }


}
