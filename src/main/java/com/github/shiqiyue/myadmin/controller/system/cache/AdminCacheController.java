package com.github.shiqiyue.myadmin.controller.system.cache;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/***
 * 系统缓存控制器
 * 
 * @author wwy
 *
 */
@Controller
@RequestMapping("admin/cache")
public class AdminCacheController {
	
}
