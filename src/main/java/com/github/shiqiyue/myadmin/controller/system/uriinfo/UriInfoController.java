package com.github.shiqiyue.myadmin.controller.system.uriinfo;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.UriGroup;
import com.github.shiqiyue.myadmin.entity.system.UriInfo;
import com.github.shiqiyue.myadmin.service.system.urigroup.UriGroupService;
import com.github.shiqiyue.myadmin.service.system.uriinfo.UriInfoService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * uri信息 控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/uriinfo/goPage")
@RequestMapping(value = "admin/uriinfo")
public class UriInfoController extends AdminBaseController {
	
	@Autowired
	private UriInfoService uriinfoService;
	
	@Autowired
	private UriGroupService uriGroupService;
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		// 根据ID读取
		UriInfo entity = uriinfoService.selectById(id);
		List<UriGroup> uriGroupList = uriGroupService.listAll(Maps.newHashMap());
		mv.setViewName("system/uriinfo/uriinfo_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		mv.addObject("uriGroupList", uriGroupList);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(UriInfo entity) {
		ModelAndView mv = this.getModelAndView();
		uriinfoService.updateById(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/uriinfo/uriinfo_list");
		mv.addObject("queryParam", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<UriInfo> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		// 列出UriInfo列表
		page = uriinfoService.list(new Page<UriInfo>(page.getCurrent(), page.getSize()), map);
		List<UriInfo> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
