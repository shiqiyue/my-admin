package com.github.shiqiyue.myadmin.controller.system.systemimage;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.ImageType;
import com.github.shiqiyue.myadmin.entity.system.SystemImage;
import com.github.shiqiyue.myadmin.service.system.imagetype.ImageTypeService;
import com.github.shiqiyue.myadmin.service.system.systemimage.SystemImageService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 系统图片控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/systemimage/goPage")
@RequestMapping(value = "admin/systemimage")
public class SystemImageController extends AdminBaseController {
	
	@Autowired
	private SystemImageService systemimageService;
	
	@Autowired
	private ImageTypeService imageTypeService;
	
	/**
	 * 去新增页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/systemimage/systemimage_edit");
		SystemImage entity = new SystemImage();
		List<ImageType> imageTypes = imageTypeService.listAll(Maps.newHashMap());
		mv.addObject("msg", "save");
		mv.addObject("imageTypes", imageTypes);
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@SysLog("系统图片-添加")
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	public ModelAndView save(SystemImage entity) {
		ModelAndView mv = this.getModelAndView();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		systemimageService.insert(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 删除
	 *
	 * @param out
	 * @
	 */
	@SysLog("系统图片-删除")
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/delete")
	@ResponseBody
	public String delete(String id) {
		
		systemimageService.deleteById(id);
		return "success";
	}
	
	/**
	 * 批量删除
	 *
	 * @param @
	 */
	@SysLog("系统图片-批量删除")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/deleteAll")
	@ResponseBody
    public RepBase deleteAll(String ids) {
        RepBase rep = new RepBase<>();
		if (null != ids && !"".equals(ids)) {
			String[] idArray = ids.split(",");
			systemimageService.deleteBatchIds(Lists.newArrayList(idArray));
			
		}
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		// 根据ID读取
		SystemImage entity = systemimageService.selectById(id);
		List<ImageType> imageTypes = imageTypeService.listAll(Maps.newHashMap());
		mv.addObject("imageTypes", imageTypes);
		mv.setViewName("system/systemimage/systemimage_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@SysLog("系统图片-修改")
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(SystemImage entity) {
		ModelAndView mv = this.getModelAndView();
		systemimageService.updateById(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		List<ImageType> imageTypes = imageTypeService.listAll(Maps.newHashMap());
		mv.addObject("imageTypes", imageTypes);
		mv.setViewName("system/systemimage/systemimage_list");
		mv.addObject("pd", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<SystemImage> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		// 列出SystemImage列表
		page = systemimageService.list(new Page<SystemImage>(page.getCurrent(), page.getSize()), map);
		List<SystemImage> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
