package com.github.shiqiyue.myadmin.controller;

import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.service.system.area.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/***
 * 首页
 * 
 * @author wwy shiqiyue.github.com
 *
 */
@Controller
@RequestMapping
public class IndexController extends AdminBaseController {

	@Autowired
	private AreaService areaService;

	/***
	 * 首页跳转
	 * 
	 * @return
	 */
	@GetMapping
	public String index() {
		if (getLoginUser() != null) {
			return "redirect:/admin/main";
		}
		return "redirect:/admin/login";
		
	}

	/***
	 * 获取地址信息Json
	 * @return
	 */
	@RequestMapping("area/json")
	@ResponseBody
	public String getJson() {
		return areaService.listAllJson();
	}

}
