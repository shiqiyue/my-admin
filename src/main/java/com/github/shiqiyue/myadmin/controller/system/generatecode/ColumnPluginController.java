package com.github.shiqiyue.myadmin.controller.system.generatecode;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnPlugin;
import com.github.shiqiyue.myadmin.service.system.generatecode.ColumnPluginService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.github.shiqiyue.myadmin.vo.rep.base.RepPage;
import com.github.shiqiyue.myadmin.vo.req.page.ReqPageVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 14:25 
 **/
@Controller
@AuthorityConfig(prefix = "/admin/generatecode/plugin/list")
@RequestMapping(value = "admin/generatecode/plugin")
public class ColumnPluginController extends AdminBaseController {

    @Autowired
    private ColumnPluginService columnPluginService;

    /*
     *角色列表-页面
     * @return
     */
    @Authority(name = AuthorityCommon.LIST)
    @GetMapping("list")
    public String listPage() {
        return "pages/generatecode/plugin/plugin-list";
    }

    /***
     * 列表数据
     * @param pageVO
     * @param map
     * @return
     */
    @Authority(name = AuthorityCommon.LIST)
    @PostMapping("list")
    @ResponseBody
    public RepBase listData(ReqPageVO pageVO, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
        processParams(map);
        Page<ColumnPlugin> page = processPage(pageVO);
        page = columnPluginService.list(page, map);
        RepPage<ColumnPlugin> repPage = new RepPage<>(page.getRecords(), page.getTotal());
        return rep.setCommonSuccess().setData(repPage);
    }

    /***
     * 新增页面
     * @param model
     * @return
     */
    @Authority(name = AuthorityCommon.ADD)
    @GetMapping(value = "/add")
    public String toAdd(Model model) {
        model.addAttribute("entity", new ColumnPlugin());
        return "pages/generatecode/plugin/plugin-edit";
    }

    /***
     * 新增操作
     * @param entity
     * @return
     */
    @SysLog("column plugin-添加")
    @Authority(name = AuthorityCommon.ADD)
    @PostMapping("add")
    @ResponseBody
    public RepBase add(ColumnPlugin entity) {
        columnPluginService.insert(entity);
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 修改页面
     * @param id
     * @return
     */
    @Authority(name = AuthorityCommon.EDIT)
    @GetMapping("edit")
    public String toEdit(String id, Model model) {
        model.addAttribute("entity", columnPluginService.selectById(id));
        return "pages/generatecode/plugin/plugin-edit";
    }

    /***
     * 修改操作
     * @param entity
     * @return
     */
    @SysLog("column plugin-修改")
    @Authority(name = AuthorityCommon.EDIT)
    @PostMapping("edit")
    @ResponseBody
    public RepBase edit(ColumnPlugin entity) {
        columnPluginService.updateById(entity);
        return RepBase.newIntance().setCommonSuccess();
    }

    /***
     * 删除操作
     * @param id
     * @return
     */
    @SysLog("column plugin-删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/del")
    @ResponseBody
    public RepBase deleteRole(@RequestParam String id) {
        RepBase rep = new RepBase<>();
        columnPluginService.deleteById(id);
        return rep.setCommonSuccess();
    }

    /**
     * 批量删除
     */
    @SysLog("column plugin-批量删除")
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/batch/del")
    @ResponseBody
    public RepBase deleteAll(@RequestParam("ids[]") List<String> ids) {
        RepBase rep = new RepBase<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            columnPluginService.deleteBatchIds(ids);
        }
        return rep.setCommonSuccess();
    }
}
