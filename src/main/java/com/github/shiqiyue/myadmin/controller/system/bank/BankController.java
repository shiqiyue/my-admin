package com.github.shiqiyue.myadmin.controller.system.bank;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.Bank;
import com.github.shiqiyue.myadmin.service.system.bank.BankService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 银行-控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/bank/goPage")
@RequestMapping(value = "admin/bank")
public class BankController extends AdminBaseController {
	
	@Autowired
	private BankService bankService;
	
	/**
	 * 去新增页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/bank/bank_edit");
		Bank entity = new Bank();
		mv.addObject("msg", "save");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 *
	 * @param @
	 */
	@SysLog("银行-添加")
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	public ModelAndView save(Bank entity) {
		ModelAndView mv = this.getModelAndView();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		bankService.save(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 删除
	 *
	 * @param out
	 * @
	 */
	@SysLog("银行-删除")
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/delete")
	@ResponseBody
	public String delete(String id) {
		
		bankService.delete(id);
		return "success";
	}
	
	/**
	 * 批量删除
	 *
	 * @param @
	 */
	@SysLog("银行-批量删除")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/deleteAll")
	@ResponseBody
    public RepBase deleteAll(String ids) {
        RepBase rep = new RepBase<>();
		if (null != ids && !"".equals(ids)) {
			String[] idArray = ids.split(",");
			bankService.deleteBatch(Lists.newArrayList(idArray));
			
		}
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去修改页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		Bank entity = bankService.selectById(id);
		mv.setViewName("system/bank/bank_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 *
	 * @param @
	 */
	@SysLog("银行-修改")
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(Bank entity) {
		ModelAndView mv = this.getModelAndView();
		bankService.edit(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/bank/bank_list");
		mv.addObject("pd", map);
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<Bank> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		page = bankService.list(new Page<Bank>(page.getCurrent(), page.getSize()), map);
		List<Bank> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}

}
