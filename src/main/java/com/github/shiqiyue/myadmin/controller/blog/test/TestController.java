package com.github.shiqiyue.myadmin.controller.blog.test;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.common.RegexConst;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.BaseController;
import com.github.shiqiyue.myadmin.entity.blog.Test;
import com.github.shiqiyue.myadmin.service.blog.test.TestService;
import com.github.shiqiyue.myadmin.util.date.DateUtil;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.github.shiqiyue.myadmin.vo.req.page.ReqPageVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wwy
 *
 *         <p>
 *         说明：测试控制器<br/>
 *         创建时间：2018-02-01
 *         </p>
 */
@Controller
@AuthorityConfig(prefix = "/admin/test/list")
@RequestMapping(value = "admin/test")
public class TestController extends BaseController {
	
	/** 菜单地址(权限用) */
	final String menuUrl = "/admin/test/list";
	@Autowired
	private TestService testService;
	
	/**
	 * 去列表页面
	 * 
	 */
	@Authority(name = AuthorityCommon.LIST)
	@GetMapping(value = "/list")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("blog/test/test_list");
		mv.addObject("queryParam", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表数据
	 */
	@SysLog("查询测试列表数据")
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@PostMapping(value = "/list")
	@ResponseBody
    public RepBase list(ReqPageVO pageVO, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		Page<Test> page = processPage(pageVO);
		page = testService.list(page, map);
		List<Test> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}
	
	/**
	 * 去新增页面
	 */
	@Authority(name = AuthorityCommon.ADD)
	@GetMapping(value = "/add")
	public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("blog/test/test_edit");
		Test entity = new Test();
		mv.addObject("msg", "add");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	/**
	 * 保存
	 */
	@SysLog("添加测试数据")
	@Authority(name = AuthorityCommon.ADD)
	@PostMapping(value = "/add")
	@ResponseBody
    public RepBase save(Test entity) {
        RepBase rep = new RepBase();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		testService.insert(entity);
        return rep.setCommonSuccess();
	}
	
	/**
	 * 去修改页面
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@GetMapping(value = "/edit")
	public ModelAndView goEdit(String id) {
		ModelAndView mv = this.getModelAndView();
		Test entity = testService.selectById(id);
		mv.setViewName("blog/test/test_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		return mv;
	}
	
	/**
	 * 修改
	 */
	@SysLog("修改测试数据")
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value = "/edit")
	@ResponseBody
    public RepBase edit(Test entity) {
        RepBase rep = new RepBase<>();
		testService.updateById(entity);
        return rep.setCommonSuccess();
	}
	
	/**
	 * 删除
	 */
	@SysLog("删除测试数据")
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value = "/delete")
	@ResponseBody
    public RepBase delete(String id) {
		testService.deleteById(id);
        return RepBase.newIntance().setCommonSuccess();
	}
	
	/**
	 * 批量删除
	 */
	@SysLog("批量删除测试数据")
	@SuppressWarnings({ "rawtypes" })
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value = "/deleteAll")
	@ResponseBody
    public RepBase deleteAll(@RequestParam("ids[]") List<String> ids) {
        RepBase rep = new RepBase<>();
		if (null != ids && !ids.isEmpty()) {
			testService.deleteBatchIds(ids);
		}
        return rep.setCommonSuccess();
	}
	
	/***
	 * 处理输入参数
	 * 
	 * @param map
	 */
	private void processParams(Map<String, Object> map) {
		String sStartDate = (String) map.get("startDate");
		String sEndDate = (String) map.get("endDate");
		if (StringUtils.isBlank(sStartDate)) {
			map.put("startDate", null);
		} else {
			map.put("startDate", DateUtil.getStartTimeOfDate(sStartDate));
		}
		if (StringUtils.isBlank(sEndDate)) {
			map.put("endDate", null);
		} else {
			map.put("endDate", DateUtil.getEndTimeOfDate(sEndDate));
		}
		String sortDirection = (String) map.get("sortDirection");
		String sortColumn = (String) map.get("sortColumn");
		if (StringUtils.isNotBlank(sortColumn) && !sortColumn.matches(RegexConst.WORD)) {
			map.remove("sortColumn");
		}
		if (StringUtils.isNotBlank(sortDirection) && !sortDirection.matches(RegexConst.WORD)) {
			map.remove("sortDirection");
		}
	}
	
	/***
	 * 处理分页参数
	 * 
	 * @param pageVO
	 * @return
	 */
	private Page<Test> processPage(ReqPageVO pageVO) {
		Page<Test> page = new Page<>(pageVO.getCurrent(), pageVO.getSize());
		return page;
	}
	
}
