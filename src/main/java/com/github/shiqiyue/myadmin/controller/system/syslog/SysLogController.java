package com.github.shiqiyue.myadmin.controller.system.syslog;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.SysLogRecord;
import com.github.shiqiyue.myadmin.service.system.syslog.SysLogService;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 系统日志控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/syslog/goPage")
@RequestMapping(value = "admin/syslog")
public class SysLogController extends AdminBaseController {
	
	@Autowired
	private SysLogService syslogService;
	
	/**
	 * 去列表页面
	 *
	 * @param @
	 */
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/goPage")
	public ModelAndView goPage(@RequestParam Map<String, Object> map) {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/syslog/syslog_list");
		mv.addObject("queryParam", map);
		// 按钮权限
		
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@RequestMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<SysLogRecord> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
		processParams(map);
		// 列出SysLog列表
		page = syslogService.list(new Page<SysLogRecord>(page.getCurrent(), page.getSize()), map);
		List<SysLogRecord> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}


}
