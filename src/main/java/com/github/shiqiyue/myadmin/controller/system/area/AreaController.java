package com.github.shiqiyue.myadmin.controller.system.area;

import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.config.syslog.anontation.SysLog;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.Area;
import com.github.shiqiyue.myadmin.service.system.area.AreaService;
import com.github.shiqiyue.myadmin.util.id.generator.IDUtils;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 地区-控制器
 * 
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/area/list")
@RequestMapping(value = "admin/area")
public class AreaController extends AdminBaseController {
	
	@Autowired
	private AreaService areaService;
	
	/**
	 * 去列表页面
	 *
	 * @param
	 * @throws Exception
	 */
	@Authority(name = AuthorityCommon.LIST)
	@GetMapping(value = "/list")
	public ModelAndView goPage() throws Exception {
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("system/area/area_list");
		return mv;
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Authority(name = AuthorityCommon.LIST)
	@PostMapping(value = "/list")
	@ResponseBody
    public RepBase list(Page<Area> page, @RequestParam Map<String, Object> map) throws Exception {
        RepBase rep = new RepBase<>();
		if (StringUtils.isBlank((String) map.get("type"))) {
			map.put("type", Area.TYPE_PROVINCE);
		}
		page = areaService.list(new Page<Area>(page.getCurrent(), page.getSize()), map);
		List<Area> varList = page.getRecords();
		Map<String, Object> resultInfo = new HashMap<>(2);
		resultInfo.put("data", varList);
		resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}
	
	/**
	 * 保存
	 *
	 * @param
	 * @throws Exception
	 */
	@SysLog("地区-添加")
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/save")
	public ModelAndView save(Area entity) throws Exception {
		ModelAndView mv = this.getModelAndView();
		entity.setId(IDUtils.getAreaId());
		areaService.save(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 删除
	 *
	 * @param out
	 * @throws Exception
	 */
	@SysLog("地区-删除")
	@Authority(name = AuthorityCommon.DELETE)
	@RequestMapping(value = "/delete")
	@ResponseBody
	public String delete(Long id) throws Exception {
		
		areaService.delete(id);
		return "success";
	}
	
	/**
	 * 修改
	 *
	 * @param
	 * @throws Exception
	 */
	@SysLog("提现-修改")
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/edit")
	public ModelAndView edit(Area entity) throws Exception {
		ModelAndView mv = this.getModelAndView();
		areaService.edit(entity);
		mv.addObject("msg", "success");
		mv.setViewName("save_result");
		return mv;
	}
	
	/**
	 * 去新增页面
	 *
	 * @param
	 * @throws Exception
	 */
	@Authority(name = AuthorityCommon.ADD)
	@RequestMapping(value = "/goAdd")
	public ModelAndView goAdd(Long pid) throws Exception {
		ModelAndView mv = this.getModelAndView();
		Area pArea = areaService.findById(pid);
		
		mv.addObject("pArea", pArea);
		mv.setViewName("system/area/area_add");
		return mv;
	}
	
	/**
	 * 去修改页面
	 *
	 * @param
	 * @throws Exception
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@RequestMapping(value = "/goEdit")
	public ModelAndView goEdit(Long id, @RequestParam Map<String, Object> map) throws Exception {
		ModelAndView mv = this.getModelAndView();
		
		Area entity = areaService.findById(id);
		mv.setViewName("system/area/area_edit");
		mv.addObject("msg", "edit");
		mv.addObject("entity", entity);
		mv.addObject("pd", map);
		return mv;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
	}
}
