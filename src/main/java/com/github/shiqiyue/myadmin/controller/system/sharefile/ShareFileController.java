package com.github.shiqiyue.myadmin.controller.system.sharefile;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.github.shiqiyue.myadmin.config.ApplicationConfig;
import com.github.shiqiyue.myadmin.config.security.anontation.Authority;
import com.github.shiqiyue.myadmin.config.security.anontation.AuthorityConfig;
import com.github.shiqiyue.myadmin.config.security.common.AuthorityCommon;
import com.github.shiqiyue.myadmin.controller.base.AdminBaseController;
import com.github.shiqiyue.myadmin.entity.system.ShareFile;
import com.github.shiqiyue.myadmin.service.system.sharefile.ShareFileService;
import com.github.shiqiyue.myadmin.util.web.FileUpload;
import com.github.shiqiyue.myadmin.vo.rep.base.RepBase;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 共享文件控制器
 *
 * @author wwy
 *
 */
@Controller
@AuthorityConfig(prefix = "/admin/sharefile/list")
@RequestMapping(value = "admin/sharefile")
public class ShareFileController extends AdminBaseController {

    @Autowired
    private ShareFileService sharefileService;

    @Autowired
    private ApplicationConfig applicationConfig;

    /**
     * 去列表页面
     *
     * @param @
     */
    @Authority(name = AuthorityCommon.LIST)
    @GetMapping(value = "/list")
    public ModelAndView goPage(@RequestParam Map<String, Object> map) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("system/sharefile/sharefile_list");
        mv.addObject("queryParam", map);
        // 按钮权限

        return mv;
    }

    /**
     * 列表
     *
     * @param page
     * @
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Authority(name = AuthorityCommon.LIST)
    @PostMapping(value = "/list")
    @ResponseBody
    public RepBase list(Page<ShareFile> page, @RequestParam Map<String, Object> map) {
        RepBase rep = new RepBase<>();
        processParams(map);
        // 列出ShareFile列表
        page = sharefileService.list(new Page<ShareFile>(page.getCurrent(), page.getSize()), map);
        List<ShareFile> varList = page.getRecords();
        Map<String, Object> resultInfo = new HashMap<>(2);
        resultInfo.put("data", varList);
        resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
    }

    /**
     * 去新增页面
     *
     * @param @
     */
    @Authority(name = AuthorityCommon.ADD)
    @GetMapping(value = "/add")
    public ModelAndView goAdd(@RequestParam Map<String, Object> map) {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("system/sharefile/sharefile_edit");
        ShareFile entity = new ShareFile();
        mv.addObject("msg", "add");
        mv.addObject("entity", entity);
        mv.addObject("pd", map);
        return mv;
    }

    /**
     * 保存
     *
     * @param @
     */
    @Authority(name = AuthorityCommon.ADD)
    @PostMapping(value = "/add")
    @ResponseBody
    public RepBase save(ShareFile entity, @RequestPart(value = "uploadFile") MultipartFile multipartFile) {
        RepBase rep = new RepBase();
        if (multipartFile != null) {
            // 执行上传
            String fileName = FileUpload.fileUp(multipartFile, applicationConfig.getUploadFileLocation(),
                    IdWorker.getId() + "");
            String fileUrl = applicationConfig.getFileServerPrefix() + "/uploads/" + fileName;
            entity.setFileurl(fileUrl);
        }

        entity.setVersion(1L);
        entity.setAddDate(new Date());
        sharefileService.insert(entity);
        return rep.setCommonSuccess();
    }

    /**
     * 去修改页面
     *
     * @param @
     */
    @Authority(name = AuthorityCommon.EDIT)
    @GetMapping(value = "/edit")
    public ModelAndView goEdit(Long id) {
        ModelAndView mv = this.getModelAndView();
        // 根据ID读取
        ShareFile entity = sharefileService.selectById(id);
        mv.setViewName("system/sharefile/sharefile_edit");
        mv.addObject("msg", "edit");
        mv.addObject("entity", entity);
        return mv;
    }

    /**
     * 修改
     *
     * @param @
     */
    @Authority(name = AuthorityCommon.EDIT)
    @PostMapping(value = "/edit")
    @ResponseBody
    public RepBase edit(ShareFile entity, @RequestPart(value = "uploadFile") MultipartFile multipartFile) {
        RepBase rep = new RepBase<>();
        if (multipartFile != null) {
            // 执行上传
            String fileName = FileUpload.fileUp(multipartFile, applicationConfig.getUploadFileLocation(),
                    IdWorker.getId() + "");
            String fileUrl = applicationConfig.getFileServerPrefix() + fileName;
            entity.setFileurl(fileUrl);
        }
        sharefileService.updateById(entity);
        return rep.setCommonSuccess();
    }

    /**
     * 删除
     *
     * @param out
     * @
     */
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/delete")
    @ResponseBody
    public RepBase delete(Long id) {
        sharefileService.deleteById(id);
        return RepBase.newIntance().setCommonSuccess();
    }

    /**
     * 批量删除
     *
     * @param @
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Authority(name = AuthorityCommon.DELETE)
    @PostMapping(value = "/deleteAll")
    @ResponseBody
    public RepBase deleteAll(String ids) {
        RepBase rep = new RepBase<>();
        if (null != ids && !"".equals(ids)) {
            String[] idArray = ids.split(",");
            sharefileService.deleteBatchIds(Lists.newArrayList(idArray));

        }
        return rep.setCommonSuccess();
    }


}
