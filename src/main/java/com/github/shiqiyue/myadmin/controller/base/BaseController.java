package com.github.shiqiyue.myadmin.controller.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.github.shiqiyue.myadmin.util.web.IpUtils;

/***
 * 基类-controller
 * 
 * @author wwy
 *
 */
public class BaseController {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 得到ModelAndView
	 *
	 * @return
	 */
	public ModelAndView getModelAndView() {
		return new ModelAndView();
	}
	
	/***
	 * 获得request
	 *
	 * @return
	 */
	protected static HttpServletRequest getServletRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	/***
	 * 获得ip
	 *
	 * @return
	 * @throws Exception
	 */
	protected static String getIp() throws Exception {
		return IpUtils.getIpAddr(getServletRequest());
	}
	
	/***
	 * 获得session
	 *
	 * @return
	 */
	protected static HttpSession getHttpSession() {
		return getServletRequest().getSession();
	}
	
}
