package com.github.shiqiyue.myadmin.exception;

/***
 * 程序-运行时错误
 *
 * @author wwy
 *
 */
public class ApplicationRuntimeException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -3607242177736229275L;

	public ApplicationRuntimeException(String mes) {
		super(mes);
	}

}
