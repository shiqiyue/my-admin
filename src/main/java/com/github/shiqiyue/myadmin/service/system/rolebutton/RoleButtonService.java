package com.github.shiqiyue.myadmin.service.system.rolebutton;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Menu;
import com.github.shiqiyue.myadmin.entity.system.RoleButton;
import com.github.shiqiyue.myadmin.mapper.system.RoleButtonMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wwy
 * <p>
 * <p>
 * 说明：角色-按钮权限 服务<br/>
 * 创建时间：2018-03-14
 * </p>
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class RoleButtonService extends ServiceImpl<RoleButtonMapper, RoleButton> {

    /**
     * 列表
     *
     * @param page
     * @
     */
    public Page<RoleButton> list(Page<RoleButton> page, Map<String, Object> map) {
        page.setRecords(baseMapper.dataList(page, map));
        return page;
    }

    /***
     * 列表(全部)
     * @param map
     * @return
     */
    public List<RoleButton> listAll(Map<String, Object> map) {
        return baseMapper.dataList(map);
    }

    /****
     * 修改角色按钮权限
     *
     * @param roleId
     *            角色id
     * @param buttonId
     *            按钮id
     * @param menuIds
     *            菜单id（数组）
     */
    public void updateRoleButtons(String roleId, String buttonId, List<String> menuIds) {
        // 删除之前记录
        this.delete(new EntityWrapper<RoleButton>().eq("roleId", roleId).eq("buttonId", buttonId));
        if (CollectionUtils.isEmpty(menuIds)) {
            return;
        }
        List<RoleButton> roleButtonList = new ArrayList<>(menuIds.size());
        for (String menuId : menuIds) {
            RoleButton roleButton = new RoleButton();
            roleButton.setButtonId(buttonId);
            roleButton.setMenuId(Long.parseLong(menuId));
            roleButton.setRoleId(roleId);
            roleButtonList.add(roleButton);
        }
        insertBatch(roleButtonList);
    }

    public void updateSpecialRoleButtons(String roleId, List<String> buttonIds) {
        // 删除之前记录
        this.delete(new EntityWrapper<RoleButton>().eq("roleId", roleId).isNull("menuId"));
        if (CollectionUtils.isEmpty(buttonIds)) {
            return;
        }
        List<RoleButton> roleButtonList = new ArrayList<>(buttonIds.size());
        for (String buttonId : buttonIds) {
            RoleButton roleButton = new RoleButton();
            roleButton.setButtonId(buttonId);
            roleButton.setRoleId(roleId);
            roleButtonList.add(roleButton);
        }
        insertBatch(roleButtonList);
    }

    public List<RoleButton> selectMoreInfoByRoleId(String roleId) {
        Map<String, Object> queryParam = new HashMap<>(1);
        queryParam.put("roleId", roleId);
        return baseMapper.selectMoreInfo(queryParam);
    }


    /***
     * 通过角色id和按钮id获取权限信息
     * @param roleId
     * @param buttonId
     * @return
     */
    public List<Menu> selectAuthsByRoleIdAndButtonId(String roleId, String buttonId) {
        return baseMapper.selectAuthsByRoleIdAndButtonId(roleId, buttonId);
    }
}
