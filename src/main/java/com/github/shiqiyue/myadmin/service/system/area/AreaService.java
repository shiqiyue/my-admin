package com.github.shiqiyue.myadmin.service.system.area;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.entity.system.Area;
import com.github.shiqiyue.myadmin.entity.system.area.AllArea;
import com.github.shiqiyue.myadmin.mapper.system.AreaMapper;

/***
 * 地区服务
 * 
 * @author wwy
 *
 */
@CacheConfig(cacheNames = "areaCache")
@Service("areaService")
@Transactional(rollbackFor = RuntimeException.class)
public class AreaService {
	
	@Autowired
	public AreaMapper areaMapper;
	
	/**
	 * 新增
	 *
	 * @param pd
	 * @
	 */
	@CacheEvict(allEntries = true)
	public void save(Area pd) {
		areaMapper.insert(pd);
	}
	
	/**
	 * 删除
	 *
	 * @param pd
	 * @
	 */
	@CacheEvict(allEntries = true)
	public void delete(Long id) {
		areaMapper.deleteById(id);
	}
	
	/**
	 * 修改
	 *
	 * @param pd
	 * @
	 */
	@CacheEvict(allEntries = true)
	public void edit(Area pd) {
		areaMapper.updateById(pd);
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<Area> list(Page<Area> page, Map<String, Object> map) throws Exception {
		page.setRecords(areaMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 城市列表
	 *
	 * @param page
	 * @
	 */
	@Cacheable(key = "methodName")
	public List<Area> cityList() {
		return areaMapper
				.selectList(new EntityWrapper<Area>().eq("type", 2).eq("isLock", Area.LOCK_ON).orderBy("sort", false));
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	@Cacheable(key = "methodName")
	public String listAllJson() {
		return JSON.toJSONString(listAllArea());
	}
	
	@Cacheable(key = "methodName")
	public AllArea listAllArea() {
		AllArea allArea = new AllArea();
		allArea.setProvinces(areaMapper.listAll());
		return allArea;
	}
	
	/**
	 * 通过id获取数据
	 *
	 * @param pd
	 * @
	 */
	@Cacheable(key = "#id")
	public Area findById(Long id) {
		return areaMapper.selectById(id);
	}
	
	/**
	 * 批量删除
	 *
	 * @param ArrayDATA_IDS
	 * @
	 */
	@CacheEvict(allEntries = true)
	public void deleteAll(String[] idArray) {
		areaMapper.deleteBatchIds(Arrays.asList(idArray));
	}
	
	/**
	 * 获取数据记录条数
	 *
	 * @param id
	 * @throws Exception
	 */
	@Cacheable(key = "methodName")
	public Integer findCount() throws Exception {
		return areaMapper.selectCount(new EntityWrapper<Area>());
	}
	
}
