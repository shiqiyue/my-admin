package com.github.shiqiyue.myadmin.service.system.role;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Role;
import com.github.shiqiyue.myadmin.mapper.system.RoleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/***
 * 角色服务
 * 
 * @author wwy
 *
 */
@Service("roleService")
@Transactional(rollbackFor = RuntimeException.class)
public class RoleService extends ServiceImpl<RoleMapper, Role> {
	
	/**
	 * 列出此组下级角色
	 *
	 * @param pd
	 * @return
	 * @
	 */
	public List<Role> listByParentId(String parentId) {
		return baseMapper.selectList(new EntityWrapper<Role>().eq("parentId", parentId).orderBy("addDate", true));
	}

	public Integer countByParentId(String parentId) {
		return baseMapper.selectCount(new EntityWrapper<Role>().eq("parentId", parentId));
	}

	/***
	 * 列出所有不是顶级的角色
	 *
	 * @return
	 */
	public List<Role> listAllNotTopRole() {
		return baseMapper.selectList(new EntityWrapper<Role>().ne("parentId", "0").orderBy("addDate", true));
	}
	
	/**
	 * 通过id查找
	 *
	 * @param pd
	 * @return
	 * @
	 */
	public Role findObjectById(String id) {
		return baseMapper.selectById(id);
	}
	
	/**
	 * 添加
	 *
	 * @param pd
	 * @
	 */
	public void add(Role entity) {
		
		baseMapper.insert(entity);
	}
	
	/**
	 * 保存修改
	 *
	 * @param pd
	 * @
	 */
	public void edit(Role entity) {
		baseMapper.updateById(entity);
	}
	
	/**
	 * 删除角色
	 *
	 * @param ROLE_ID
	 * @
	 */
	public void deleteRoleById(String id) {
		baseMapper.deleteById(id);
	}
	
	/**
	 * 通过id查找
	 *
	 * @param roleId
     * @returnR
	 * @
	 */
	public Role getRoleById(String id) {
		return baseMapper.selectById(id);
	}


}
