package com.github.shiqiyue.myadmin.service.system.document;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Document;
import com.github.shiqiyue.myadmin.mapper.system.DocumentMapper;

/***
 * 系统文档服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class DocumentService extends ServiceImpl<DocumentMapper, Document> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<Document> list(Page<Document> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<Document> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
	public Integer countByType(Integer type) {
		return selectCount(new EntityWrapper<Document>().eq("type", type));
	}
	
}
