package com.github.shiqiyue.myadmin.service.system.systemimage;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.SystemImage;
import com.github.shiqiyue.myadmin.mapper.system.SystemImageMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/***
 * 系统图片服务
 * 
 * @author wwy
 *
 */
@CacheConfig(cacheNames = "systemImageCache")
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class SystemImageService extends ServiceImpl<SystemImageMapper, SystemImage> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@Cacheable
	public Page<SystemImage> list(Page<SystemImage> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @
	 */
	@Cacheable
	public List<SystemImage> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
	@Cacheable
	public List<SystemImage> selectByType(Long type) {
		
		return baseMapper.selectList(new EntityWrapper<SystemImage>().eq("type", type));
	}
	
	@Cacheable
	public Integer countByType(Long type) {
		return baseMapper.selectCount(new EntityWrapper<SystemImage>().eq("type", type));
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean insert(SystemImage entity) {
		return super.insert(entity);
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean updateById(SystemImage entity) {
		return super.updateById(entity);
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean deleteById(Serializable id) {
		return super.deleteById(id);
	}
	
	@CacheEvict(allEntries = true)
	@Override
    public boolean deleteBatchIds(Collection<? extends Serializable> idList) {
		return super.deleteBatchIds(idList);
	}
	
}
