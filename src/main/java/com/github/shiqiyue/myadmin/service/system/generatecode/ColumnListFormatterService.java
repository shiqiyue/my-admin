package com.github.shiqiyue.myadmin.service.system.generatecode;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnListFormatter;
import com.github.shiqiyue.myadmin.mapper.system.generatecode.ColumnListFormatterMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ColumnListFormatterService extends ServiceImpl<ColumnListFormatterMapper, ColumnListFormatter> {

    public Page<ColumnListFormatter> list(Page<ColumnListFormatter> page, Map<String, Object> map) {
        page.setRecords(baseMapper.dataList(page, map));
        return page;
    }
}
