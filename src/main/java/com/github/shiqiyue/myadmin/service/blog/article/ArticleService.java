package com.github.shiqiyue.myadmin.service.blog.article;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.blog.Article;
import com.github.shiqiyue.myadmin.mapper.blog.ArticleMapper;
import com.github.shiqiyue.myadmin.service.XssProtectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/***
 * 文章服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ArticleService extends ServiceImpl<ArticleMapper, Article> {
	
	@Autowired
	private XssProtectService xssProtectService;
	
	public void addRecord(Article article) {
		cleanXss(article);
		insert(article);
		
	}
	
	private void cleanXss(Article article) {
		if (StringUtils.isNotBlank(article.getBrief())) {
			article.setBrief(xssProtectService.cleanXss(article.getBrief()));
		}
		if (StringUtils.isNotBlank(article.getTitle())) {
			article.setTitle(xssProtectService.cleanXss(article.getTitle()));
		}
	}
	
	public void editRecord(Article article) {
		cleanXss(article);
		updateById(article);
	}

    public void deleteRecord(String id) {
		
		deleteById(id);
	}

    public void deleteRecords(List<String> ids) {
		if (ids != null) {
            for (String id : ids) {
				deleteRecord(id);
			}
		}
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<Article> list(Page<Article> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<Article> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}


}
