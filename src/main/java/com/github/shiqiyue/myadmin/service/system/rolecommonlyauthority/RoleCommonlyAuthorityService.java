package com.github.shiqiyue.myadmin.service.system.rolecommonlyauthority;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.CommonlyAuthority;
import com.github.shiqiyue.myadmin.entity.system.RoleCommonlyAuthority;
import com.github.shiqiyue.myadmin.mapper.system.RoleCommonlyAuthorityMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 16:38 
 **/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class RoleCommonlyAuthorityService extends ServiceImpl<RoleCommonlyAuthorityMapper, RoleCommonlyAuthority> {


    public List<CommonlyAuthority> selectAuthsByRoleId(String roleId) {
        return baseMapper.selectAuthsByRoleId(roleId);
    }

    public void updateRoleCommonlyAuthority(String roleId, List<String> authorityIds) {
        baseMapper.delete(new EntityWrapper<RoleCommonlyAuthority>().eq("roleId", roleId));
        if (CollectionUtils.isEmpty(authorityIds)) {
            return;
        }
        List<RoleCommonlyAuthority> authorities = new ArrayList<>(authorityIds.size());
        for (String authorityId : authorityIds) {
            RoleCommonlyAuthority authority = new RoleCommonlyAuthority();
            authority.setAuthorityId(authorityId);
            authority.setRoleId(roleId);
            authorities.add(authority);
        }
        insertBatch(authorities);

    }

    public List<RoleCommonlyAuthority> selectMoreInfoByRoleId(String roleId) {
        return baseMapper.selectMoreInfoByRoleId(roleId);

    }
}
