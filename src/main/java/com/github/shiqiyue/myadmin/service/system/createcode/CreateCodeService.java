package com.github.shiqiyue.myadmin.service.system.createcode;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.github.shiqiyue.myadmin.entity.system.Createcode;
import com.github.shiqiyue.myadmin.mapper.system.CreatecodeMapper;

/***
 * 代码生成服务
 * 
 * @author wwy
 *
 */
@Service("createcodeService")
@Transactional(rollbackFor = RuntimeException.class)
public class CreateCodeService {
	
	@Autowired
	private CreatecodeMapper createcodeMapper;
	
	/**
	 * 新增
	 *
	 * @param entity
	 * @throws Exception
	 */
	public void save(Createcode entity) throws Exception {
		
		createcodeMapper.insert(entity);
	}
	
	/**
	 * 删除
	 *
	 * @param id
	 * @throws Exception
	 */
	public void delete(String id) throws Exception {
		createcodeMapper.deleteById(id);
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @throws Exception
	 */
	public Page<Createcode> list(Page<Createcode> page, Map<String, Object> map) throws Exception {
		page.setRecords(createcodeMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 获取数据记录条数
	 *
	 * @param id
	 * @throws Exception
	 */
	public Integer findCount() throws Exception {
		return createcodeMapper.selectCount(new EntityWrapper<Createcode>());
	}
	
	/**
	 * 通过id获取数据
	 *
	 * @param id
	 * @throws Exception
	 */
	public Createcode findById(String id) throws Exception {
		return createcodeMapper.selectById(id);
	}
	
	/**
	 * 批量删除
	 *
	 * @param ArrayDATA_IDS
	 * @throws Exception
	 */
	public void deleteAll(String[] ids) throws Exception {
		createcodeMapper.deleteBatchIds(Arrays.asList(ids));
	}
	
}
