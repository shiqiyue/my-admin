package com.github.shiqiyue.myadmin.service.system.imagetype;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.ImageType;
import com.github.shiqiyue.myadmin.mapper.system.ImageTypeMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/***
 * 图片类型服务
 * 
 * @author wwy
 *
 */
@CacheConfig(cacheNames = "imageTypeCache")
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ImageTypeService extends ServiceImpl<ImageTypeMapper, ImageType> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@Cacheable
	public Page<ImageType> list(Page<ImageType> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 */
	@Cacheable
	public List<ImageType> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
	@Cacheable
	public ImageType selectByType(Long type) {
		ImageType queryParam = new ImageType();
		queryParam.setType(type);
		return baseMapper.selectOne(queryParam);
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean insert(ImageType entity) {
		return super.insert(entity);
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean deleteById(Serializable id) {
		return super.deleteById(id);
	}
	
	@CacheEvict(allEntries = true)
	@Override
    public boolean deleteBatchIds(Collection<? extends Serializable> idList) {
		return super.deleteBatchIds(idList);
	}
	
	@CacheEvict(allEntries = true)
	@Override
	public boolean updateById(ImageType entity) {
		return super.updateById(entity);
	}
	
}
