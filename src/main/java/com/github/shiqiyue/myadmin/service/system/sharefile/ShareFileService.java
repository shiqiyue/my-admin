package com.github.shiqiyue.myadmin.service.system.sharefile;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.ShareFile;
import com.github.shiqiyue.myadmin.mapper.system.ShareFileMapper;

/***
 * 共享文件服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ShareFileService extends ServiceImpl<ShareFileMapper, ShareFile> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<ShareFile> list(Page<ShareFile> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<ShareFile> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
}
