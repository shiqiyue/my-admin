package com.github.shiqiyue.myadmin.service.system.syslog;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.SysLogRecord;
import com.github.shiqiyue.myadmin.mapper.system.SysLogMapper;

/***
 * 系统日志服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class SysLogService extends ServiceImpl<SysLogMapper, SysLogRecord> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<SysLogRecord> list(Page<SysLogRecord> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<SysLogRecord> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
}
