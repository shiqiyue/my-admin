package com.github.shiqiyue.myadmin.service.system.uristat;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.UriStat;
import com.github.shiqiyue.myadmin.mapper.system.UriStatMapper;

/***
 * uri请求状况服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class UriStatService extends ServiceImpl<UriStatMapper, UriStat> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<UriStat> list(Page<UriStat> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<UriStat> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
}
