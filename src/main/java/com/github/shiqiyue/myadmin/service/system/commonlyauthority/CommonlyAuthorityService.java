package com.github.shiqiyue.myadmin.service.system.commonlyauthority;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.CommonlyAuthority;
import com.github.shiqiyue.myadmin.mapper.system.CommonlyAuthorityMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/13/013 14:29 
 **/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class CommonlyAuthorityService extends ServiceImpl<CommonlyAuthorityMapper, CommonlyAuthority> {
    public Page<CommonlyAuthority> list(Page<CommonlyAuthority> page, Map<String, Object> map) {

        page.setRecords(baseMapper.dataList(page, map));
        return page;
    }
}
