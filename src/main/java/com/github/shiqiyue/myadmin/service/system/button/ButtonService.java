package com.github.shiqiyue.myadmin.service.system.button;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Button;
import com.github.shiqiyue.myadmin.mapper.system.ButtonMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/***
 * 按钮服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ButtonService extends ServiceImpl<ButtonMapper, Button> {

    public List<Button> selectByType(Short type) {
        return baseMapper.selectList(new EntityWrapper<Button>().eq("type", type));
    }


}
