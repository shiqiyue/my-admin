package com.github.shiqiyue.myadmin.service.system.uriinfo;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.UriInfo;
import com.github.shiqiyue.myadmin.entity.system.UriStat;
import com.github.shiqiyue.myadmin.mapper.system.UriInfoMapper;

/***
 * uri信息服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class UriInfoService extends ServiceImpl<UriInfoMapper, UriInfo> {
	
	public void addRecord(UriStat uriStat) {
		UriInfo uriInfo = selectOne(new EntityWrapper<UriInfo>().eq("uri", uriStat.getUri()));
		if (uriInfo != null) {
			uriInfo.setRequestCount(uriInfo.getRequestCount() + uriStat.getRequestCount());
			uriInfo.setRequestTimeMillis(uriInfo.getRequestTimeMillis() + uriStat.getRequestTimeMillis());
			uriInfo.setConcurrentMax(Math.max(uriInfo.getConcurrentMax(), uriStat.getConcurrentMax()));
			uriInfo.setLastAccessTime(uriStat.getLastAccessTime());
			updateById(uriInfo);
		} else {
			uriInfo = new UriInfo();
			uriInfo.setUri(uriStat.getUri());
			uriInfo.setRequestCount(uriStat.getRequestCount());
			uriInfo.setRequestTimeMillis(uriStat.getRequestTimeMillis());
			uriInfo.setConcurrentMax(uriStat.getConcurrentMax());
			uriInfo.setLastAccessTime(uriStat.getLastAccessTime());
			insert(uriInfo);
		}
		
	}
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<UriInfo> list(Page<UriInfo> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<UriInfo> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
}
