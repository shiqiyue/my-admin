package com.github.shiqiyue.myadmin.service.system.urigroup;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.UriGroup;
import com.github.shiqiyue.myadmin.mapper.system.UriGroupMapper;

/***
 * uri分组服务
 * 
 * @author wwy
 *
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class UriGroupService extends ServiceImpl<UriGroupMapper, UriGroup> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	public Page<UriGroup> list(Page<UriGroup> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	public List<UriGroup> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
}
