package com.github.shiqiyue.myadmin.service.blog.test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.blog.Test;
import com.github.shiqiyue.myadmin.mapper.blog.TestMapper;

/**
*  @author wwy
*
*  <p>说明：测试 服务<br/>
*  创建时间：2018-02-01</p>
*/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class TestService extends ServiceImpl<TestMapper, Test>{


	/**
	 * 列表
	 * 
	 * @param page
	 * @
	 */
	public Page<Test> list(Page<Test> page, Map<String, Object> map)  {
	    page.setRecords(baseMapper.dataList(page, map));
        return page;
	}

	/**
	 * 列表(全部)
	 * 
	 * @param pd
	 * @
	 */
	public List<Test> listAll(Map<String, Object> map)  {
		return baseMapper.dataList(map);
	}

}

