package com.github.shiqiyue.myadmin.service.system.generatecode;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.generateode.ColumnPlugin;
import com.github.shiqiyue.myadmin.mapper.system.generatecode.ColumnPluginMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/***
 *
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 14:43 
 **/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ColumnPluginService extends ServiceImpl<ColumnPluginMapper, ColumnPlugin> {

    public Page<ColumnPlugin> list(Page<ColumnPlugin> page, Map<String, Object> map) {
        page.setRecords(baseMapper.dataList(page, map));
        return page;
    }
}
