package com.github.shiqiyue.myadmin.service.system.bank;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Bank;
import com.github.shiqiyue.myadmin.mapper.system.BankMapper;

/***
 * 银行服务
 * 
 * @author wwy
 *
 */
@CacheConfig(cacheNames = "bankCache")
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class BankService extends ServiceImpl<BankMapper, Bank> {
	
	/**
	 * 列表
	 *
	 * @param page
	 * @
	 */
	@Cacheable
	public Page<Bank> list(Page<Bank> page, Map<String, Object> map) {
		page.setRecords(baseMapper.datalistPage(page, map));
		return page;
	}
	
	/**
	 * 列表(全部)
	 *
	 * @param pd
	 * @
	 */
	@Cacheable
	public List<Bank> listAll(Map<String, Object> map) {
		return baseMapper.listAll(map);
	}
	
	@CacheEvict(allEntries = true)
	public void save(Bank entity) {
		insert(entity);
	}
	
	@CacheEvict(allEntries = true)
	public void edit(Bank bank) {
		updateById(bank);
	}
	
	@CacheEvict(allEntries = true)
	public void delete(String id) {
		deleteById(id);
	}
	
	@CacheEvict(allEntries = true)
	public void deleteBatch(List<String> ids) {
		deleteBatch(ids);
	}
	
}
