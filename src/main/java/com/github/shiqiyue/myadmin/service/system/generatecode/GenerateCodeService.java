package com.github.shiqiyue.myadmin.service.system.generatecode;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.generateode.GenerateCode;
import com.github.shiqiyue.myadmin.mapper.system.generatecode.GenerateCodeMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
*  @author wwy
*
*  <p>说明：代码 服务<br/>
*  创建时间：2018-03-20</p>
*/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class GenerateCodeService extends ServiceImpl<GenerateCodeMapper, GenerateCode>{


	/**
	 * 列表
	 * 
	 * @param page
	 * @
	 */
	public Page<GenerateCode> list(Page<GenerateCode> page, Map<String, Object> map)  {
	    page.setRecords(baseMapper.dataList(page, map));
        return page;
	}

	/**
	 * 列表(全部)
	 * 
	 * @param pd
	 * @
	 */
	public List<GenerateCode> listAll(Map<String, Object> map)  {
		return baseMapper.dataList(map);
	}

}

