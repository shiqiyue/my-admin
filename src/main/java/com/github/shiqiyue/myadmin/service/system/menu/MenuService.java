package com.github.shiqiyue.myadmin.service.system.menu;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.shiqiyue.myadmin.entity.system.Menu;
import com.github.shiqiyue.myadmin.mapper.system.MenuMapper;
import com.github.shiqiyue.myadmin.service.system.rolemenu.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/***
 * 菜单服务
 *
 * @author wwy
 *
 */
// @CacheConfig(cacheNames = "cache.menu")
@Service("menuService")
@Transactional(rollbackFor = RuntimeException.class)
public class MenuService extends ServiceImpl<MenuMapper, Menu> {


    @Autowired
    private RoleMenuService roleMenuService;


    public List<Menu> listByParentId(String parentId) {
        return baseMapper.selectList(new EntityWrapper<Menu>().eq("parentId", parentId).orderBy("sort", true));
    }

    public Integer countByParentId(String parentId) {
        return baseMapper.selectCount(new EntityWrapper<Menu>().eq("parentId", parentId));
    }


    public void deleteMenuById(String id) {
        baseMapper.deleteById(id);
    }


    public void edit(Menu menu) {
        baseMapper.updateById(menu);
    }

    public void editIcon(String id, String icon) {
        Menu menu = new Menu();
        menu.setId(id);
        menu.setIcon(icon);
        baseMapper.updateById(menu);
    }


}
