package com.github.shiqiyue.myadmin.vo.rep.rolebutton;

import java.util.List;

import lombok.Data;

/***
 * 树节点
 * 
 * @author wwy shiqiyue.github.com
 *
 */
@Data
public class TreeNode {
	
	private String menuId;
	
	private String name;
	
	private List<TreeNode> nodes;
	
	private Boolean checked;
	
}
