package com.github.shiqiyue.myadmin.vo.rep.base;

/***
 * 接口返回信息wrapper
 * 
 * @author wwy
 *
 * @param <T>
 */
public class RepBase<T> {
	
	private Integer code;
	private String mes;
	private T data;

    public static RepBase newIntance() {
        return new RepBase<>();
	}
	
	public Integer getCode() {
		return code;
	}

    public RepBase<T> setCode(ResultCode code) {
		this.code = code.getCode();
		return this;
	}
	
	public String getMes() {
		return mes;
	}

    public RepBase<T> setMes(String mes) {
		this.mes = mes;
		return this;
	}
	
	public T getData() {
		return data;
	}

    public RepBase<T> setData(T data) {
		this.data = data;
		return this;
	}

    public RepBase<T> setInfo(ResultCode code, String mes, T data) {
		this.code = code.getCode();
		this.mes = mes;
		this.data = data;
		return this;
	}

    public RepBase<T> setInfo(ResultCode code, String mes) {
		this.code = code.getCode();
		this.mes = mes;
		this.data = null;
		return this;
	}

    public RepBase<T> setCommonFail() {
		this.code = ResultCode.COMMON_FAIL.getCode();
		this.mes = ResultCode.COMMON_FAIL.getMes();
		return this;
	}

    public RepBase<T> setCommonSuccess() {
		this.code = ResultCode.SUCCESS.getCode();
		this.mes = ResultCode.SUCCESS.getMes();
		return this;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
        builder.append("RepBase [code=").append(code).append(", mes=").append(mes).append(", data=").append(data)
				.append("]");
		return builder.toString();
	}
	
}
