package com.github.shiqiyue.myadmin.vo.rep.base;

import java.util.Collection;

/***
 * 接口返回分页信息
 * @Author: shiqiyue
 * @Date: 创建于2018/4/19/019 14:26
 **/
public class RepPage<T> {

    /***
     * 记录
     */
    private Collection<T> rows;

    /***
     * 总共几条记录
     */
    private Long total;

    /***
     * 分页大小
     */
    private Integer size;

    /***
     * 当前第几页
     */
    private Integer current;

    /***
     * 一共几个分页
     */
    private Long pages;

    public RepPage() {
        this(null, null);
    }

    public RepPage(Collection<T> rows, Long total) {
        this(rows, total, null, null, null);
    }

    public RepPage(Collection<T> rows, Long total, Integer size, Integer current, Long pages) {
        this.rows = rows;
        this.total = total;
        this.size = size;
        this.current = current;
        this.pages = pages;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    public Collection<T> getRows() {
        return rows;
    }

    public void setRows(Collection<T> rows) {
        this.rows = rows;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
