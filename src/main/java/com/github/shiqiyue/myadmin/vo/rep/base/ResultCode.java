package com.github.shiqiyue.myadmin.vo.rep.base;

/***
 * 返回代码
 *
 * @author wwy
 *
 */
public enum ResultCode {
	/** 一般成功 */
	SUCCESS(200, "请求成功"),
	/** 一般失败 */
	COMMON_FAIL(400, "请求失败"),
	/** 未登陆 */
	NOT_LOGIN(401, "登录失效"),
	/** 会员冻结 */
	MEMBER_FORZEN(402, "会员冻结"),
	/** 权限不足 */
	AUTHORITY_ERROR(403, "权限不足"),;
	
	private Integer code;
	
	private String mes;
	
	private ResultCode(Integer code, String mes) {
		this.code = code;
		this.mes = mes;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public String getMes() {
		return mes;
	}
	
	public void setMes(String mes) {
		this.mes = mes;
	}
	
}
