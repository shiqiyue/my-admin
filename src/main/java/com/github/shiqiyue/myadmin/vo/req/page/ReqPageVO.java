package com.github.shiqiyue.myadmin.vo.req.page;

/***
 * 请求分页
 * 
 * @author wwy
 *
 */
public class ReqPageVO {
	
	private Integer size;
	
	private Integer current;
	
	private String sortColumn;
	
	private String sortDirection;
	
	public Integer getSize() {
		return size;
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}
	
	public Integer getCurrent() {
		return current;
	}
	
	public void setCurrent(Integer current) {
		this.current = current;
	}
	
	public String getSortColumn() {
		return sortColumn;
	}
	
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	
	public String getSortDirection() {
		return sortDirection;
	}
	
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReqPageVO [size=").append(size).append(", current=").append(current).append(", sortColumn=")
				.append(sortColumn).append(", sortDirection=").append(sortDirection).append("]");
		return builder.toString();
	}
	
}
