package com.github.shiqiyue.myadmin.util.number;

import java.math.BigDecimal;

/***
 * 数字工具类
 * 
 * @author wwy
 *
 */
public class NumberUtils {
	
	/***
	 * 单位元 精确到小数点后两位，如果后两位有数值则显示，如果后两位没有数值则不显示
	 *
	 * @return
	 */
	public static String normalPrice(Double price) {
		BigDecimal bd = new BigDecimal(price);
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		bd = bd.stripTrailingZeros();
		return bd.toPlainString();
	}
	
	/***
	 * 如果为null则返回0
	 *
	 * @param number
	 * @return
	 */
	public static Number ifNullThenZero(Number number) {
		if (number == null) {
			return 0;
		} else {
			return number;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(normalPrice(0.00));
		;
		// System.out.println(BigDecimalUtils.divide(new BigDecimal("20.0"), new
		// BigDecimal(100)));
		// System.out.println(new BigDecimal("20.0").divide(new
		// BigDecimal(100)));
	}
}
