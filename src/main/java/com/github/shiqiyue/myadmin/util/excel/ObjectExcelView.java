package com.github.shiqiyue.myadmin.util.excel;

import com.github.shiqiyue.myadmin.util.date.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 导入到EXCEL
 * 
 * @author wwy
 *
 */
public class ObjectExcelView extends AbstractXlsxStreamingView {
	
	private static final Logger log = LoggerFactory.getLogger(ObjectExcelView.class);
	
	/** 栏目名称前缀 */
	private static final String SHEET_NAME_PREFIX = "第";
	/** 栏目名称后缀 */
	private static final String SHEET_NAME_SUFFIX = "页";
	/** 单元格默认宽度 */
	private static final Short CELL_DEFAULT_WIDTH = 20;
	/** 标题默认高度 */
	private static final Short TITLE_DEFAULT_HEIGHT = 500;
	/** 每页几条数据 */
	private static final Integer PAGE_SIZE = 10000;
	
	/** 标题 */
	private List<String> titles;
	/** 数据 */
	private List<List<String>> datas;
	
	public ObjectExcelView(List<String> titles, List<List<String>> datas) {
		super();
		this.titles = titles;
		this.datas = datas;
	}
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		long startTime = System.currentTimeMillis();
		Date now = new Date();
		String filename = DateUtil.getSdfTimes(now);
		// response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=" + filename + ".xlsx");
		for (int pageIndex = 0; pageIndex * PAGE_SIZE < datas.size(); pageIndex++) {
			createSheet(workbook, pageIndex, titles, datas, pageIndex * PAGE_SIZE,
					Math.min((pageIndex + 1) * PAGE_SIZE, datas.size()));
		}
		logger.info("use:" + (System.currentTimeMillis() - startTime));
	}
	
	private Sheet createSheet(Workbook workbook, int index, List<String> titles, List<List<String>> datas,
			int dataStart, int dataEnd) {
		Sheet sheet = workbook.createSheet(SHEET_NAME_PREFIX + (index + 1) + SHEET_NAME_SUFFIX);
		// 标题样式
		CellStyle headerStyle = workbook.createCellStyle();
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		// 标题字体
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerStyle.setFont(headerFont);
		
		sheet.setDefaultColumnWidth(CELL_DEFAULT_WIDTH);
		// 输出标题
		for (int i = 0; i < titles.size(); i++) {
			String title = titles.get(i);
			createCell(sheet, 0, i, headerStyle, title);
		}
		sheet.getRow(0).setHeight(TITLE_DEFAULT_HEIGHT);
		// 内容样式
		CellStyle contentStyle = workbook.createCellStyle();
		contentStyle.setAlignment(HorizontalAlignment.CENTER);
		// 输出内容
		for (int dataIndex = dataStart; dataIndex < dataEnd; dataIndex++) {
			List<String> dataItem = datas.get(dataIndex);
			for (int titleIndex = 0; titleIndex < titles.size(); titleIndex++) {
				String val = StringUtils.defaultString(dataItem.get(titleIndex));
				createCell(sheet, dataIndex - dataStart + 1, titleIndex, contentStyle, val);
			}
		}
		return sheet;
	}
	
	private Cell createCell(Sheet sheet, int row, int col, CellStyle cellStyle, String value) {
		Row sheetRow = sheet.getRow(row);
		if (sheetRow == null) {
			sheetRow = sheet.createRow(row);
		}
        Cell cell = sheetRow.getCell(col);
		if (cell == null) {
			cell = sheetRow.createCell(col);
		}
		cell.setCellStyle(cellStyle);
		cell.setCellValue(value);
		return cell;
	}
	
}
