package com.github.shiqiyue.myadmin.util.id.generator;

/***
 * id 工具类
 * 
 * @author wwy
 *
 */
public class IDUtils {
	public static final SnowflakeIdWorker USER_IDWORK = new SnowflakeIdWorker(0, 0);
	public static final SnowflakeIdWorker ORDER_IDWORK = new SnowflakeIdWorker(1, 0);
	public static final SnowflakeIdWorker AREA_WORK = new SnowflakeIdWorker(0, 2);
	
	/***
	 * 生成用户id
	 *
	 * @return
	 */
	public static String getUserId() {
		return USER_IDWORK.nextId() + "";
	}
	
	public static Long getAreaId() {
		return AREA_WORK.nextId();
	}
	
}
