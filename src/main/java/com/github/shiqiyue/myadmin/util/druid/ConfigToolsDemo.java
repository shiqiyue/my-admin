package com.github.shiqiyue.myadmin.util.druid;

import com.alibaba.druid.filter.config.ConfigTools;

/***
 * config tool demo
 * 
 * @author wwy
 *
 */
public class ConfigToolsDemo {
	
	/***
	 * 上述生成的私钥
	 */
	private static final String PRIVATE_KEY_STRING = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAI88eIk1eDwcr900w90cXqPJB67+cJuhMQraJLUNrN43V+X66oU5m1Q0ykxaPomm9DaxWcIu+4QOBXmA3UXs6vnGZ4xtUOVu7ZcEWbO0aSEcmIlm+3pkkmz/BeHMNoguLKuHN2Y3aSyLO5nGcugfMfkNTJSaMyUS1yvn701lm98bAgMBAAECgYAOWbqh1I+VRjKAcRbmgj8hHbT9XVSe4wls0uo2kYYuRVEzI253povfZeKWv7vgQq/ZAu/BOr/MPTNjNvv0U4Y/FbVHL3jHy0GxfvwYb9BPTW2w7pkdkVz8bnbtwNb3w88p7pqNyZSDf9SHT3fTWsLSkAUpD6YDqRrAEIIGOOzggQJBAMS2jVu8gevwUsdmrVajRwsaBlKjOVjpMHtmrxi7GStFx5Z3ct6a73EdPKIRbXG7UiHag89t6sf1XIyIcmJesXsCQQC6Z+WKci9r2Zn8okNFLkxQxeziNBDqL+lVZ/tEpehkVmiC3FGxxCMzOexjB8XnKO9T8jfnvHF0+VBIXy9s2wbhAkAfEnbhtrUO/7cTqDRZKxFdv46gHwg/8xWlaGLgFS20mCEes1+BqheIVoXtU7Nl86DVmB6Z9VLQowErcaIfx7VhAkEAk+fWgYjiyb+qaIPTvrPiyACUWlbuttonRightsy8piaWsNO8Ys5FiPqq3ogJXxnDbssxM625VmeQRriYvDtp4QLqygM86C4QJAMMAFoCby578fB0DL4bk8QTiX0QTiNIIq0JgBI3drnhAZcAEdMqPlAHpt3pKAw3CdJSMkpaoknDiUl6b2NmUmcA==";
	
	public static void main(String[] args) throws Exception {
		// 密码明文，也就是数据库的密码
		String plainText = "123456";
		System.out.printf(ConfigTools.encrypt(PRIVATE_KEY_STRING, plainText));
	}
	
}