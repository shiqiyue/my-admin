package com.github.shiqiyue.myadmin.util.number;

import java.math.BigDecimal;

/***
 * 价格工具类
 * 
 * @author wwy
 *
 */
public class PriceUtils {
	
	/***
	 * 单位元 精确到小数点后两位，如果后两位有数值则显示，如果后两位没有数值则不显示
	 *
	 * @return
	 */
	public static String normalPrice(Object d) {
		BigDecimal price = null;
		if (d instanceof Double) {
			price = new BigDecimal((Double) d);
		} else if (d instanceof String) {
			price = new BigDecimal((String) d);
		} else {
			price = new BigDecimal(d.toString());
		}
		price = price.setScale(3, BigDecimal.ROUND_HALF_UP);
		price = price.stripTrailingZeros();
		return price.toPlainString();
	}
	
	public static Double normalPrice2(Object d) {
		BigDecimal price = null;
		if (d instanceof Double) {
			price = new BigDecimal((Double) d);
		} else if (d instanceof String) {
			price = new BigDecimal((String) d);
		} else {
			price = new BigDecimal(d.toString());
		}
		price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
		price = price.stripTrailingZeros();
		return price.doubleValue();
	}
	
	public static void main(String[] args) {
		System.out.println(normalPrice("111.333"));
		System.out.println(normalPrice(111.200));
	}
	
}
