package com.github.shiqiyue.myadmin.util.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 * ip工具类
 *
 * @author kjq
 *
 */
public class IpUtils {
	
	private static final Logger log = LoggerFactory.getLogger(IpUtils.class);
	
	private static final String LOCALHOST_IPV4 = "127.0.0.1";
	
	private static final String LOCALHOST_IPV6 = "0:0:0:0:0:0:0:1";
	
	private static final String UNKNOWN = "unknown";
	
	private static final String DELIMITER = ",";
	
	private static final String[] IP_HEADERS = { "x-forwarded-for", "Proxy-Client-IP", "WL-Proxy-Client-IP",
			"HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR" };
	
	/**
	 * 获取IP地址
	 *
	 * 使用Nginx等反向代理软件， 则不能通过request.getRemoteAddr()获取IP地址
	 * 如果使用了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP地址，X-Forwarded-For中第一个非unknown的有效IP字符串，则为真实IP地址
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = null;
		try {
			for (String header : IP_HEADERS) {
				ip = request.getHeader(header);
				if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
					continue;
				}
				break;
			}
			if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
		} catch (Exception e) {
			log.error("IPUtils ERROR ", e);
		}
		
		// 使用代理，则获取第一个IP地址
		if (StringUtils.isNotBlank(ip) && ip.length() > 15) {
			if (ip.indexOf(DELIMITER) > 0) {
				ip = ip.substring(0, ip.indexOf(DELIMITER));
			}
		}
		
		return LOCALHOST_IPV6.equals(ip) ? LOCALHOST_IPV4 : ip;
	}
}
