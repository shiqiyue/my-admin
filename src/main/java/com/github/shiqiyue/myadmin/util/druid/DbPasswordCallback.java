package com.github.shiqiyue.myadmin.util.druid;

import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.util.DruidPasswordCallback;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/***
 * 数据库密码加密callback
 * 
 * @author wwy
 *
 */
public class DbPasswordCallback extends DruidPasswordCallback {
	
	/**
	 *
	 */
	private static final long serialVersionUID = -1549487816015856282L;
	/***
	 * 上述生成的公钥
	 */
	public static final String PUBLIC_KEY_STRING = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCPPHiJNXg8HK/dNMPdHF6jyQeu/nCboTEK2iS1DazeN1fl+uqFOZtUNMpMWj6JpvQ2sVnCLvuEDgV5gN1F7Or5xmeMbVDlbu2XBFmztGkhHJiJZvt6ZJJs/wXhzDaILiyrhzdmN2ksizuZxnLoHzH5DUyUmjMlEtcr5+9NZZvfGwIDAQAB";
	
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		String pwd = properties.getProperty("password");
		if (StringUtils.isNotBlank(pwd)) {
			try {
				// 这里的password是将jdbc.properties配置得到的密码进行解密之后的值
				// 所以这里的代码是将密码进行解密
				String password = ConfigTools.decrypt(PUBLIC_KEY_STRING, pwd);
				setPassword(password.toCharArray());
			} catch (Exception e) {
				setPassword(pwd.toCharArray());
			}
		}
	}
	
}