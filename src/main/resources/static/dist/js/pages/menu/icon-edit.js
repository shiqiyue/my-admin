//页面请求链接
var urlBase = "/admin/menu";

var urls = {
    editIcon: urlBase + "/icon/edit"
}

$(function () {

    $("input[name='icon']").click(function () {
        var $this = $(this);
        var $i = $this.next("i");
        var icon = $i.attr("class");
        console.warn(icon);
        $("#hidden-icon").val(icon);
    })
});
// 操作表单(增加或者修改)el
var $operForm = $("#form-oper");

// 操作，增删改查等等
var operate = {
    merge: function () {
        /** 合并修改和添加操作 */
        var id = $("#hidden-id").val();
        if (id.length == 0) {
            operate.add();
        } else {
            operate.edit();
        }
    },
    edit: function () {
        /** 修改操作 */

            // 特殊验证

            // 提交
        var postData = {};
        postData.id = $("#hidden-id").val();
        postData.icon = $("#hidden-icon").val();
        $.post(urls.editIcon, postData, function (result) {
            if (result.code == 200) {
                bootbox.alert("修改成功", function () {
                    operate.cancel();
                })
            } else {
                bootbox.alert(result.mes);
            }
        })
    },
    add: function () {

    },
    cancel: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
}