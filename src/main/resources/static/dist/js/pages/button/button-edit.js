//页面请求链接
var urlBase = "/admin/button";

var urls = {
    add: urlBase + "/add",
    edit: urlBase + "/edit"
}
// 操作表单(增加或者修改)el
var $operForm = $("#form-oper");
$(function () {
    $operForm.validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {

        } else {
            operate.merge();
        }
        return false;
    })
})
// 操作，增删改查等等
var operate = {
    merge: function () {
        /** 合并修改和添加操作 */
        var id = $("#hidden-id").val();
        if (id.length == 0) {
            operate.add();
        } else {
            operate.edit();
        }
    },
    edit: function () {
        /** 修改操作 */

        // 特殊验证

        // 提交
        $.post(urls.edit, $operForm.serialize(), function (result) {
            if (result.code == 200) {
                bootbox.alert("修改成功", function () {
                    operate.cancel();
                })
            } else {
                bootbox.alert(result.mes);
            }
        })
    },
    add: function () {
        /** 添加操作 */

        // 特殊验证

        // 提交
        $.post(urls.add, $operForm.serialize(), function (result) {
            if (result.code == 200) {
                bootbox.alert("添加成功", function () {
                    operate.cancel();
                })
            } else {
                bootbox.alert(result.mes);
            }
        })
    },
    cancel: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
}