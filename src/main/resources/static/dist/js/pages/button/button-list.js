//页面请求链接
var urlBase = "/admin/button";

var urls = {
    list: urlBase + "/list",
    add: urlBase + "/add",
    edit: urlBase + "/edit",
    del: urlBase + "/del",
    batchDel: urlBase + "/batch/del"
}
// 表格el
var $table = $("#table-data");
// 安全信息el
var $security = $("#div-security-info");

// 权限信息
var auths = {};
auths.add = $security.data("auth-add");
auths.del = $security.data("auth-delete");
auths.edit = $security.data("auth-edit");
auths.list = $security.data("auth-list");
auths.exportExcel = $security.data("auth-exportexcel");

// 自定义formatter
var customFormatter = {
    oper: function (value, row, index, field) {
        var result = "";
        var editHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit' title='修改'><i class='fa fa-edit'></i></a></div>");
        var delHtml = ("<div class='btn-group'><a class='btn btn-danger btn-md oper-delete' title='减少'><i class='fa fa-close'></i></a></div>");

        if (auths.edit && row.editable) {
            result += editHtml;
        }
        if (auths.del && row.editable) {
            result += delHtml;
        }
        return result;
    },
    typeEnum: function (value, row, index, field) {
        if (value == 1) {
            return "全局";
        }
        if (value == 2) {
            return "特殊";
        }
        return "";
    }
};
// 最终formatter
var formatter = $.extend({}, listCommon.formatter, customFormatter);

// 自定义表格事件
var customTableEvent = {
    "click .oper-edit": function (e, value, row, index) {
        operate.toEdit(row);
    },
    "click .oper-delete": function (e, value, row, index) {
        operate.del(row.id);
    },
    "click .oper-add": function (e, value, row, index) {
        operate.toAdd(row);
    }
};
// 最终表格事件
var tableEvent = $.extend({}, listCommon.tableEvent, customTableEvent);

// 自定义请求参数
var customRequestParams = {};
// 自定义表格参数
var customtableOption = {
    url: urls.list,
    pagination: false,
    queryParams: function (params) {
        var requestParams = $.extend({}, customRequestParams);
        requestParams.order = params.order;
        requestParams.sort = params.sort;
        return requestParams;
    }
};
// 最终表格参数
var tableOption = $.extend({}, listCommon.tableOption, customtableOption);
$table.bootstrapTable(tableOption);

// 操作，增删改查等等
var operate = {
    tableRefresh: function (options) {
        /** 表格刷新 */
        var defaultOptions = {silent: true};
        var currentOptions = $.extend({}, defaultOptions, options);
        $table.bootstrapTable("refresh", currentOptions);
    },
    getTableSelectIds: function () {
        /** 返回表格选择的row的id */
        var ids = [];
        var selectRows = $table.bootstrapTable("getSelections");
        if (selectRows.length == 0) {
            return ids;
        }
        for (var i = 0; i < selectRows.length; i++) {
            ids.push(selectRows[i].id);
        }
        return ids;
    },
    toEdit: function (data) {
        listCommon.openIframeDialog("修改", urls.edit + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toAdd: function (data) {
        listCommon.openIframeDialog("添加", urls.add, function () {
            operate.tableRefresh();
        });
    },
    del: function (id) {
        /** 删除 */
        bootbox.confirm("确定删除吗？", function (result) {
            if (!result) {
                return;
            }
            var postData = {};
            postData.id = id;
            $.post(urls.del, postData, function (result) {
                if (result.code == 200) {
                    bootbox.alert("删除成功", function () {
                        operate.tableRefresh();
                    })
                } else {
                    bootbox.alert(result.mes);
                }
            });
        });

    },
    batchDel: function () {
        /** 批量删除 */
        bootbox.confirm("确定删除吗？", function (result) {
            if (!result) {
                return;
            }
            var postData = {};
            postData.ids = operate.getTableSelectIds();
            $.post(urls.batchDel, postData, function (result) {
                if (result.code == 200) {
                    operate.tableRefresh();
                } else {
                    bootbox.alert(result.mes);
                }
            })
        });

    }
}