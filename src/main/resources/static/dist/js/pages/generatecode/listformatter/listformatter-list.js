//页面请求链接
var urlBase = "/admin/generatecode/listformatter";

var urls = {
    list: urlBase + "/list",
    add: urlBase + "/add",
    edit: urlBase + "/edit",
    del: urlBase + "/del",
    batchDel: urlBase + "/batch/del",
    exportExcel: urlBase + "/excel"
};
// 表格el
var $table = $("#table-data");
// 查询表单el
var $searchForm = $("#form-search");
// 操作表单(增加或者修改)el
var $operForm = $("#form-oper");

// 安全信息el
var $security = $("#div-security-info");

// 权限信息
var auths = {};
auths.add = $security.data("auth-add");
auths.del = $security.data("auth-delete");
auths.edit = $security.data("auth-edit");
auths.list = $security.data("auth-list");
auths.exportExcel = $security.data("auth-exportexcel");

// 自定义formatter
var customFormatter = {
    oper: function (value, row, index, field) {
        var result = "";
        var editHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit' title='修改'><i class='fa fa-edit'></i></a></div>");
        var delHtml = ("<div class='btn-group'><a class='btn btn-danger btn-md oper-delete' title='删除'><i class='fa fa-close'></i></a></div>");
        if (auths.edit) {
            result += editHtml;
        }
        if (auths.del) {
            result += delHtml;
        }
        return result;
    }
};
// 最终formatter
var formatter = $.extend({}, listCommon.formatter, customFormatter);

// 自定义表格事件
var customTableEvent = {
    "click .oper-edit": function (e, value, row, index) {
        operate.toEdit(row);
    },
    "click .oper-delete": function (e, value, row, index) {
        operate.del(row.id);
    }
};
// 最终表格事件
var tableEvent = $.extend({}, listCommon.tableEvent, customTableEvent);

// 自定义请求参数
var customRequestParams = {};
// 自定义表格参数
var customtableOption = {
    url: urls.list,
    sortName: "addDate",
    sortOrder: "desc",
    queryParams: function (params) {
        var requestParams = $.extend({}, customRequestParams);
        requestParams.size = params.limit;
        requestParams.current = params.offset / params.limit + 1;
        requestParams.sortDirection = params.order;
        requestParams.sortColumn = params.sort;
        return requestParams;
    }
};
// 最终表格参数
var tableOption = $.extend({}, listCommon.tableOption, customtableOption);
$table.bootstrapTable(tableOption);

// 操作，增删改查等等
var operate = {
    search: function () {
        /** 查询 */
        customRequestParams = $.extend({}, customRequestParams, $searchForm.parseForm());
        operate.tableFirstPage();
    },
    tableRefresh: function (options) {
        /** 表格刷新 */
        var defaultOptions = {silent: true};
        var currentOptions = $.extend({}, defaultOptions, options);
        $table.bootstrapTable("refresh", currentOptions);
    },
    tableFirstPage: function () {
        /** 表格刷新并且跳转第一页 */
        $table.bootstrapTable("refresh", {
            "pageNumber": 1
        });
    },
    getTableSelectIds: function () {
        /** 返回表格选择的row的id */
        var ids = [];
        var selectRows = $table.bootstrapTable("getSelections");
        if (selectRows.length == 0) {
            return ids;
        }
        for (var i = 0; i < selectRows.length; i++) {
            ids.push(selectRows[i].id);
        }
        return ids;
    },
    toEdit: function (data) {
        /** 跳转到修改页面 */
        listCommon.openIframeDialog("修改", urls.edit + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toAdd: function () {
        /** 跳转到添加页面 */
        listCommon.openIframeDialog("添加", urls.add, function () {
            operate.tableRefresh();
        });
    },
    del: function (id) {
        bootbox.confirm("确定删除吗？", function (result) {
            if (!result) {
                return;
            }
            /** 删除 */
            var postData = {};
            postData.id = id;
            $.post(urls.del, postData, function (result) {
                if (result.code == 200) {
                    bootbox.alert("删除成功", function () {
                        operate.tableRefresh();
                    })
                } else {
                    bootbox.alert(result.mes);
                }
            })
        });


    },
    batchDel: function () {
        /** 批量删除 */
        var postData = {};
        postData.ids = operate.getTableSelectIds();
        if (postData.ids.length == 0) {
            return;
        }
        bootbox.confirm("确定删除吗？", function (result) {
            if (!result) {
                return;
            }
            $.post(urls.batchDel, postData, function (result) {
                if (result.code == 200) {
                    operate.tableRefresh();
                } else {
                    bootbox.alert(result.mes);
                }
            })
        });

    },
    exportExcel: function () {
        /** 导出excel */
        var preAction = $searchForm.attr("action");
        $searchForm.attr("action", urls.exportExcel);
        $searchForm.submit();
        $searchForm.attr("action", preAction);
    }
}