//页面请求链接
var urlBase = "/admin/role";
var urls = {
    buttonAuthEdit: urlBase + "/button/auth/edit",
    buttonAuthData: urlBase + "/button/auth/edit/data"
}
// 操作表单(增加或者修改)el
var zTreeObj;
var buttonId;
var roleId = $("#hidden-id").val();
var zTreesetting = {
    check: {
        enable: true
        //chkboxType: {"Y": "", "N": ""},
        //nocheckInherit: true
    },
    data: {
        key: {
            checked: "ownMenuRights",
            url: "ccc"

        },
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "parentId"
        }
    }

};

// 操作，增删改查等等
var operate = {
    submit: function () {
        var nodes = zTreeObj.getCheckedNodes();
        var postData = {};
        var requestUrl;

        var ids = [];
        for (var i = 0; i < nodes.length; i++) {
            ids.push(nodes[i].id);
        }
        postData.roleId = roleId;
        postData.buttonId = buttonId;
        postData.menuIds = ids;
        requestUrl = urls.buttonAuthEdit;

        $.post(requestUrl, postData, function (result) {
            if (result.code == 200) {
                bootbox.alert("修改成功");
            } else {
                bootbox.alert(result.mes);
            }
        })
    },
    search: function () {
        buttonId = $("#select-button").val();
        var postData = {};
        postData.roleId = roleId;
        postData.buttonId = buttonId;
        var requestUrl;
        requestUrl = urls.buttonAuthData;
        $.post(requestUrl, postData, function (result) {
            if (result.code == 200) {
                var zNodes = result.data;
                zTreeObj = $.fn.zTree.init($("#ul-tree"), zTreesetting, zNodes);
            }
        });

    },
    cancel: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
}