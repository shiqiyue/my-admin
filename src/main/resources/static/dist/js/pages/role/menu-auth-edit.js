//页面请求链接
var urls = {
    edit: "/admin/role/menu/auth/edit"
}
// 操作表单(增加或者修改)el
var zTreeObj;
$(function () {
    var json = $("#hidden-json").val();
    var zNodes = JSON.parse(json);
    var setting = {
        check: {
            enable: true
            //chkboxType: {"Y": "", "N": ""},
            //nocheckInherit: true
        },
        data: {
            key: {
                checked: "ownMenuRights",
                url: "ccc"

            },
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentId"
            }
        }

    };
    zTreeObj = $.fn.zTree.init($("#ul-tree"), setting, zNodes);
})
// 操作，增删改查等等
var operate = {
    submit: function () {
        var nodes = zTreeObj.getCheckedNodes();
        var ids = [];
        for (var i = 0; i < nodes.length; i++) {
            ids.push(nodes[i].id);
        }
        var roleId = $("#hidden-id").val();
        var postData = {};
        postData.roleId = roleId;
        postData.menuIds = ids;
        $.post(urls.edit, postData, function (result) {
            if (result.code == 200) {
                bootbox.alert("修改成功", function () {
                    operate.cancel();
                })
            } else {
                bootbox.alert(result.mes);
            }
        })
    },
    cancel: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
}