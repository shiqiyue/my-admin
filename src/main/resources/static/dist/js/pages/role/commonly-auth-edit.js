//页面请求链接
var urlBase = "/admin/role";
var urls = {
    buttonAuthEdit: urlBase + "/commonly/auth/edit"
}
// 操作表单(增加或者修改)el
var $operate = $("#form-oper");

// 操作，增删改查等等
var operate = {
    submit: function () {
        $.post(urls.buttonAuthEdit, $operate.serialize(), function (result) {
            if (result.code == 200) {
                bootbox.alert("修改成功");
            } else {
                bootbox.alert(result.mes);
            }
        })
    },

    cancel: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
}