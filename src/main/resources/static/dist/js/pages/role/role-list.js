//页面请求链接
var urlBase = "/admin/role";

var urls = {
    list: urlBase + "/list",
    add: urlBase + "/add",
    edit: urlBase + "/edit",
    editMenuAuth: urlBase + "/menu/auth/edit",
    editButtonAuth: urlBase + "/button/auth/edit",
    editCommonlyAuth: urlBase + "/commonly/auth/edit",
    del: urlBase + "/del"
}
// 表格el
var $table = $("#table-data");
// 安全信息el
var $security = $("#div-security-info");

// 权限信息
var auths = {};
auths.add = $security.data("auth-add");
auths.del = $security.data("auth-delete");
auths.edit = $security.data("auth-edit");
auths.list = $security.data("auth-list");
auths.exportExcel = $security.data("auth-exportexcel");

// 自定义formatter
var customFormatter = {
    oper: function (value, row, index, field) {
        var result = "";
        var addHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-add' title='添加'><i class='fa fa-plus'></i></a></div>");
        var editHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit' title='修改'><i class='fa fa-edit'></i></a></div>");
        var delHtml = ("<div class='btn-group'><a class='btn btn-danger btn-md oper-delete' title='减少'><i class='fa fa-close'></i></a></div>");
        var editMenuAuthHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit-menu-auth' title='修改菜单权限'>菜单权限</a></div>");
        var editButtonAuthHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit-button-auth' title='修改按钮权限'>按钮权限</a></div>");
        var editCommonlyAuthorityHtml = ("<div class='btn-group'><a class='btn btn-default btn-md oper-edit-commonly-auth' title='修改一般权限'>一般权限</a></div>");
        if (auths.add) {
            result += addHtml;
        }
        if (auths.edit) {
            result += editHtml;
            result += editMenuAuthHtml;
            result += editButtonAuthHtml;
            result += editCommonlyAuthorityHtml;
        }
        if (auths.del) {
            result += delHtml;
        }
        return result;
    }
};
// 最终formatter
var formatter = $.extend({}, listCommon.formatter, customFormatter);

// 自定义表格事件
var customTableEvent = {
    "click .oper-edit": function (e, value, row, index) {
        operate.toEdit(row);
    },
    "click .oper-delete": function (e, value, row, index) {
        operate.del(row.id);
    },
    "click .oper-add": function (e, value, row, index) {
        operate.toAdd(row);
    },
    "click .oper-edit-menu-auth": function (e, value, row, index) {
        operate.toEditMenuAuth(row);
    },
    "click .oper-edit-button-auth": function (e, value, row, index) {
        operate.toEditButtonAuth(row);
    },
    "click .oper-edit-commonly-auth": function (e, value, row, index) {
        operate.toEditCommonlyAuth(row);
    }
};
// 最终表格事件
var tableEvent = $.extend({}, listCommon.tableEvent, customTableEvent);

// 自定义请求参数
var customRequestParams = {};
// 自定义表格参数
var customtableOption = {
    url: urls.list,
    treeShowField: 'roleName',
    parentIdField: 'parentId',
    pagination: false,
    sortName: "addDate",
    sortOrder: "desc",
    onLoadSuccess: function (data) {
        $table.treegrid({
            initialState: 'collapsed',// 收缩
            treeColumn: 0,// 指明第几列数据改为树形
            expanderExpandedClass: 'glyphicon glyphicon-triangle-bottom',
            expanderCollapsedClass: 'glyphicon glyphicon-triangle-right',
            onChange: function () {
                $table.bootstrapTable('resetWidth');
            }
        });
    },
    queryParams: function (params) {
        var requestParams = $.extend({}, customRequestParams);
        requestParams.order = params.order;
        requestParams.sort = params.sort;
        return requestParams;
    }
};
// 最终表格参数
var tableOption = $.extend({}, listCommon.tableOption, customtableOption);
$table.bootstrapTable(tableOption);

// 操作，增删改查等等
var operate = {
    tableRefresh: function (options) {
        /** 表格刷新 */
        var defaultOptions = {silent: true};
        var currentOptions = $.extend({}, defaultOptions, options);
        $table.bootstrapTable("refresh", currentOptions);
    },
    getTableSelectIds: function () {
        /** 返回表格选择的row的id */
        var ids = [];
        var selectRows = $table.bootstrapTable("getSelections");
        if (selectRows.length == 0) {
            return ids;
        }
        for (var i = 0; i < selectRows.length; i++) {
            ids.push(selectRows[i].id);
        }
        return ids;
    },
    toEdit: function (data) {
        listCommon.openIframeDialog("修改", urls.edit + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toEditMenuAuth: function (data) {
        listCommon.openIframeDialog("修改菜单权限", urls.editMenuAuth + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toEditButtonAuth: function (data) {
        listCommon.openIframeDialog("修改按钮权限", urls.editButtonAuth + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toEditCommonlyAuth: function (data) {
        listCommon.openIframeDialog("修改一般权限", urls.editCommonlyAuth + '?id=' + data.id, function () {
            operate.tableRefresh();
        });
    },
    toAdd: function (data) {
        var pid;
        if (data) {
            pid = data.id;
        } else {
            pid = "";
        }
        listCommon.openIframeDialog("添加", urls.add + '?parentId=' + pid, function () {
            operate.tableRefresh();
        });
    },
    del: function (id) {
        /** 删除 */
        bootbox.confirm("确定删除吗？", function (result) {
            if (!result) {
                return;
            }
            var postData = {};
            postData.id = id;
            $.post(urls.del, postData, function (result) {
                if (result.code == 200) {
                    bootbox.alert("删除成功", function () {
                        operate.tableRefresh();
                    })
                } else {
                    bootbox.alert(result.mes);
                }
            })
        });


    }
}