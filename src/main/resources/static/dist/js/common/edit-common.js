$(function() {
	if ($.fn.datepicker) {
		$('.date-picker').datepicker({
			autoclose : true,
			todayHighlight : true,
			language : "zh-CN",
			clearBtn : true
		});
    }
    ;


    $.ajaxSetup({
        beforeSend: function (evt, request, settings) {
            if (request && request.data && request.data.indexOf && request.data.indexOf("hideLoading=true") != -1) {
            } else {
                $(".overlay").show();
            }
        },
        complete: function () {
            $(".overlay").hide();
        }
    });
})

