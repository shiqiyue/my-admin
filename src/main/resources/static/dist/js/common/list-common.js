

$(function () {

    //设置日期控件
    if ($.fn.datepicker) {
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true,
            language: "zh-CN",
            clearBtn: true
        });
    }
    //获取会员信息
    listCommon.operate.getUserInfo();
})

var listCommon = {};

// 弹出iframe对话框
listCommon.openIframeDialog = function (title, url, callBackF, height, width) {
    title = title || "";
    var area = [];
    callBackF = callBackF || function () {
    };
    area = ["90%", "90%"];
    layer.open({
        type: 2,
        title: title,
        area: area,
        shadeClose: true,
        shade: true,
        shade: 0.8,
        maxmin: true,
        content: url,
        end: function () {
            callBackF();
        }
    });
};

// 列表公共formatter
listCommon.formatter = {
    /****
     * 转换时间戳为日期格式
     * @param value
     * @returns {string} yyyy-MM-dd
     */
    date: function (value) {
        if (value == null || value == "" || value == undefined) {
            return "";
        }
        var date = new Date(value);
        Y = date.getFullYear(), m = date.getMonth() + 1, d = date.getDate(), H = date.getHours(), i = date.getMinutes(), s = date.getSeconds();
        if (m < 10) {
            m = '0' + m;
        }
        if (d < 10) {
            d = '0' + d;
        }
        if (H < 10) {
            H = '0' + H;
        }
        if (i < 10) {
            i = '0' + i;
        }
        if (s < 10) {
            s = '0' + s;
        }
        var t = Y + '-' + m + '-' + d;
        return t;
    },
    /***
     * 转换时间戳为日期格式
     * @param value
     * @returns {string} yyyy-MM-dd hh:mm:ss
     */
    datetime: function (value) {
        if (value == null || value == "" || value == undefined) {
            return "";
        }
        var date = new Date(value);
        Y = date.getFullYear(), m = date.getMonth() + 1, d = date.getDate(), H = date.getHours(), i = date.getMinutes(), s = date.getSeconds();
        if (m < 10) {
            m = '0' + m;
        }
        if (d < 10) {
            d = '0' + d;
        }
        if (H < 10) {
            H = '0' + H;
        }
        if (i < 10) {
            i = '0' + i;
        }
        if (s < 10) {
            s = '0' + s;
        }
        var t = Y + '-' + m + '-' + d + ' ' + H + ':' + i + ':' + s;
        return t;
    },
    /***
     * 转换字符串为图像, 可选参数宽度（width） 和高度（height）,参数格式: {"width": "100", height :"100"}
     * @param value
     * @returns {string}
     */
    image: function (value) {
        var data = this.formatterData || {};
        var width = data.width || 50;
        var height = data.height || 50;
        return "<image src='" + value + "' width='" + width + "px' height='" + height + "px'></image>";
    },
    /***
     * 转换boolean为字符串， 格式为 {"true": "启用", "false": "禁用"}
     * @param value
     * @returns {*}
     */
    boolean: function (value) {
        var data = this.formatterData || {};
        var trueStr = data.true || "正确";
        var falseStr = data.false || "错误";
        if (value) {
            return trueStr;
        } else {
            return falseStr;
        }
    },
    /*****
     * 转换数字到字符串， 格式: {"1": "a", "2": "b"}
     * @param value
     * @returns {*|string}
     */
    enum: function (value) {
        var data = this.formatterData || {};
        var result = data[value] || "-";
        return result;
    }
};
// 列表页表格公共事件
listCommon.tableEvent = {};
// 表格插件公共参数
listCommon.tableOption = {
    classes: "table table-hover",
    method: "post",
    pagination: true,
    sidePagination: "server",
    contentType: "application/x-www-form-urlencoded",
    showRefresh: true,
    // cardView: true,
    sortName: "addDate",
    sortOrder: "desc",
    idField: "id",
    //工具栏
    toolbar: "#toolbar",
    /***
     * 没匹配记录时候显示内容
     * @returns {string}
     */
    formatNoMatches: function () {
        return "没有相关的匹配结果";
    },
    /***
     * 加载效果
     * @returns {string}
     */
    formatLoadingMessage: function () {
        var html = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        return html;
    },
    /***
     * 查询参数处理
     * @param params
     * @returns {{}}
     */
    queryParams: function (params) {
        // console.warn(params);
        var requestParams = {};
        requestParams.size = params.limit;
        requestParams.current = params.offset / params.limit + 1;
        requestParams.sortDirection = params.order;
        requestParams.sortColumn = params.sort;
        return requestParams;
    },
    /***
     * 返回信息处理
     * @param res
     * @returns {null}
     */
    responseHandler: function (res) {
        if (res.code == 200) {
            return res.data;
        } else {
            bootbox.alert(res.mes);
            return null;
        }
    }
};

listCommon.urls = {
    toEditUserInfo: "/admin/userinfo/edit",
    getUserInfo: "/admin/userinfo"
}

listCommon.operate = {
    /***
     * 前往修改当前会员信息
     */
    toEditUserInfo: function () {
        listCommon.openIframeDialog("修改信息", listCommon.urls.toEditUserInfo, function () {
            listCommon.operate.getUserInfo();
        })
    },
    /***
     * 获取当前会员信息
     */
    getUserInfo: function () {
        $.post(listCommon.urls.getUserInfo, {}, function (result) {
            if (result.code == 200) {
                var nickname = result.data.nickname;
                var photoUrl = result.data.photoUrl;
                $(".list-common-user-nickname").text(nickname);
                $(".list-common-user-photo").attr("src", photoUrl);
            }
        });
    }
}


//设置左边菜单-激活当前菜单
var currentPath = location.pathname;
var $sideBarMenu = $(".sidebar-menu");
$sideBarMenu.each(function () {
    var $this = $(this);
    var datas = $this.data("menu-json");
    var resultDatas = [];
    if (datas && datas.length > 0) {
        for (var i = 0; i < datas.length; i++) {
            var data = datas[i];
            if (data.parentId && data.parentId != 0) {
                for (var j = 0; j < datas.length; j++) {
                    var data2 = datas[j];
                    if (data2.id == data.parentId) {
                        if (!data2.sons) {
                            data2.sons = [];
                        }
                        data2.sons.push(data);
                        break;
                    }
                }
            } else {
                resultDatas.push(data);
            }
        }
    }
    buildTree($this, resultDatas);
})


/***
 * 构建菜单树
 * @param $parent
 * @param datas
 */
function buildTree($parent, datas) {
    var liEl = "<li></li>";
    var aEl = "<a></a>";
    var ulEl = "<ul></ul>"
    var iEl = "<i></i>";
    var spanEl = "<span></span>";
    var iAnagelEl = "<span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span>";
    for (var i = 0; i < datas.length; i++) {
        var data = datas[i];
        if (!data.hasAuthority) {
            continue;
        }
        var $li = $(liEl);
        var $a = $(aEl).attr("href", data.url);
        var $icon = $(iEl).addClass(data.icon);
        var $span = $(spanEl).text(data.name);
        $a.append($icon).append($span);
        $li.append($a);
        if (data.sons && data.sons.length > 0) {
            $li.addClass("treeview");
            $a.append($(iAnagelEl));
            var $ul = $(ulEl).addClass("treeview-menu");
            $li.append($ul);
            buildTree($ul, data.sons);
        }
        $parent.append($li);
    }
}

/***
 * 选择当前菜单
 */
$(".treeview a").each(function () {
    var $item = $(this);
    var itemHref = $item.attr("href");
    if (itemHref == currentPath) {
        $item.parents(".treeview").addClass("active");
    }
});