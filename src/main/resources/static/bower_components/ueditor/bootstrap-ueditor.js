;
(function ($) {


    $(function () {
        $("textarea[data-role='ueditor']").each(function () {
            var $item = $(this);
            var id = $item.attr("id");
            var value = $item.attr("value") || $item.data("value") || "";
            var now = new Date();
            var t = now.getTime();
            id = id || ("ueditor" + t);
            $item.attr("id", id);
            var editor = UE.getEditor(id, {
                initialContent: value
            });
        })
    })
})(jQuery);