;
(function ($) {

    var ZtreeSelect = {};

    var defaultSetting = {
        data: {
            key: {
                name: "roleName"
            },
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentId"
            }
        }
    };


    /***
     * 获取裁剪modal
     */
    ZtreeSelect.getModal = function getModal(data, settings) {
        var currentSettings = $.extend({}, defaultSetting, settings);
        var $modal;
        $modal = $('<div class="modal fade" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1"></div>');
        var $modal_dialog = $('<div class="modal-dialog modal-lg"></div>');
        var $modal_content = $('<div class="modal-content"></div>');
        var $modal_header = $('<div class="modal-header"><button class="close" data-dismiss="modal" type="button">&times;</button><h4 class="modal-title">请选择</h4></div>');
        var $modal_body = $('<div class="modal-body"></div>');
        var $modal_footer = $("<div class='modal-footer'></div>");
        var $row_button = $("<div class='row'></div>");
        var $row_button_col1 = $("<div class='col-md-12'></div>")
        var $button_confirm = $("<a class='btn btn-primary btn-confirm'>确定</a>");
        var $button_cancel = $("<a class='btn btn-danger btn-cancel'>取消</a>");
        var $row_ztree_ul = $('<div class="row"><div class="col-md-12"><ul class="ztree"></ul></div></div>');
        $row_button_col1.append($button_confirm).append($button_cancel);
        $row_button.append($row_button_col1);
        $modal_footer.append($row_button);
        $modal_body.append($row_ztree_ul);
        $modal_content.append($modal_header).append($modal_body).append($modal_footer);
        $modal_dialog.append($modal_content);
        $modal.append($modal_dialog);
        $("body").append($modal);
        zTreeObj = $.fn.zTree.init($row_ztree_ul.find(".ztree"), currentSettings, data);
        $modal.zTreeObj = zTreeObj;
        $modal.settings = currentSettings;
        return $modal;

    }
    $(function () {
        $("input[data-role='ztree-select']").each(function () {
            var $item = $(this);
            var value = $item.attr("value") || $item.data("value") || "";
            var data = $item.data("ztree-data");
            var now = new Date();
            var $hidden = $("<input type='hidden'>");
            $hidden.attr("name", $item.attr("name"));
            $item.removeAttr("name");
            $item.after($hidden);
            var settings = $item.data("ztree-settings");
            var $modal = ZtreeSelect.getModal(data, settings);
            $item.click(function () {
                $modal.modal("show");
            });
            $modal.find(".btn-confirm").click(function () {
                var selectRows = $modal.zTreeObj.getSelectedNodes();
                var selectRow = selectRows[0] || {};
                var name = selectRow[$modal.settings.data.key.name];
                var id = selectRow[$modal.settings.data.simpleData.idKey];
                $item.val(name);
                $hidden.val(id);
                $modal.modal("hide");
            });
            $modal.find(".btn-cancel").click(function () {
                $modal.modal("hide");
            });
            //设置默认选中的值
            $modal.zTreeObj.selectNode($modal.zTreeObj.getNodesByParam($modal.settings.data.simpleData.idKey, value)[0] || {});
            var selectRows = $modal.zTreeObj.getSelectedNodes();
            var selectRow = selectRows[0] || {};
            var name = selectRow[$modal.settings.data.key.name];
            var id = selectRow[$modal.settings.data.simpleData.idKey];
            $item.val(name);
            $hidden.val(id);
        })
    })
})(jQuery);