;
(function ($) {

    var PhotoCropper = {};
    PhotoCropper.defaultOptions = {
        aspectRatio: 16 / 9, // 比率
        uploadUrl: "/admin/file/image/upload", //上传图片到服务器的地址
        imageUrl: "/dist/img/blank.png", //初始图片地址
        uploadFileToServerSuccess: function () {
        } //点击保存成功后的时间
    };
    /***
     * 打开裁剪modal
     */
    PhotoCropper.cropper = function (options) {
        var currentOptions = $.extend({}, PhotoCropper.defaultOptions, options);
        var $modal = PhotoCropper.getModal();
        $modal.modal("show");
        var $cropper;
        var $image;
        var cropperOptions = {
            aspectRatio: currentOptions.aspectRatio,
            preview: $modal.find('.img-preview')
        }
        setTimeout(function () {
            $image = $modal.find(".img-upload");
            $image.cropper('destroy').cropper(cropperOptions);
            $cropper = $image.data('cropper');
        }, 500);
        // 设置上传文件表单的地址
        $modal.find("form").attr("action", currentOptions.uploadUrl);
        //设置初始图片
        $modal.find(".img-upload").attr("src", currentOptions.imageUrl);
        //$modal.find(".file-upload").val(currentOptions.initImageUrl);
        //设置保存修改按钮点击事件
        $modal.find(".button-save").click(function () {
            if ($modal.find(".file-upload").val().trim().length == 0) {
                bootbox.alert("请选择上传图片");
                return;
            }
            //获取裁剪信息
            var cropperInfo = {};
            cropperInfo = $cropper.getData();
            $modal.find(".file-upload-cropper").val(JSON.stringify(cropperInfo));
            //ajax提交
            $modal.find("form").ajaxSubmit(function (result) {
                if (result.code == 200) {
                    currentOptions.uploadFileToServerSuccess(result.data);
                    $modal.modal("hide");
                } else {
                    bootbox.alert(result.mes);
                }
            })
        })
        //设置上传图片按钮点击事件
        $modal.find(".button-file-upload").click(function () {
            $modal.find(".file-upload").click();
        })
        //设置图片变化-自动变化预览图片
        $modal.find(".file-upload").change(function () {
            var files = this.files;
            var file;
            if (!$image.data('cropper')) {
                return;
            }
            if (files && files.length) {
                file = files[0];
                if (/^image\/\w+$/.test(file.type)) {
                    var uploadedImageURL = URL.createObjectURL(file);
                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(cropperOptions);
                    $cropper = $image.data('cropper');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    }


    /***
     * 获取裁剪modal
     */
    PhotoCropper.getModal = function getModal() {
        var $modal;

        $modal = $('<div class="modal fade"   aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1"></div>');
        var $modal_dialog = $('<div class="modal-dialog modal-lg"></div>');
        var $modal_content = $('<div class="modal-content"></div>');
        var $form_upload_file = $('<form class="form-file-upload" enctype="multipart/form-data" method="post"></form>');
        var $modal_header = $('<div class="modal-header"><button class="close" data-dismiss="modal" type="button">&times;</button><h4 class="modal-title">上传图片</h4></div>');
        var $modal_body = $('<div class="modal-body"></div>');
        var $row_upload_file = $('<div class="row"><div class="col-md-3 col-md-offset-9"><input class="file-upload" style="display: none"  name="upfile" type="file" accept="image/*"><input class="file-upload-cropper"   name="cropperData" type="hidden" accept="image/*"></div></div>')
        var $row_cropper = $('<div class="row"><div class="col-md-9"><div class="img-container"><img class="img-upload"  alt="请上传图片"></div></div><div class="col-md-3"><div class="docs-preview clearfix"><div class="img-preview preview-lg"></div><div class="img-preview preview-md"></div><div class="img-preview preview-sm"></div><div class="img-preview preview-xs"></div></div></div></div>');
        var $row_upload_server = $('<div class="row"><div class="col-md-9"><button class="btn btn-primary button-file-upload" type="button"><i class="fa fa-upload"></i>选择图片</button><button class="btn btn-success  button-save pull-right" type="button"><i class="fa fa-save"></i>保存修改</button></div></div>');
        $modal_body.append($row_upload_file).append($row_cropper).append($row_upload_server);
        $form_upload_file.append($modal_header).append($modal_body);
        $modal_content.append($form_upload_file);
        $modal_dialog.append($modal_content);
        $modal.append($modal_dialog);
        $("body").append($modal);


        return $modal;

    }


    $(function () {
        $("input[data-role='photocrop']").each(function () {
            var $item = $(this);
            var itemOptions = {};

            $item.addClass("hidden-upload-info");
            $item.attr("type", "hidden");
            var $button = $("<a class='btn btn-default btn-cropper-upload' >点击上传</a>");
            var $preview = $("<div class='preview'></div>");
            var $img = $("<img class='img-upload-info img-thumbnail'  title='图片预览' width='300px' height='300px' style='max-width: 100%'>");
            //var $hidden = $("<input  class='hidden-upload-info' type='hidden' />");
            if ($item.data("aspect-ratio")) {
                itemOptions.aspectRatio = $item.data("aspect-ratio")
            }
            if ($item.data("upload-url")) {
                itemOptions.uploadUrl = $item.data("upload-url");
            }
            itemOptions.imageUrl = $item.data("image-url") || $item.val() || PhotoCropper.defaultOptions.imageUrl;
            itemOptions.uploadFileToServerSuccess = function (data) {
                $img.attr("src", data.url);
                $item.val(data.url);
            }

            $img.attr("src", itemOptions.imageUrl);

            $button.click(function () {
                PhotoCropper.cropper(itemOptions);
            })
            $preview.append($img);
            $item.after($button).after($preview);
            //$item.hide();
        })
    })
})(jQuery);