﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="util" uri="functions"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/WEB-INF/jsp/common/list_top.jsp"%>
</head>
<body class="no-skin">
	<!-- /section:basics/navbar.layout -->
	<div class="main-container" id="main-container">
		<!-- /section:basics/sidebar -->
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div>
							<!-- 检索  -->
							<form class="form-inline" method="post" name="Form" id="Form">
								<input type="hidden" name="sortColumn" v-model="pageInfo.sortColumn">
								<input type="hidden" name="sortDirection" v-model="pageInfo.sortDirection">
								<div class="form-group">
									<input class="form-control date-picker" name="startDate" value="${r"${util:getDateTime(queryParam.startDate)}"}" type="text" data-date-format="yyyy-mm-dd" readonly="readonly"
										placeholder="开始日期" title="开始日期" />
								</div>
								<div class="form-group">
									<input class="form-control date-picker" name="endDate" value="${r"${util:getDateTime(queryParam.endDate)}"}" type="text" data-date-format="yyyy-mm-dd" readonly="readonly"
										placeholder="结束日期" title="结束日期" />
								</div>
								<div class="form-group">
									<select class=" form-control">
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>

								<c:if test="${r"${util:listAble() }"}">
									<a class="form-control btn btn-comfirm" onclick="toSearch();" title="检索">
										<i id="nav-search-icon" class="ace-icon fa fa-search bigger-110 nav-search-icon blue"></i>
									</a>
								</c:if>
								<c:if test="${r"${util:exportExcelAble() }"}">
									<a class="btn btn-comfirm  form-control" onclick="toExcel();" title="导出到EXCEL">
										<i id="nav-search-icon" class="ace-icon fa fa-download bigger-110 nav-search-icon blue"></i>
									</a>
								</c:if>
							</form>
							</div>
							<!-- 检索  -->
							<div class="row" v-cloak>
								<table id="simple-table" class="table table-striped table-bordered table-hover" style="margin-top: 5px;">
									<thead>
										<tr>
											<th class="center" style="width: 35px;">
												<label class="pos-rel">
													<input type="checkbox" class="ace" id="zcheckbox" />
													<span class="lbl"></span>
												</label>
											</th>
											<th class="center" style="width: 50px;">序号</th>
											<#list fieldList as var>
											<th class="center" data-column-sortable="true" data-column-name="${var[0]}">${var[2]}</th>
											</#list>
											<th class="center" data-column-sortable="true" data-column-name="addDate">添加时间</th>
											<th class="center">操作</th>
										</tr>
									</thead>

									<tbody>
										<!-- 开始循环 -->
										<c:if test="${r"${util:listAble() }"}">
										<tr v-for="(item,index) in data">
											<td class='center'>
												<label class="pos-rel"><input type='checkbox' name='ids' :value="item.id" class="ace" /><span class="lbl"></span></label>
											</td>
											<td class='center' style="width: 30px;">{{index + 1}}</td>
										<#list fieldList as var>
											<#if var[1] == 'Date'>
											<td class='center'>{{item.${var[0]} | datetime}}</td>
											<#else>
											<td class='center'>{{item.${var[0]}}}</td>
											</#if>
											
										</#list>
											<td class='center'>{{item.addDate | datetime}}</td>
											<td class="center">
												<c:if test="${r"${!util:editAble() && !util:deleteAble() }"}">
												<span class="label label-large label-grey arrowed-in-right arrowed-in"><i class="ace-icon fa fa-lock" title="无权限"></i></span>
												</c:if>
												<div class="hidden-sm hidden-xs btn-group">
													<c:if test="${r"${util:editAble() }"}">
													<a class="btn btn-xs btn-success" title="编辑" @click="edit(item.id);">
														<i class="ace-icon fa fa-pencil-square-o bigger-120" title="编辑"></i>
													</a>
													</c:if>
													<c:if test="${r"${util:deleteAble() }"}">
													<a class="btn btn-xs btn-danger" @click="del(item.id);">
														<i class="ace-icon fa fa-trash-o bigger-120" title="删除"></i>
													</a>
													</c:if>
												</div>
											</td>
										</tr>
									
									</c:if>
									<c:if test="${r"${!util:listAble() }"}">
										<tr>
											<td colspan="100" class="center">您无权查看</td>
										</tr>
									</c:if>

									</tbody>
								</table>
								<div class="page-header position-relative">
									<table style="width: 100%;">
										<tr>
											<td style="vertical-align: top;">
												<c:if test="${r"${util:addAble() }"}">
													<a class="btn btn-sm btn-success" @click="add();">新增</a>
												</c:if>
												<c:if test="${r"${util:deleteAble() }"}">
													<a class="btn btn-sm btn-danger" @click="deleteAll" title="批量删除" ><i class='ace-icon fa fa-trash-o bigger-120'></i></a>
												</c:if>
											</td>
											<!-- 分页的底部 -->
											<td style="vertical-align: top;">
												<nav style="float: right">
													<ul id="ul_page" class="pagination">
													</ul>
												</nav>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.page-content -->
		</div>
	</div>
	<!-- /.main-content -->

	<!-- 返回顶部 -->
	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>

	</div>
	<!-- /.main-container -->

	<!-- basic scripts -->
	<!-- 页面底部js¨ -->
	<%@ include file="/WEB-INF/jsp/common/list_foot.jsp"%>
	<script type="text/javascript">
		$(function() {
			toSearch();
			$("#simple-table").tableSorter(function(columnName, sort) {
				vueData.pageInfo.sortColumn = columnName;
				vueData.pageInfo.sortDirection = sort;
				refresh();
			})
		});
		//检索
		function toSearch() {
			var queryParam = $('#Form').parseForm();
			$.extend(vueData.pageInfo, queryParam);
			firstPage();
		}
		//刷新
		function refresh() {
			pageSearch(vueData.pageInfo.current, vueData.pageInfo.size, true);
		}
		//跳转到第一页
		function firstPage() {
			pageSearch(1, vueData.pageInfo.size, true);
		}
		//数据
		var vueData = {};
		//分页数据
		vueData.pageInfo = {};
		vueData.pageInfo.total = 0;
		vueData.pageInfo.size = 10;
		vueData.pageInfo.current = 1;
		vueData.pageInfo.sortColumn = "addDate";
		vueData.pageInfo.sortDirection = "DESC";
		vueData.data = [];

		var pageVm = new Vue({
			el : '#main-container',
			data : vueData,
			methods : {
				edit : function(id) {
					top.showDialog("修改", "/admin/${objectNameLower}/edit?id=" + id, 1000, 750, function() {
						refresh();
					});
				},
				del : function(id) {
					//删除
					bootbox.confirm("确定要删除吗?", function(result) {
						if (!result) {
							return;
						}
						var postData = {};
						postData.id = id;
						$.post("/admin/${objectNameLower}/delete", postData, function(result) {
							if (result.code == 200) {
								bootbox.alert("成功", function() {
									refresh();
								})
							}
						});
					});
				},
				add : function() {
					//添加
					top.showDialog("添加", "/admin/${objectNameLower}/add", 1000, 750, function() {
						refresh();
					});
				},
				deleteAll : function() {
					//批量删除
					bootbox.confirm("你确认要删除选中的数据吗？", function(result) {
						if (!result) {
							return;
						}
						var ids = [];
						$("[name='ids']:checked").each(function(i, item) {
							ids.push($(item).val());
						});
						var postData = {};
						postData.ids = ids;
						$.post("/admin/${objectNameLower}/deleteAll", postData, function(result) {
							if (result.code == 200) {
								refresh();
							} else {
								bootbox.alert(data.mes);
							}
						});
					});
				}

			}
		})

		//分页查询
		function pageSearch(pageIndex, pageSize, resetPage) {
			vueData.pageInfo.current = pageIndex;
			vueData.pageInfo.size = pageSize;
			$.post("/admin/${objectNameLower}/list", vueData.pageInfo, function(result) {
				if (result.code == 200) {
					vueData.data = [];
					var resultData = result.data.data;
					vueData.pageInfo.total = result.data.total;
					//是否重置查询插件
					if (resetPage) {
						resetPagePlugin();
					}
					for (var i = 0; i < resultData.length; i++) {
						vueData.data.push(resultData[i]);
					}
					//没有数据,自动查询上一页
					if (resultData.length == 0) {
						if (vueData.pageInfo.current > 1) {
							pageSearch(vueData.pageInfo.current - 1, vueData.pageInfo.size, true);
						}
					}
				}

			})

		}

		//重置分页插件
		function resetPagePlugin() {
			BootstrapPagination($("#ul_page"), {
				total : vueData.pageInfo.total,
				pageIndex : vueData.pageInfo.current - 1,
				pageGroupSize : 5,
				pageSize : vueData.pageInfo.size,
				pageChanged : function(pageIndex, pageSize) {
					pageSearch(pageIndex + 1, pageSize);
				},
			});
		}

		//导出excel
		function toExcel() {
			var preAction = $("#Form").attr("action");
			$("#Form").attr("action", "/admin/${objectNameLower}/excel");
			$("#Form").submit();
			$("#Form").attr("action", preAction);
		}
	</script>


</body>
</html>