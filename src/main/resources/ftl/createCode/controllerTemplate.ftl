package ${basePackage}.controller.${packageName}.${objectNameLower};

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ${basePackage}.controller.base.BaseController;
import ${basePackage}.anontation.Authority;
import ${basePackage}.anontation.AuthorityConfig;
import ${basePackage}.enums.RightType;
import ${basePackage}.util.excel.ObjectExcelView;
import ${basePackage}.util.security.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.Lists;
import ${basePackage}.util.date.DateUtil;
import com.baomidou.mybatisplus.plugins.Page;
import ${basePackage}.vo.rep.base.RepBaseVO;
import ${basePackage}.vo.req.page.ReqPageVO;
import ${basePackage}.service.${packageName}.${objectNameLower}.${objectName}Service;
import ${basePackage}.entity.${packageName}.${objectName};
import ${basePackage}.anontation.SysLog;
import ${basePackage}.common.RegexConst;
import ${basePackage}.common.AuthorityCommon;

/**
*  @author ${author}
*
*  <p>说明：${TITLE}控制器<br/>
*  创建时间：${nowDate?string("yyyy-MM-dd")}</p>
*/
@Controller
@AuthorityConfig(prefix = "/admin/${objectNameLower}/list")
@RequestMapping(value="admin/${objectNameLower}")
public class ${objectName}Controller extends BaseController {

	/** 菜单地址(权限用) */
	final String menuUrl = "/admin/${objectNameLower}/list"; 
	@Autowired
	private ${objectName}Service ${objectNameLower}Service;
	
	/**
     * 去列表页面
     * 
     */
    @Authority(name = AuthorityCommon.LIST)
    @GetMapping(value = "/list")
    public ModelAndView goPage(@RequestParam Map<String, Object> map)  {
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("${packageName}/${objectNameLower}/${objectNameLower}_list");
        mv.addObject("queryParam", map);
        return mv;
    }
	
	/**
	 * 列表数据
	 */
	@SysLog("查询${TITLE}列表数据")
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Authority(name = AuthorityCommon.LIST)
	@PostMapping(value="/list")
	@ResponseBody
	public RepBaseVO list(ReqPageVO pageVO, @RequestParam Map<String, Object> map) {
		RepBaseVO rep = new RepBaseVO<>();
		processParams(map);
		Page<${objectName}> page = processPage(pageVO);
		page = ${objectNameLower}Service.list(page, map);
		List<${objectName}> varList = page.getRecords();
        Map<String, Object> resultInfo = new HashMap<>(2);
        resultInfo.put("data", varList);
        resultInfo.put("total", page.getTotal());
        return rep.setCommonSuccess().setData(resultInfo);
	}
	
	/**
	 * 去新增页面
	 */
	@Authority(name = AuthorityCommon.ADD)
	@GetMapping(value="/add")
	public ModelAndView goAdd(@RequestParam Map<String, Object> params){
		ModelAndView mv = this.getModelAndView();
		mv.setViewName("${packageName}/${objectNameLower}/${objectNameLower}_edit");
		${objectName} entity = new ${objectName}();
		mv.addObject("action", "add");
		mv.addObject("entity", entity);
		mv.addObject("params", params);
		return mv;
	}	
	
	/**
	 * 保存
	 */
	@SysLog("添加${TITLE}数据")
	@Authority(name = AuthorityCommon.ADD)
	@PostMapping(value="/add")
	@ResponseBody
	public RepBaseVO save(${objectName} entity) {
		RepBaseVO rep = new RepBaseVO();
		entity.setVersion(1L);
		entity.setAddDate(new Date());
		${objectNameLower}Service.insert(entity);
		return rep.setCommonSuccess();
	}
	
	
	 /**
	 * 去修改页面
	 */
	@Authority(name = AuthorityCommon.EDIT)
	@GetMapping(value="/edit")
	public ModelAndView goEdit(${idTypeStr} id){
		ModelAndView mv = this.getModelAndView();
${objectName} entity = ${objectNameLower}Service.selectById(id);
		mv.setViewName("${packageName}/${objectNameLower}/${objectNameLower}_edit");
		mv.addObject("action", "edit");
		mv.addObject("entity", entity);
		return mv;
	}	
	
	/**
	 * 修改
	 */
	@SysLog("修改${TITLE}数据")
	@Authority(name = AuthorityCommon.EDIT)
	@PostMapping(value="/edit")
	@ResponseBody
	public RepBaseVO edit(${objectName} entity) {
		RepBaseVO rep = new RepBaseVO<>();
		${objectNameLower}Service.updateById(entity);
		return rep.setCommonSuccess();
	}
	
	/**
	 * 删除
	 */
	@SysLog("删除${TITLE}数据")
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value="/delete")
	@ResponseBody
	public RepBaseVO delete(${idTypeStr} id) {
		${objectNameLower}Service.deleteById(id);
		return RepBaseVO.newIntance().setCommonSuccess();
	}
	

	/**
	 * 批量删除
	 */
	@SysLog("批量删除${TITLE}数据")
	@SuppressWarnings({ "rawtypes" })
	@Authority(name = AuthorityCommon.DELETE)
	@PostMapping(value = "/deleteAll")
	@ResponseBody
	public RepBaseVO deleteAll(@RequestParam("ids[]") List<String> ids) {
    RepBaseVO rep = new RepBaseVO<>();
		if (null != ids && !ids.isEmpty()) {
			${objectNameLower}Service.deleteBatchIds(ids);
		}
    return rep.setCommonSuccess();
	}


    /**
	 * 导出到excel
	 */
	@SysLog("导出${TITLE}excel")
	@Authority(name = AuthorityCommon.EXPORT_EXCEL)
	@PostMapping(value="/excel")
	public ModelAndView exportExcel(@RequestParam Map<String, Object> map) {
		ModelAndView mv = new ModelAndView();
		processParams(map);
		Map<String,Object> dataMap = new HashMap<String,Object>(2);
		List<String> titles = new ArrayList<String>();
<#list fieldList as var>
		titles.add("${var[2]}");
</#list>
		dataMap.put("titles", titles);
		List<${objectName}> varOList = ${objectNameLower}Service.listAll(map);
		List<Map<String, Object>> varList = new ArrayList<Map<String, Object>>();
		for(${objectName} entity: varOList){
			Map<String, Object> vpd = new HashMap<String, Object>(30);
<#list fieldList as var>
			vpd.put("var${var_index+1}", entity.get${var[7]}());
</#list>
			varList.add(vpd);
		}

		dataMap.put("varList", varList);
		ObjectExcelView erv = new ObjectExcelView();
		mv = new ModelAndView(erv,dataMap);
		return mv;
	}

	/***
	 * 处理输入参数
        *
	 * @param map
	 */
	private void processParams(Map<String, Object> map) {
		String sStartDate = (String) map.get("startDate");
		String sEndDate = (String) map.get("endDate");
		if (StringUtils.isBlank(sStartDate)) {
			map.put("startDate", null);
		} else {
			map.put("startDate", DateUtil.getStartTimeOfDate(sStartDate));
		}
		if (StringUtils.isBlank(sEndDate)) {
			map.put("endDate", null);
		} else {
			map.put("endDate", DateUtil.getEndTimeOfDate(sEndDate));
		}
		String sortDirection = (String) map.get("sortDirection");
		String sortColumn = (String) map.get("sortColumn");
		if (StringUtils.isNotBlank(sortColumn) && !sortColumn.matches(RegexConst.WORD)) {
			map.remove("sortColumn");
		}
		if (StringUtils.isNotBlank(sortDirection) && !sortDirection.matches(RegexConst.WORD)) {
			map.remove("sortDirection");
		}
	}


        /***
	 * 处理分页参数
        *
	 * @param pageVO
	 * @return
	 */
	private Page<${objectName}> processPage(ReqPageVO pageVO) {
		Page<${objectName}> page = new Page<>(pageVO.getCurrent(), pageVO.getSize());
		return page;
	}

}
