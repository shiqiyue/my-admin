
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `${tabletop}${objectTable}`
-- ----------------------------
DROP TABLE IF EXISTS `${tabletop}${objectTable}`;
CREATE TABLE `${tabletop}${objectTable}` (
		<#if idType == 1>
		`id` varchar(100) NOT NULL,
		<#elseif idType == 2>
		`id` bigint(20) NOT NULL AUTO_INCREMENT,
		<#elseif idType == 3>
		`id` bigint(20) NOT NULL,
		<#elseif idType == 4>
		`id` varchar(100) NOT NULL,
		</#if>
 		
	<#list fieldList as var>
		<#if var[1] == 'Integer'>
		`${var[0]}` int(${var[5]}) NOT NULL COMMENT '${var[2]}',
		<#elseif var[1] == 'Double'>
		`${var[0]}` double(${var[5]},3) NOT NULL COMMENT '${var[2]}',
		<#elseif var[1] == 'Date'>
		`${var[0]}` datetime(3) NOT NULL COMMENT '${var[2]}',
		<#elseif var[1] == 'Long'>
		`${var[0]}` bigint(${var[5]}) NOT NULL COMMENT '${var[2]}',
		<#elseif var[1] == 'Short'>
		`${var[0]}` smallint(${var[5]}) NOT NULL COMMENT '${var[2]}',
		<#else>
		`${var[0]}` varchar(${var[5]}) DEFAULT NULL COMMENT '${var[2]}',
		</#if>
	</#list>
	
		`version` bigint(20) NOT NULL default 1,
 		`addDate` datetime(3) NOT NULL,
  		PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
