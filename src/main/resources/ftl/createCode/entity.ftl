package ${basePackage}.entity.${packageName};

import java.util.Date;
import ${basePackage}.entity.base.*;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;


/**
*  @author ${author}
*
*  <p>说明：${TITLE}<br/>
*  创建时间：${nowDate?string("yyyy-MM-dd")}</p>
*/
@Data
@TableName(value = "${tabletop}${objectTable}")
<#if idType == 1>
public class ${objectName} extends BaseUuidEntity{
<#elseif idType == 2>
public class ${objectName} extends BaseAutoIncEntity{
<#elseif idType == 3>
public class ${objectName} extends BaseIdWorkEntity{
<#elseif idType == 4>
public class ${objectName} extends BaseEntity{
</#if>
	private static final long serialVersionUID = 1L;
	
	<#list fieldList as var>
		<#if var[1] == 'Date'>
	/** ${var[2]} */
	private Date ${var[6]};
		<#elseif var[1] == 'Integer'>
	/** ${var[2]} */	
	private Integer ${var[6]};
		<#elseif var[1] == 'Double'>
	/** ${var[2]} */
	private Double ${var[6]};
		<#elseif var[1] == 'Long'>
	/** ${var[2]} */
	private Long ${var[6]};
		<#elseif var[1] == 'Short'>
	/** ${var[2]} */
	private Short ${var[6]};
		<#else>
	/** ${var[2]} */
	private String ${var[6]};
		</#if>
	</#list>

	
	
	
}