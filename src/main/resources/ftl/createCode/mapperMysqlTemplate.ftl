<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${basePackage}.mapper.${packageName}.${objectName}Mapper">
	<resultMap type="${basePackage}.entity.${packageName}.${objectName}" id="${objectNameCamel}Map">
		<id column="id" property="id" />
		<result column="version" property="version" />
		<result column="addDate" property="addDate" />
		<#list fieldList as var>
		<result column="${var[0]}" property="${var[6]}" />
		</#list>
	</resultMap>
	
	<!--表名 -->
	<sql id="tableName">
		${tabletop}${objectTable}
	</sql>
	
	
	<!-- 列表 -->
	<select id="dataList" parameterType="map" resultMap="${objectNameCamel}Map">
		select
		*
		from 
		<include refid="tableName"></include>
		where 1=1
		<if test="map.keywords!= null and map.keywords != ''"><!-- 关键词检索 -->
			and
				(
				<!--	根据需求自己加检索条件
					字段1 LIKE CONCAT(CONCAT('%', ${r"#{map.keywords})"},'%')
					 or 
					字段2 LIKE CONCAT(CONCAT('%', ${r"#{map.keywords})"},'%') 
				-->
				)
		</if>
		<if test="map.startDate != null">
			and addDate &gt;= ${r"#{map.startDate}"}
		</if>
		<if test="map.endDate != null">
			and addDate &lt;= ${r"#{map.endDate}"}
		</if>
		<if test="map.sortColumn != null and map.sortColumn != ''">
			order by ${r"${map.sortColumn}"} 
			<if test="map.sortDirection != null and map.sortDirection != ''">
				${r"${map.sortDirection}"} 
			</if>
		</if>
	</select>
	
	
	
</mapper>