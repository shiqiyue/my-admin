package ${basePackage}.mapper.${packageName};

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import ${basePackage}.entity.${packageName}.${objectName};

/**
*  @author ${author}
*
*  <p>说明：${TITLE} mapper<br/>
*  创建时间：${nowDate?string("yyyy-MM-dd")}</p>
*/
public interface ${objectName}Mapper extends BaseMapper<${objectName}>{

	/***
	 * 分页
	 * 
	 * @param page
	 * @param map
	 * @return
	 */
	public List<${objectName}> dataList(Pagination page, @Param("map") Map<String, Object> map);

	/***
	 * 列表
	 * 
	 * @param map
	 * @return
	 */
	public List<${objectName}> dataList(@Param("map") Map<String, Object> map);

}
