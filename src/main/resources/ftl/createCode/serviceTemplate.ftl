package ${basePackage}.service.${packageName}.${objectNameLower};

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import ${basePackage}.entity.${packageName}.${objectName};
import ${basePackage}.mapper.${packageName}.${objectName}Mapper;

/**
*  @author ${author}
*
*  <p>说明：${TITLE} 服务<br/>
*  创建时间：${nowDate?string("yyyy-MM-dd")}</p>
*/
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class ${objectName}Service extends ServiceImpl<${objectName}Mapper, ${objectName}>{


	/**
	 * 列表
	 * 
	 * @param page
	 * @
	 */
	public Page<${objectName}> list(Page<${objectName}> page, Map<String, Object> map)  {
	    page.setRecords(baseMapper.dataList(page, map));
        return page;
	}

	/**
	 * 列表(全部)
	 * 
	 * @param pd
	 * @
	 */
	public List<${objectName}> listAll(Map<String, Object> map)  {
		return baseMapper.dataList(map);
	}

}

