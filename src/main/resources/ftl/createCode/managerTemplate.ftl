package com.szt.service.${packageName}.${objectNameLower};

import java.util.List;
import ${basePackage}.mapper.Page;
import com.szt.util.PageData;
import ${basePackage}.mapper.${packageName}.${objectName};

/** 
 * 说明： ${TITLE}接口
 * 创建时间：${nowDate?string("yyyy-MM-dd")}
 * @version
 */
public interface ${objectName}Manager{

	/**新增
	 * @param pd
	 * @throws Exception
	 */
	public void save(${objectName} pd)throws Exception;
	
	/**删除
	 * @param pd
	 * @throws Exception
	 */
	public void delete(PageData pd)throws Exception;
	
	/**修改
	 * @param pd
	 * @throws Exception
	 */
	public void edit(${objectName} pd)throws Exception;
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	public List<${objectName}> list(Page page)throws Exception;
	
	/**列表(全部)
	 * @param pd
	 * @throws Exception
	 */
	public List<${objectName}> listAll(PageData pd)throws Exception;
	
	/**通过id获取数据
	 * @param pd
	 * @throws Exception
	 */
	public ${objectName} findById(String pd)throws Exception;
	
	/**批量删除
	 * @param ArrayDATA_IDS
	 * @throws Exception
	 */
	public void deleteAll(String[] ArrayDATA_IDS)throws Exception;
	
}

