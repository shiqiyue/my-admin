<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/jsp/common/edit_top.jsp"%>
</head>
<body class="no-skin">
<!-- /section:basics/navbar.layout -->
<div class="main-container" id="main-container">
	<!-- /section:basics/sidebar -->
	<div class="main-content">
		<div class="main-content-inner">
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12">
					
					<form class="form-horizontal"  name="Form" id="Form" method="post">
						<input type="hidden" name="id"  value="${r"${entity.get"}Id()${r"}"}"/>
				<#list fieldList as var>
					<#if var[3] == "是">
						<#if var[1] == 'Date'>
						<div class="form-group">
							<label class="col-sm-2 control-label">${var[2] }:</label>
							<div class="col-sm-10">
								<input readonly="readonly" type="text" data-date-format="yyyy-mm-dd"  class="form-control date-picker notEmpty" value="${r"${entity.get"}${var[7] }()${r"}"}" name="${var[6] }" placeholder="${var[2] }" >
							</div>
						</div>
						<#elseif var[1] == 'Integer'>
						<div class="form-group">
							<label class="col-sm-2 control-label">${var[2] }:</label>
							<div class="col-sm-10">
								<input class="form-control integer notEmpty"  value="${r"${entity.get"}${var[7] }()${r"}"}" name="${var[6] }" maxlength="${var[5] }" placeholder="${var[2] }">
							</div>
						</div>
						<#elseif var[1] == 'Long'>
						<div class="form-group">
							<label class="col-sm-2 control-label">${var[2] }:</label>
							<div class="col-sm-10">
								<input class="form-control integer notEmpty"  value="${r"${entity.get"}${var[7] }()${r"}"}" name="${var[6] }" maxlength="${var[5] }" placeholder="${var[2] }">
							</div>
						</div>
						<#elseif var[1] == 'Short'>
						<div class="form-group">
							<label class="col-sm-2 control-label">${var[2] }:</label>
							<div class="col-sm-10">
								<input class="form-control integer notEmpty"  value="${r"${entity.get"}${var[7] }()${r"}"}" name="${var[6] }" maxlength="${var[5] }" placeholder="${var[2] }">
							</div>
						</div>
						<#else>
						<div class="form-group">
							<label class="col-sm-2 control-label">${var[2] }:</label>
							<div class="col-sm-10">
								<input class="form-control  notEmpty"  value="${r"${entity.get"}${var[7] }()${r"}"}" name="${var[6] }" maxlength="${var[5] }" placeholder="${var[2] }">
							</div>
						</div>
						</#if>
					</#if>
				</#list>
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2">
								<a class="btn btn-mini btn-primary" onclick="save();">保存</a>
								<a class="btn btn-mini btn-danger" onclick="top.Dialog.close();">取消</a>
							</div>
						</div>
					</form>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.page-content -->
		</div>
	</div>
	<!-- /.main-content -->
</div>
<!-- /.main-container -->


	<!-- 页面底部js¨ -->
	<%@ include file="/WEB-INF/jsp/common/edit_foot.jsp"%>
	<script type="text/javascript">
	//保存
	function save(){
		if (!myValidation.commonValid("Form")){
			return false;
		}

		$.post("/admin/${objectNameLower}/${r"${action }"}", $("#Form").serialize(),function(result){
			if(result.code == 200){
				bootbox.alert("成功", function(){
					top.Dialog.close();
				});
			}else{
				bootbox.alert(result.mes);
			}
		})
	}
	
	
	</script>
</body>
</html>