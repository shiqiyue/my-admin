ALTER TABLE `sys_user`
  CHANGE COLUMN `allRights` `allAuthority` TINYINT(1) NOT NULL DEFAULT 0
COMMENT '是否拥有所有权限'
  AFTER `editable`;


ALTER TABLE `sys_user`
  MODIFY COLUMN `roleId` CHAR(32) CHARACTER SET utf8
COLLATE utf8_general_ci NOT NULL
  AFTER `version`;


