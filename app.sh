#!/bin/sh

## java env
export JAVA_EXEC_PATH=/root/software/jre1.8.0_131/bin/java

## you just need to change this param name
APP_NAME=sy

SERVICE_DIR=/root/application/$APP_NAME
SERVICE_NAME=$APP_NAME
JAR_NAME=$SERVICE_NAME\.war
PID=$SERVICE_NAME\.pid

cd $SERVICE_DIR

case "$1" in

    start)
        nohup $JAVA_EXEC_PATH -Xms128m -Xmx512m -Djava.security.egd=file:/dev/./urandom  -Dspring.profiles.active=test -jar $JAR_NAME >/dev/null 2>&1 &
        echo $! > $SERVICE_DIR/$PID
        echo "=== start $SERVICE_NAME"
        ;;

    stop)
	echo `cat $SERVICE_DIR/$PID`
        kill `cat $SERVICE_DIR/$PID`
        rm -rf $SERVICE_DIR/$PID
        echo "=== stop $SERVICE_NAME"

        sleep 5
        P_ID=`ps -ef | grep -w "$SERVICE_NAME" | grep java | grep -v "grep" | awk '{print $2}'`
        if [ "$P_ID" == "" ]; then
            echo "=== $SERVICE_NAME process not exists or stop success"
        else
            echo "=== $SERVICE_NAME process pid is:$P_ID"
            echo "=== begin kill $SERVICE_NAME process, pid is:$P_ID"
            kill -9 $P_ID
        fi
        ;;

    restart)
        $0 stop
        sleep 2
        $0 start
        echo "=== restart $SERVICE_NAME"
        ;;

    *)
        ## restart
        $0 stop
        sleep 2
        $0 start
        ;;
esac
exit 0

