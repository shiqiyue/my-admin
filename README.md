## my-admin(开发中，不稳定，不建议使用)

使用spring boot 搭建后台管理系统，目标是使用代码生成器生成目标代码，不用修改代码。 
已部署到个人服务器，[点击进入](http://47.94.14.59:8081)  
用户名:test,密码:123456,只有查看和添加的权限

## 使用技术

1. spring boot
2. spring security(安全框架)
3. mysql
4. redis(session数据存储和缓存数据)
5. thymeleaf(页面模板)
6. boostrap和adminlte(后台页面框架)

## 开始之前  

1.  安装jdk8,本项目基于java1.8开发，不建议使用低于1.8版本的java开发；
2.  数据库服务采用mysql,版本为5.7；
3.  缓存采用redis进行缓存；如果你需要了解redis，请查看[redis官方网站](https://redis.io/)。
4.  项目使用了lombok自动生成getter方法和setter方法，如ide报错，请自行百度解决方案
5.  数据库文件在src/main/resource/sql目录下，后期稳定后将集成flyway(自动处理sql更新)

## 本地运行

1.首先下载项目到本地

1. 正确配置信息；默认配置环境为dev(开发环境)，你需要更改application-dev.yml文件修改想要的配置信息;

1. 一切就绪后，直接运行Application.java就能够启动程序；通过浏览器访问http://localhost{:端口号} 就能够访问首页(默认是服务器后台首页)

## 部署

1.  运行mvn -Dmaven.test.skip=true clean package，生成可执行jar包

2.  执行java -jar -Djava.security.egd=file:/dev/./urandom  -Dspring.profiles.active=prod app.war 就能够运行jar包。
			1.  -Djava.security.egd=file:/dev/./urandom ，这个参数作用是放置tomcat启动慢
			2.  -Dspring.profiles.active=prod，这个参数作用是启用profile为prod（正式环境）的配置文件
		
3. 如果一切正常，则恭喜你成功部署！

## 页面模板介绍

1. 后台使用了thymeleaf layout，类似于sitemesh，[参考资料](https://www.thymeleaf.org/doc/articles/layouts.html)
2. 后台总共分成了两个模板，列表模板和修改模板。  
   列表模板放置了列表页面（表格数据）公共的css和js,并定义了整体html结构。[查看文件](src/main/resources/templates/common/admin-list-template.html)   
   修改模板放置了修改和添加页面公共的css和js,并定义了整体的html结构。[查看文件](src/main/resources/templates/common/admin-edit-template.html)   

## 目录结构和重要文件介绍
   + src/main/java
        + com/github/shiqiyue/myadmin
            + anontation :工程所使用的注解
            + aspect : 工程所使用的切面类
            + common : 工程所使用的一些公共信息，多是静态字符串或者静态数字
            + config : 工程配置信息，类名以Config结尾是配置信息；类名以Configuration结尾是配置类，配置类中定义工程所需要的Bean
                + aspect : 代理配置
                + async : 异步配置
                + cache : 缓存配置
                + kaptcha : 图形验证码配置
                + log4j : log4j配置
                + mvc : spring mvc 配置
                    + interceptor : 拦截器
                + mybatis.plus : mybatis plus 配置
                + security : spring security 配置
                    + authentication 
                        + provider : 认证提供者
                    + intercetor : 拦截器，处理安全信息
                + session : spring session 配置
                + swagger : swagger配置
                + thymeleaf : thymeleaf配置
                    + dialect : 自定义thymeleaf方言
                + transaction : 事务配置
                    
            + controller : 控制器
            + entity : 实体
            + enums : 枚举
            + exception : 异常
            + mapper : mybatis mapper
            + service : 服务类，不止包含数据库服务，还有其他服务
            + util : 工具类
                + bean.mapper : dozer工具类
                + date : 日期工具
                + druid : druid工具
                + encrypt : 加密工具 @Deprecated
                + excel : excel工具
                + file : file工具
                + freemaker : freemaker工具
                + id : id生成工具
                + img : 图片处理工具
                + json : json处理工具
                + md5 : md5工具
                + number : 数字处理工具
                + reflect : 反射工具
                + security : 安全工具
                + spring : spring 工具
                + string : 字符串工具
                + web : web工具
            + vo : 页面请求或者返回数据封装的实体
            + Application.java 应用启动类
   + src/main/resource 资源文件，配置文件和页面html
        + ftl : 代码生成器模板文件，（未完成）
        + mybatis : mybatis mapper xml
        + sql : sql文件
        + static : 静态资源，css,js,图片等
        + template : 页面模板文件
            + common : 公共模板
            + error : 错误页面
            + pages : 页面模板
        + antisamy.xml : xss过滤配置文件
        + log4j2.xml : 正式环境的log4j配置
        + log4j2-test.xml : 测试或者开发环境的log4j配置
        + application.yml : spring boot 配置文件，公共的
        + application-dev.yml : 开发环境使用的spring boot配置文件
        + cache-config.yml : 缓存配置文件（暂时没用）
   + src/main/test 测试类

## 常见问题
    
   + 什么时候使用thymeleaf，什么时候ajax请求？
     + 基本原则是不变的内容使用thymeleaf，变化的内容使用ajax（比如表格内容）
   + thymeleaf中安全标签的使用  
     + 可以使用spring security的标签(前缀为:sec);  
       也可以自定义标签，项目中自定义了securitys标签，用来处理通用的权限。具体可查看实现[AdminAuthenticationProvider](src/main/java/com/github/shiqiyue/myadmin/config/security/authentication/provider/AdminAuthenticationProvider.java)  
       这部分后面可能会优化下，目标是弄得简单点。
## 版本日志

* v1.0：权限功能基本完成
* v1.1: 代码生成器（未完成）

## Licensing

* See [LICENSE](LICENSE)
